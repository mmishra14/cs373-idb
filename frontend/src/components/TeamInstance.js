import React from 'react';
import { Col, Row, Table } from 'react-bootstrap'
import moment from "moment";

class TeamInstance extends React.Component {
    constructor() {
      super();

      // stores the content that will be rendered for 
      // this team, as determined by this.props.instance
      // inserting dummy data as a placeholder
      this.state = {
        roster: [],
        rosterIds: [],
        
        "data": [
          {
            "conference_rank": 5, 
            "division_rank": 0, 
            "fg3_pct": 0.373, 
            "fg_pct": 0.468, 
            "finals_appearance": "N/A", 
            "ft_pct": 0.812, 
            "game_dates": ["Tue, 29 Jun 2021 00:00:00 GMT","Tue, 29 Jun 2021 00:00:00 GMT","Tue, 29 Jun 2021 00:00:00 GMT","Tue, 29 Jun 2021 00:00:00 GMT","Tue, 29 Jun 2021 00:00:00 GMT","Tue, 29 Jun 2021 00:00:00 GMT","Tue, 29 Jun 2021 00:00:00 GMT","Tue, 29 Jun 2021 00:00:00 GMT"],
            "game_ids": [
              445510, 
              444934, 
              444358, 
              443783, 
              442921, 
              442056, 
              441189, 
              440182, 
              438743, 
              437308, 
              436299, 
              433679, 
              430992, 
              429183, 
              427510, 
              424778, 
              264794, 
              264770, 
              264762, 
              264747, 
              264717, 
              264710, 
              264691, 
              264675, 
              264666, 
              264650, 
              264635, 
              264628, 
              264613, 
              264596, 
              264585, 
              264571, 
              264549, 
              264530, 
              264514, 
              264498, 
              264481, 
              264474, 
              264457, 
              264443, 
              264437, 
              264404, 
              264390, 
              264376, 
              264352, 
              264338, 
              264321, 
              264304, 
              264294, 
              264284, 
              264263, 
              128049, 
              128043, 
              128025, 
              128011, 
              127993, 
              127968, 
              127939, 
              127922, 
              127910, 
              127901, 
              127891, 
              127855, 
              127840, 
              127829, 
              127816, 
              127793, 
              127777, 
              127770, 
              127753, 
              127737, 
              127706, 
              127695, 
              127686, 
              157594, 
              127655, 
              127611, 
              127596, 
              127581, 
              127574, 
              127542, 
              127528, 
              127510
            ], 
            "game_names": [
              "Atlanta Hawks vs Milwaukee Bucks", 
              "Atlanta Hawks vs Milwaukee Bucks", 
              "Milwaukee Bucks vs Atlanta Hawks", 
              "Milwaukee Bucks vs Atlanta Hawks", 
              "Philadelphia 76ers vs Atlanta Hawks", 
              "Atlanta Hawks vs Philadelphia 76ers", 
              "Philadelphia 76ers vs Atlanta Hawks", 
              "Atlanta Hawks vs Philadelphia 76ers", 
              "Atlanta Hawks vs Philadelphia 76ers", 
              "Philadelphia 76ers vs Atlanta Hawks", 
              "Philadelphia 76ers vs Atlanta Hawks", 
              "New York Knicks vs Atlanta Hawks", 
              "Atlanta Hawks vs New York Knicks", 
              "Atlanta Hawks vs New York Knicks", 
              "New York Knicks vs Atlanta Hawks", 
              "New York Knicks vs Atlanta Hawks", 
              "Atlanta Hawks vs Houston Rockets", 
              "Atlanta Hawks vs Orlando Magic", 
              "Atlanta Hawks vs Washington Wizards", 
              "Atlanta Hawks vs Washington Wizards", 
              "Indiana Pacers vs Atlanta Hawks", 
              "Atlanta Hawks vs Phoenix Suns", 
              "Atlanta Hawks vs Portland Trail Blazers", 
              "Atlanta Hawks vs Chicago Bulls", 
              "Philadelphia 76ers vs Atlanta Hawks", 
              "Philadelphia 76ers vs Atlanta Hawks", 
              "Detroit Pistons vs Atlanta Hawks", 
              "Atlanta Hawks vs Milwaukee Bucks", 
              "Atlanta Hawks vs Miami Heat", 
              "New York Knicks vs Atlanta Hawks", 
              "Atlanta Hawks vs Orlando Magic", 
              "Atlanta Hawks vs Indiana Pacers", 
              "Atlanta Hawks vs Milwaukee Bucks", 
              "Toronto Raptors vs Atlanta Hawks", 
              "Charlotte Hornets vs Atlanta Hawks", 
              "Atlanta Hawks vs Chicago Bulls", 
              "Atlanta Hawks vs Memphis Grizzlies", 
              "Atlanta Hawks vs New Orleans Pelicans", 
              "Atlanta Hawks vs Golden State Warriors", 
              "New Orleans Pelicans vs Atlanta Hawks", 
              "San Antonio Spurs vs Atlanta Hawks", 
              "Denver Nuggets vs Atlanta Hawks", 
              "Golden State Warriors vs Atlanta Hawks", 
              "Sacramento Kings vs Atlanta Hawks", 
              "LA Clippers vs Atlanta Hawks", 
              "Los Angeles Lakers vs Atlanta Hawks", 
              "Atlanta Hawks vs Oklahoma City Thunder", 
              "Houston Rockets vs Atlanta Hawks", 
              "Atlanta Hawks vs Cleveland Cavaliers", 
              "Atlanta Hawks vs Sacramento Kings", 
              "Toronto Raptors vs Atlanta Hawks", 
              "Orlando Magic vs Atlanta Hawks", 
              "Miami Heat vs Atlanta Hawks", 
              "Miami Heat vs Atlanta Hawks", 
              "Oklahoma City Thunder vs Atlanta Hawks", 
              "Atlanta Hawks vs Boston Celtics", 
              "Atlanta Hawks vs Denver Nuggets", 
              "Boston Celtics vs Atlanta Hawks", 
              "New York Knicks vs Atlanta Hawks", 
              "Atlanta Hawks vs Indiana Pacers", 
              "Atlanta Hawks vs San Antonio Spurs", 
              "Dallas Mavericks vs Atlanta Hawks", 
              "Atlanta Hawks vs Toronto Raptors", 
              "Atlanta Hawks vs Utah Jazz", 
              "Atlanta Hawks vs Dallas Mavericks", 
              "Atlanta Hawks vs Los Angeles Lakers", 
              "Washington Wizards vs Atlanta Hawks", 
              "Atlanta Hawks vs Brooklyn Nets", 
              "Atlanta Hawks vs LA Clippers", 
              "Milwaukee Bucks vs Atlanta Hawks", 
              "Minnesota Timberwolves vs Atlanta Hawks", 
              "Atlanta Hawks vs Minnesota Timberwolves", 
              "Portland Trail Blazers vs Atlanta Hawks", 
              "Utah Jazz vs Atlanta Hawks", 
              "Phoenix Suns vs Atlanta Hawks", 
              "Atlanta Hawks vs Philadelphia 76ers", 
              "Atlanta Hawks vs Charlotte Hornets", 
              "Atlanta Hawks vs New York Knicks", 
              "Atlanta Hawks vs Cleveland Cavaliers", 
              "Brooklyn Nets vs Atlanta Hawks", 
              "Atlanta Hawks vs Detroit Pistons", 
              "Memphis Grizzlies vs Atlanta Hawks", 
              "Chicago Bulls vs Atlanta Hawks"
            ], 
            "game_scores": [
              "110-88","110-88","110-88","110-88","110-88","110-88","110-88","110-88","110-88",
            ],
            "id": 1, 
            "losses": 31, 
            "name": "Atlanta Hawks", 
            "player_ids": [
              490, 
              426, 
              212, 
              3547289, 
              3547244, 
              3547295, 
              481, 
              139, 
              221, 
              101, 
              666656, 
              167, 
              83, 
              666860, 
              666564, 
              3091, 
              53
            ], 
            "player_names": [
              "Trae Young", 
              "Tony Snell", 
              "Solomon Hill", 
              "Skylar Mays", 
              "Onyeka Okongwu", 
              "Nathan Knight", 
              "Lou Williams", 
              "Kris Dunn", 
              "Kevin Huerter", 
              "John Collins", 
              "De'Andre Hunter", 
              "Danilo Gallinari", 
              "Clint Capela", 
              "Cam Reddish", 
              "Bruno Fernando", 
              "Brandon Goodwin", 
              "Bogdan Bogdanovic"
            ], 
            "player_positions": [
              "Guard", 
              "Guard", 
              "Forward", 
              "Guard", 
              "Forward-Center", 
              "Forward-Center", 
              "Guard", 
              "Guard", 
              "Guard-Forward", 
              "Forward-Center", 
              "Forward-Guard", 
              "Forward", 
              "Center", 
              "Forward-Guard", 
              "Forward-Center", 
              "Guard", 
              "Guard"
            ], 
            "season": "2020-21", 
            "wins": 41
          }
        ], 
        "meta": {
          "current_page": 1, 
          "per_page": 25, 
          "total_items": 1, 
          "total_pages": 1
        },
        map: "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d6996657.965330243!2d-104.56847283781877!3d31.1003670946313!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x864070360b823249%3A0x16eb1c8f1808de3c!2sTexas!5e0!3m2!1sen!2sus!4v1625185588845!5m2!1sen!2sus",
        video: "https://www.youtube.com/embed/rBMGF6u__u4"
      }

      this.formatDate = this.formatDate.bind(this);
    }

    /**
     * Will be called as soon as this component is rendered
     * Call the backend API upon render to fetch all necessary data
     */
    componentDidMount() {
      let link = "https://nbatoday.xyz/api/teams?id=" + this.props.instance;
      fetch(link)
        .then(response => response.json())
        .then(response => {
          this.setState({ data: response.data, meta: response.meta })
        });
    }

    /**
     * Builds the rows for the Roster table to be rendered
     * @returns the set of rows for the Roster table
     */
    buildRoster () {
      let roster = [];
      for (let i = 0; i < this.state.data[0].player_ids.length; i++){
        roster.push(
        <tr onClick={() => this.props.toggle(6, this.state.data[0].player_ids[i])}>
          <td >{this.state.data[0].player_names[i]}</td>
        </tr>
        );
      }
      return roster;
    }

    /**
     * to format the date given by backend in a more user-friendly style
     * @returns the formatted date
     */
     formatDate(date) {
      let time = moment(date);
      let time_formatted = time.format("YYYY-MM-DD");
      return time_formatted;
    }

    /**
     * Build the rows for the Recent Games table
     * @returns the set of rows for the Recent Games table
     */
    buildRecentGamesTable () {
      let gamesTable = [];
      for (let i = 0; i < 5 &&  i < this.state.data[0].game_ids.length; i++){
        gamesTable.push (
          <tr onClick={() => this.props.toggle(2, this.state.data[0].game_ids[i])}>
            <td>{this.formatDate(this.state.data[0].game_dates[i])}</td>
            <td>{this.state.data[0].game_names[i].split(" vs ")[0]}</td>
            <td>{this.state.data[0].game_names[i].split(" vs ")[1]}</td>
            <td>{this.state.data[0].game_scores[i]}</td>
          </tr>
        )
      }
      return gamesTable;
    }

    render() {

        let statsStyle = { fontSize:"22px" }

        return(
          <div style={{overflowX:"auto", overflowY:"auto", height:"93vh"}}> 
          <h1 style={{textAlign:"center"}}>{this.state.data[0].name}</h1>
            <Row style={{backgroundColor:"", height:"fit-content", paddingTop:"30px", paddingLeft:"30px", paddingBottom:"15px"}}>
                <Col><img className="d-block m-auto" src={this.state.data[0].logo_embed_link} alt='' style={{width:"20rem", height:"20rem"}}/></Col>
                <Col>
                  <h2>{}</h2>
                  <p style={statsStyle}>Season: {this.state.data[0].season}</p>
                  <p style={statsStyle}>Losses: {this.state.data[0].wins}</p>
                  <p style={statsStyle}>Wins: {this.state.data[0].losses}</p>
                  <p style={statsStyle}>Field goals percentage: {this.state.data[0].fg_pct}</p>
                  <p style={statsStyle}>3 point percentage: {this.state.data[0].fg3_pct}</p>
                  <p style={statsStyle}>Free throw percentage: {this.state.data[0].ft_pct}</p>
                  <p style={statsStyle}>Conference rank: {this.state.data[0].conference_rank}</p>
                </Col>
            </Row>

            <Row style={{backgroundColor:"", height:"fit-content", paddingTop:"30px", paddingLeft:"30px", paddingBottom:"15px"}}><h2>Recent Games</h2></Row>
            <Row style={{backgroundColor:"", height:"fit-content", paddingLeft:"30px", paddingBottom:"15px"}}>
              <Table striped bordered hover style={{width:"95%"}}>
                <thead>
                  <tr>
                    <th>Date Played</th>
                    <th>Home</th>
                    <th>Away</th>
                    <th>Score</th>
                  </tr>
                </thead>
                <tbody>
                  {this.buildRecentGamesTable()}
                </tbody>
              </Table>
            </Row>

            <Row style={{backgroundColor:"", height:"fit-content", paddingTop:"30px", paddingLeft:"30px", paddingBottom:"15px"}}><h2>Roster</h2></Row>
            <Row style={{backgroundColor:"", height:"fit-content", paddingLeft:"30px", paddingBottom:"15px"}}>
              <Col>
                <Table striped bordered hover>
                  <tbody>
                    {this.buildRoster()}
                  </tbody>
                </Table>
              </Col>
              <Col>
                <iframe width="560" height="315" src={this.state.data[0].video_link} title="" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                <br></br><br></br>
                <iframe src={this.state.data[0].googlemap_embed_link} style={{border:"0", width:"560px", height:"400px"}} allowfullscreen="" loading="lazy"></iframe>
              </Col>
            </Row>

          </div>
        );
      }

}

export default TeamInstance;
