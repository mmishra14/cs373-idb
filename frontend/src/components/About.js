import React from "react";
import { Col, Row, Card, Button } from 'react-bootstrap';
import isaac from '../assets/img/aboutus/isaac.jpg';
import james from '../assets/img/aboutus/james.jpg';
import nalin from '../assets/img/aboutus/nalin.jpg';
import vineeth from '../assets/img/aboutus/vineeth.png';
import manas from '../assets/img/aboutus/manas.jpeg';

class About extends React.Component {
  constructor() {
    super();
    this.state = {

    }
  }

  render() {

    const baseStyle = {
      paddingLeft:"30px",
      paddingRight:"30px",
      paddingTop:"10px",
      paddingBottom:"15px",
      textAlign:"left"
    }
    
    return(
      <div style={{overflow:"auto", height:"93vh"}}>
        <Row style={baseStyle}><h1>About Us</h1></Row>
        <Row style={baseStyle}><h2>What is NBA Today?</h2></Row>
        <Row style={baseStyle}><p>NBAToday is a service that aims to serve as the ultimate hub for all of your NBA needs. Here, you can take a deep dive at the detailed records and statistics for all players and teams using our navigation bar to the left. In addition, we have the latest NBA news and upcoming games for the season all grouped into a simple interface. NBAToday is an extensive compilation of NBA data and is here for you.</p></Row>
        <Row style={baseStyle}><h2>Our Team</h2></Row>

        <Row style={baseStyle}>

          <Col style={{backgroundColor:""}}>
            <Card style={{ width: '18rem', height:"47rem", margin:"0 auto"}}>
              <Card.Img variant="top" src={manas} />
                <Card.Body>
                  <Card.Title>Mrityunjay Mishra</Card.Title>
                  <Card.Subtitle className="mb-2 text-muted">Architect and Full Stack Developer</Card.Subtitle>
                  <Card.Text>
                    Mrityunjay is a rising junior at UT Austin. He is also an undergraduate researcher at 
                    LARG at UT Austin under the supervision of Dr. Justin W. Hart. He loves tennis, working
                    out, and spending time with his friends. And when he's not doing any of those, he's 
                    usually found watching Netflix.<br/>
                    - Commits: 46<br/>
                    - Issues: 31<br/>
                    - Tests: 1<br/>
                  </Card.Text>
                  {/* <Button variant="primary">Go somewhere</Button> */}
                </Card.Body>
              </Card>
          </Col>

          <Col style={{backgroundColor:""}}>
            <Card style={{ width: '18rem', height:"47rem", margin:"0 auto" }}>
              <Card.Img variant="top" src={isaac} />
                <Card.Body>
                  <Card.Title>Isaac Hammer</Card.Title>
                  <Card.Subtitle className="mb-2 text-muted">Backend Developer</Card.Subtitle>
                  <Card.Text>
                  Isaac is a rising Junior computer science student at UT Austin. He works on the backend for this site. 
                  In his free time, he likes to selfhost various services on his homeserver, especially those which are 
                  decentralized and privacy-oriented.<br/>
                    - Commits: 28<br/>
                    - Issues:16<br/>
                    - Tests: 3<br/>
                  </Card.Text>
                </Card.Body>
              </Card>
          </Col>

          <Col style={{backgroundColor:""}}>
            <Card style={{ width: '18rem', height:"47rem", margin:"0 auto" }}>
              <Card.Img variant="top" src={nalin} />
                <Card.Body>
                  <Card.Title>Nalin Mahajan</Card.Title>
                  <Card.Subtitle className="mb-2 text-muted">Backend Developer</Card.Subtitle>
                  <Card.Text>
                  Nalin is a rising senior Computer Science and Math student at UT. 
                  He works on backend developement for this website. Outside of class he likes to bike, game, and learn new skills.<br/>
                    - Commits: 80<br/>
                    - Issues: 3<br/>
                    - Tests: 3<br/>
                  </Card.Text>
                  {/* <Button variant="primary">Go somewhere</Button> */}
                </Card.Body>
              </Card>
          </Col>

        </Row>

        <Row style={baseStyle}>

          <Col style={{backgroundColor:""}}>
            <Card style={{ width: '18rem', height:"47rem", margin:"0 auto"}}>
              <Card.Img variant="top" src={vineeth} />
                <Card.Body>
                  <Card.Title>Vineeth Bandi</Card.Title>
                  <Card.Subtitle className="mb-2 text-muted">Frontend Developer</Card.Subtitle>
                  <Card.Text>
                  Vineeth is a Computer Science student at UT Austin. He works on frontend development for the website. 
                  He is interested in machine learning and cloud infrastructure. His hobbies include gaming, basketball, 
                  and hanging out with friends.<br/>
                    - Commits: 28<br/>
                    - Issues: 3<br/>
                    - Tests: 1<br/>
                  </Card.Text>
                  {/* <Button variant="primary">Go somewhere</Button> */}
                </Card.Body>
              </Card>
          </Col>

          <Col style={{backgroundColor:""}}>
            <Card style={{ width: '18rem', height:"47rem", margin:"0 auto" }}>
              <Card.Img variant="top" src={james} />
                <Card.Body>
                  <Card.Title>James Dong</Card.Title>
                  <Card.Subtitle className="mb-2 text-muted">Frontend Developer</Card.Subtitle>
                  <Card.Text>
                  James Dong is rising junior Computer Science student at the University of Texas at Austin. 
                  He enjoys running, lifting weights, and listening to music during his free time.<br/>
                    - Commits: 32<br/>
                    - Issues: 9<br/>
                    - Tests: 1<br/>
                  </Card.Text>
                  {/* <Button variant="primary">Go somewhere</Button> */}
                </Card.Body>
              </Card>
          </Col>

        </Row>

        <Row style={baseStyle}><Col style={{textAlign:"center"}}><h2>Group Statistics</h2></Col></Row>
        <Row style={{paddingLeft:"30px",paddingRight:"30px",paddingTop:"10px",paddingBottom:"50px"}}>
          <Col style={{textAlign:"center"}}><h4>Total Commits <strong>214</strong></h4></Col>
          <Col style={{textAlign:"center"}}><h4>Total Issues <strong>57</strong></h4></Col>
          <Col style={{textAlign:"center"}}><h4>Total Tests <strong>9</strong></h4></Col>
        </Row>

        <Row style={{padding:"40px"}}><Col style={{textAlign:"center"}}><Button variant="outline-dark" style={{width:"100%", height:"60px", fontSize:"20px"}}><a style={{textDecoration:"none"}} href="https://speakerdeck.com/mmishra/cs-373-final-project-presentation">Our Speaker Deck Presentation</a></Button></Col></Row>
      </div>
    );
  }
}

export default About;