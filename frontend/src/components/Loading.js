import React from 'react';
import '../css/Loading.css';
import loading from '../assets/img/loading2.png';

/**
 * Component for rendering a loading icond
 */
class Loading extends React.Component {
  constructor() {
    super();
    this.state = {
    }
  }

  render() {

    return (
        <div style={{overflow:"auto", height:"93vh"}}>

            <img src={loading} className="App-logo" alt="logo " />

        </div>
    );
  }
}
export default Loading;

