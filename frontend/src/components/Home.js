import React from 'react';
import { Col, Row, Carousel, Image } from 'react-bootstrap'
import moment from "moment";
import slide1 from "../assets/img/StephCurryEspn.png"
import slide2 from "../assets/img/JamesHardenEspn.png"
import slide3 from "../assets/img/KevinDurantEspn.png"
import SunsLogo from "../assets/img/SunsLogo.png"
import ClippersLogo from "../assets/img/ClippersLogo.png"
import BucksLogo from "../assets/img/BucksLogo.png"
import '../css/Home.css';

class Home extends React.Component {
    constructor() {
      super();
      // inserting dummy data in this.state as placeholders
      this.state = {
        news_one: {
          content: "Charles Barkley made it clear that chicken is a greater love for him than a beautiful woman. The man would pass up Kim Kardashian for a KFC bucket. There’s no doubt that Chuck is a man built to entertain people. Whether it be his 16-year career as a Hall of Fame basketball player or his even longer (21 years long till date) sportscasting career with TNT, Barkley has entertained people across the age spectrum. What makes Chuck an entertainer par excellence is the total absence of a filter in his behaviour. The man will say exactly what comes to his mind when you bring any topic up with him. Also Read – “Luka Doncic is as great as Kobe Bryant to us”: The city of Dallas gives the star his own day just three seasons into his NBA career He demonstrated this better than any time when he talked about his food priorities last year. Chuck, as we all expected, isn’t going to go vegan anytime soon. “Kim Kardashian, Halle Berry or a bucket of chicken? I’m going for that chicken”: Charles Barkley When Chris Paul was traded to the OKC Thunder in 2019, he was on a sad streak of injuries. He’d missed crucial playoff games every season beginning in 2015 by that point. There was a clear need for him to change something in order to make his body more durable. He eventually decided on going vegan and checking the results. Since he’s gone vegan, Paul has missed only 2 games with injury trouble. The Inside guys brought this up in post-game discussion in early 2020. Paul had just shepherded the OKC Thunder to a clutch win at that point. Ernie brought up how Paul’s change to a vegan diet seemed to have worked wonders for him. Charles Barkley, who’d been silent till that point, found it in him to conjure a joke up from thin air. He used his own fat stature as the prop for his joke regarding a vegan diet: “That plant-based diet seems to be working for you, CP3. Not for everybody, though. Not for fat guys.” “If I was locked in a room with Kim Kardashian and then Halle Berry and a bucket of KFC chicken, I’m going for that bucket of chicken. Never gonna stop eating chicken. No plant-based anything.” Also Read – Kevin Durant pithily explains why his tweets make him the most relatable NBA player today: “I don’t see a problem with me interacting with basketball fans, it should be encouraged” “If I walk in and Kim and Halle Berry were wrapped in cauliflower or broccoli, and there’s a bucket of Kentucky Fried Chicken over there? clicks Can’t get to the captain.” The post “Kim Kardashian, Halle Berry or a bucket of chicken? I’m going for that chicken”: When Charles Barkley clarified that Chris Paul’s vegan diet will never be an option for the NBA legend appeared first on The SportsRush.",
          creator: [
          "Amulya Shekhar"
          ],
          description: "Charles Barkley made it clear that chicken is a greater love for him than a beautiful woman. The man would pass up Kim Kardashian for a KFC bucket. There’s no doubt that Chuck is a man built to entertain people. Whether it be his 16-year career as a Hall of Fame basketball player or his… The post “Kim Kardashian, Halle Berry or a bucket of chicken? I’m going for that chicken”: When Charles Barkley clarified that Chris Paul’s vegan diet will never be an option for the NBA legend appeared first on The SportsRush.",
          image_url: null,
          keywords: [
          "Basketball",
          "Charles Barkley",
          "Ernie Johnson",
          "Halle Berry",
          "Kenny Smith",
          "Kim Kardashian",
          "Shaquille O'Neal"
          ],
          link: "https://thesportsrush.com/nba-news-kim-kardashian-halle-berry-or-a-bucket-of-chicken-im-going-for-that-chicken-when-charles-barkley-clarified-that-chris-pauls-vegan-diet-will-never-be-an-option-for-the-nba-legend/",
          pubDate: "2021-07-06 18:45:55",
          source_id: "thesportsrush",
          title: "“Kim Kardashian, Halle Berry or a bucket of chicken? I’m going for that chicken”: When Charles Barkley clarified that Chris Paul’s vegan diet will never be an option for the NBA legend",
          video_url: null
        },

        news_two: {
          content: "Kevin Durant has laid out the exact reason why his interactions with basketball fans are good and why more players should do this. KD caught a lot of flak from NBA fans some years back when he unwittingly revealed his use of burners. People called him all sorts of disparaging names and judged him as an overly sensitive guy. I’ve never quite been on that train myself. This is a man who, as described by Matt Sullivan, leads a regular, boring life. He embodies the ‘ball is life’ mentality better than practically any superstar in the league today. It has been amazing to see him stage such an incredible comeback from his Achilles tear 2 summers back. Also Read – “Going to be hard to keep the Hawks together”: Bogdan Bogdanovic predicts a grim future for Trae Young and co following their loss to Giannis Antetokounmpo and the Bucks Durant has always been a man of the people – although mainstream media would have us forget this side of his personality. This man took advice from Warriors fans on Reddit on which places to visit around the Bay Area. He then posted pictures at all of those spots and thanked those fans who’d given him these suggestions. Durant also uses his Twitter to interact with fans regarding topics other than basketball. His tweets on Godzilla vs King Kong were received very well by fans. Kevin Durant breaks down why his Twitter behaviour should be emulated by other players The Slim Reaper has also built himself a reputation as one of the most savage Twitter accounts out there. On days when he feels like it, KD can roast any Twitter rando and display their stupidity for the world to see. NBA fans across social media call him a ‘bi**h’ and a ‘cupcake’ for this behaviour. But honestly, there’s nothing more ironically false than branding a man standing up for himself and roasting trolls as ‘overly sensitive’. Durant’s latest tweet is like a mic drop response to all this illogical criticism and insecure fan behaviour on social media: “I don’t see a problem with me interacting with basketball fans, it should be encouraged…steve should’ve also said that I’m never late and I work through every rep in practice with game speed. That should be more interesting than what I do on Twitter.” I don’t see a problem with me interacting with basketball fans, it should be encouraged…steve should’ve also said that I’m never late and I work through every rep in practice with game speed. That should be more interesting than what I do on Twitter. — Kevin Durant (@KDTrey5) July 6, 2021 Also Read – “How can Ben Simmons have confidence when he’s surrounded by bulls**t?!”: 76ers star’s brother, Liam Simmons, likes tweets criticizing Joel Embiid and Doc Rivers The post Kevin Durant pithily explains why his tweets make him the most relatable NBA player today: “I don’t see a problem with me interacting with basketball fans, it should be encouraged” appeared first on The SportsRush.",
          creator: [
          "Amulya Shekhar"
          ],
          description: "Kevin Durant has laid out the exact reason why his interactions with basketball fans are good and why more players should do this. KD caught a lot of flak from NBA fans some years back when he unwittingly revealed his use of burners. People called him all sorts of disparaging names and judged him as… The post Kevin Durant pithily explains why his tweets make him the most relatable NBA player today: “I don’t see a problem with me interacting with basketball fans, it should be encouraged” appeared first on The SportsRush.",
          image_url: null,
          keywords: [
          "Basketball",
          "Kevin Durant"
          ],
          link: "https://thesportsrush.com/nba-news-kevin-durant-pithily-explains-why-his-tweets-makes-him-the-most-relatable-nba-player-today-i-dont-see-a-problem-with-me-interacting-with-basketball-fans-it-should-be-encouraged/",
          pubDate: "2021-07-06 17:18:20",
          source_id: "thesportsrush",
          title: "Kevin Durant pithily explains why his tweets make him the most relatable NBA player today: “I don’t see a problem with me interacting with basketball fans, it should be encouraged”",
          video_url: null
        },

        news_three: {
          content: "The city of Dallas makes a grand gesture towards Luka Doncic just three seasons into the Slovenian’s NBA career Luka Doncic is a transcendent player, plain and simple. And at this point, every fan of the NBA should know it. Yes, he can be difficult to deal with at times. But, the fact of the matter is, at just 22-years-old, he is already nearing top-5 status amongst players in the entire league. That said, if you’re the Dallas Mavericks, you’d do anything in the world to keep him happy, right? But, with rumors surfacing that the player could be growing frustrated, the clock is very evidently ticking for the franchise. When they were in the same position, other front offices have tried a plethora of different things. A new contract, signing another star, or even signing a close friend of the player (shoutout to Karl-Anthony Towns). But, boy oh boy did Dallas, the city, go above and beyond. Also Read: Bogdan Bogdanovic predicts a grim future for Trae Young and co following their loss to Giannis Antetokounmpo and the Bucks   The city of Dallas announces ‘Luka Doncic Day’ amid rumors of the star wanting out Yeah, we were shocked too. Don’t get us wrong here, we love the commitment. But, given that Luka has only been with the franchise for three years, isn’t this maybe just a tad bit of overkill? Just maybe a tad? To make things worse, there was a major missed opportunity here, and it’s one that’d make anyone cringe in confusion. See, the city announced the 6th of July as Luka Doncic day, which would be written as 6/7, (or 7/6 based on where you’re from). But, as many fans know, Doncic’s jersey number is 77. When you take that into consideration wouldn’t it have been better to wait a day? 7th July, or alternatively 7/7 would have been magnanimously better. July 6th has been officially declared as Luka Doncic Day in Dallas County pic.twitter.com/hFbTgUbvPl — DallasTexasTV (@DallasTexasTV) July 6, 2021 Ribbing aside though, the fact of the matter is, these massive gestures could be the difference between Luka Doncic leaving and staying. Now, we’ll just have to wait and see if the Mavericks can pierce their way into the Slovenian’s heart. But if not, having a day for a player that doesn’t even play for you? Yikes! Also Read: 76ers star’s brother, Liam Simmons, likes tweets criticizing Joel Embiid and Doc Rivers The post “Luka Doncic is as great as Kobe Bryant to us”: The city of Dallas gives the star his own day just three seasons into his NBA career appeared first on The SportsRush.",
          creator: [
          "Tonoy Sengupta"
          ],
          description: "The city of Dallas makes a grand gesture towards Luka Doncic just three seasons into the Slovenian’s NBA career Luka Doncic is a transcendent player, plain and simple. And at this point, every fan of the NBA should know it. Yes, he can be difficult to deal with at times. But, the fact of… The post “Luka Doncic is as great as Kobe Bryant to us”: The city of Dallas gives the star his own day just three seasons into his NBA career appeared first on The SportsRush.",
          image_url: null,
          keywords: [
          "Basketball",
          "Dallas Mavericks",
          "Luka Doncic",
          "Mark Cuban",
          "NBA",
          "NBA Twitter"
          ],
          link: "https://thesportsrush.com/nba-news-luka-doncic-is-as-great-as-kobe-bryant-to-us-the-city-of-dallas-gives-the-star-his-own-day-just-three-seasons-into-his-nba-career/",
          pubDate: "2021-07-06 17:06:52",
          source_id: "thesportsrush",
          title: "“Luka Doncic is as great as Kobe Bryant to us”: The city of Dallas gives the star his own day just three seasons into his NBA career",
          video_url: null
        }
      }
    }

    /**
     * Will be called as soon as this component is rendered
     * Call the backend API upon render to fetch all necessary data
     */
    componentDidMount() {
      fetch("https://nbatoday.xyz/api/news")
          .then(response => response.json())
          .then(response => {
              if(!response.error-response) {
                let n_one = response.results[0];
                let n_two = response.results[1];
                let n_three = response.results[2];
                this.setState({ news_one: n_one, news_two: n_two, news_three: n_three });
              }
          });
    }
  
    render() {
      // to format the date given by backend in a more user-friendly style
      let formatDate = (date) => {
        let time = moment(date);
        let time_formatted = time.format("MMMM Do, YYYY");
        return time_formatted;
      }

      return (
        <Row style={{backgroundColor:""}}>
            <Col xs={8} style={{backgroundColor:"", height:"93vh", overflow:"auto", paddingLeft:"30px", paddingRight:"30px", textAlign:"justify"}}>
                <br></br>
                <h1>Latest News</h1><br></br>
                <div style={{backgroundColor:"", width:"", height:"auto", marginLeft:""}}>
                  <h2>{this.state.news_one.title}</h2>
                  <h5 style={{color:"rgb(125,125,125)"}}><i>{formatDate(this.state.news_one.pubDate)}</i></h5>
                  <p>{this.state.news_one.description}</p>
                  <p>{this.state.news_one.content}</p>
                  <button className="news-one"><strong><a href={this.state.news_one.link} style={{textDecoration:"none", color:"white"}}>Check out more here</a></strong></button>
                  <br></br><br></br>
                  <h2>{this.state.news_two.title}</h2>
                  <h5 style={{color:"rgb(125,125,125)"}}><i>{formatDate(this.state.news_two.pubDate)}</i></h5>
                  <p>{this.state.news_two.description}</p>
                  <p>{this.state.news_two.content}</p>
                  <button className="news-two"><strong><a href={this.state.news_two.link} style={{textDecoration:"none", color:"white"}}>Check out more here</a></strong></button>
                  <br></br><br></br>
                  <h2>{this.state.news_three.title}</h2>
                  <h5 style={{color:"rgb(125,125,125)"}}><i>{formatDate(this.state.news_three.pubDate)}</i></h5>
                  <p>{this.state.news_three.description}</p>
                  <p>{this.state.news_three.content}</p>
                  <button className="news-three"><strong><a href={this.state.news_three.link} style={{textDecoration:"none", color:"white"}}>Check out more here</a></strong></button>
                </div><br></br>

            <h2>Featured Players</h2>
            {/* carousel*/}
            <Carousel style = {{backgroundColor:"white", height: "254", width: "350", marginBottom:"30px"}}>
                <Carousel.Item interval={1000}>
                  <Image
                    className="d-block w-100"
                    src={slide1}
                    alt="First slide"
                  />
                  <Carousel.Caption>
                    <h3>Steph Curry</h3>
                  </Carousel.Caption>
                </Carousel.Item>
                
                <Carousel.Item interval={1000}>
                  <Image
                    className="d-block w-100"
                    src={slide2}
                    alt="First slide"
                  />
                  <Carousel.Caption>
                    <h3>James Harden</h3>
                  </Carousel.Caption>
                </Carousel.Item>

                <Carousel.Item interval={1000}>
                  <Image
                    className="d-block w-100"
                    src={slide3}
                    alt="First slide"
                  />
                  <Carousel.Caption>
                    <h3>Kevin Durant</h3>
                  </Carousel.Caption>
                </Carousel.Item>
                
              </Carousel>
            </Col>

            <Col xs={4} style={{backgroundColor:"", height:"93vh", overflow:"auto", borderLeft:"1px solid black"}}>
            <br></br>
                <h1>Upcoming Matches</h1><br></br>
                <div style = {{textAlign: "center"}}>

                  <h2 class = "TwoTeams">
                    <Image src = {BucksLogo} alt="Bucks Logo"/>
                      <span className="upcoming" onClick={() => this.props.toggle(7, 17)}>Bucks </span>
                        VS
                      <span className="upcoming" onClick={() => this.props.toggle(7, 24)}> Suns</span>
                    <Image src = {SunsLogo} alt = "Suns Logo"/> 
                  </h2>
                  <h4 class = "DateandTime">
                    Tue, Jul 6 - 8:00 PM
                  </h4>

                  <br />

                  <h2 class = "TwoTeams">
                    <Image src = {BucksLogo} alt="Bucks Logo"/>
                      <span className="upcoming" onClick={() => this.props.toggle(7, 17)}>Bucks </span>
                        VS
                      <span className="upcoming" onClick={() => this.props.toggle(7, 24)}> Suns</span>
                    <Image src = {SunsLogo} alt = "Suns Logo"/> 
                  </h2>
                  <h4 class = "DateandTime">
                    Thurs, Jul 8 - 8:00 PM
                  </h4>

                  <br />

                  <h2 class = "TwoTeams">
                    <Image src = {SunsLogo} alt="Suns Logo"/> 
                      <span span className="upcoming" onClick={() => this.props.toggle(7, 24)}>Suns </span>
                        VS
                      <span className="upcoming" onClick={() => this.props.toggle(7, 13)}> Clippers</span>
                    <Image src = {ClippersLogo} alt = "Clippers Logo"/>
                  </h2>
                  <h4 class = "DateandTime">
                    Sun, Jul 11 - 7:00 PM
                  </h4>
                </div>
                <p align = "center">
                <iframe width="424" height="238" src="https://www.youtube.com/embed/_047PszpMfs" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </p>
            </Col>

        </Row>
      );
    }
  }
  export default Home;