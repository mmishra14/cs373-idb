import React from "react";
import { Row, Col, Card, Button, Pagination, Dropdown, DropdownButton, Form, FormControl } from 'react-bootstrap';
import { DropdownSubmenu } from "react-bootstrap-submenu";

// range function implemented for ease of implementation
// in later parts
const range = (from, to, step = 1) => {
  let i = from;
  const range = [];

  while (i <= to) {
    range.push(i);
    i += step;
  }

  return range;
}

class Players extends React.Component {
  constructor() {
    super();
    this.state = {
      data: [],
      meta: { current_page: 1, total_pages: 5}
    }
    this.page_size = 10;
    this.ageFilter = "";
    this.heightFilter = "";
    this.weightFilter = "";
    this.positionFilter = "";
    this.teamFilter = "";
    this.sortBy = "";
    this.search = "";
    this.goToPage = this.renderPage.bind(this);
    this.setPaginationBar = this.setPaginationBar.bind(this);
    this.changePageState = this.changePageState.bind(this);
    this.setSort = this.setSort.bind(this);
    this.setPageSize = this.setPageSize.bind(this);
  }

  /**
   * Will be called as soon as this component is rendered
   * Call the backend API upon render to fetch all necessary data
   */
  componentDidMount() {
    fetch("https://nbatoday.xyz/api/players?per_page=10&page=1")
        .then(response => response.json())
        .then(response => {
          this.setState({ data: response.data, meta: response.meta })
        });
  }

  /**
   * Builds the pagination bar at the bottom of the page
   * Will be called on every render of this component
   * @returns the pagination bar to be rendered
   */
  setPaginationBar(){
    let items = [];
    let total = this.state.meta.total_pages;
    if (this.state.meta.current_page <= 2){
      for (let i = 1; i <= 5; i++){
        items.push( 
          <Pagination.Item key={i} active={i === this.state.meta.current_page} onClick={() => {this.changePageState(i);}}>
            {i}
          </Pagination.Item>
        )
      }
    }
    else if (this.state.meta.current_page >= total - 2){
      for (let i = total - 4; i <= total; i++){
        items.push( 
          <Pagination.Item key={i} active={i === this.state.meta.current_page} onClick={() => {this.changePageState(i);}}>
            {i}
          </Pagination.Item>
        )
      }
    }
    else {
      let start = this.state.meta.current_page - 2;
      let end = this.state.meta.current_page + 2;
      for (const i of range(start, end)){
        items.push( 
        <Pagination.Item key={i} active={i === this.state.meta.current_page} onClick={() => {this.changePageState(i);}}>
          {i}
        </Pagination.Item>
        )
      }
    }
    return items
  }

  /**
   * Fetches data from the backend using filtering, sorting, and pagination queries
   * Called whenever the user sets filtering, sorting, and page size
   * @param {*} page the page # that should be fetched from backend
   */
  changePageState(page){
    if(page > 0 && page < this.state.meta.total_pages + 1) {
      let link = "https://nbatoday.xyz/api/players?per_page=" + this.page_size + "&page=" + page + this.ageFilter + this.heightFilter + this.weightFilter + this.positionFilter + this.teamFilter + this.sortBy;
      if (this.search.length != 0){
        link += "&q=" + this.search;
      }
      fetch(link)
          .then(response => response.json())
          .then(response => {
            if(response.data.length !== 0) this.setState({ data: response.data, meta: response.meta })
            else this.setState({ data: response.data, meta: { current_page: 1, total_pages: 5 } })
          });
    }
  }

  /**
   * Constructs the grid of Cards to be displayed
   * Constructs a Card per player with players info
   * @returns grid of Cards to be rendered
   */
  renderPage(){ 
    if (this.state.data.length === 0){
      return [];
    }
    let cards = [];
    let q = this.search.split(" ");
    for (let i = 0; i < this.page_size && this.state.data[i]; i++){
      let searchMode = this.getSearchStatus(this.state.data[i]);
      
      let highlightTitle = "";
      let highlightBody = "";

      if(searchMode !== "No Search") {

        let d = this.state.data[i];
        for(let j = 0; j < q.length; j++) {
          let check = q[j].toLowerCase();
          console.log(check)

          if(d.name.toLowerCase().includes(check)) highlightTitle = "yellow";

          if (((d.age + "").includes(check)) || 
              ((d.height + "").includes(check)) ||
              ((d.weight + "").includes(check)) ||
              (d.position.toLowerCase().includes(check)) ||
              (d.current_team_name.toLowerCase().includes(check))) { highlightBody = "yellow"; }
        }
      }

      cards.push( 
      <Col style={{backgroundColor:""}}>
        <Card style={{margin:"0 auto", maxWidth:"14rem", maxHeight:"24rem"}} >
          <Card.Img variant="top" src={this.state.data[i].headshot_embed_link} style={{height:"10rem"}}/>
          <Card.Body>
            <Card.Title style={{fontSize:"1.0em", backgroundColor:highlightTitle}}>{this.state.data[i].name}</Card.Title>
            <Card.Text style={{fontSize:"0.8em", backgroundColor:highlightBody}}>
              - Age: {this.state.data[i].age} <br/>
              - Height: {this.state.data[i].height} in.<br />
              - Weight: {this.state.data[i].weight} lb.<br />
              - Position: {this.state.data[i].position} <br />
              - Team: {this.state.data[i].current_team_name} <br />
              - Search Mode: {searchMode} <br />
            </Card.Text>
            <Button style={{fontSize:"0.8em"}} variant="dark" onClick={() => this.props.toggle(6, this.state.data[i].id)}>More Info</Button>
          </Card.Body>
        </Card>
      </Col>
      )
    }   
    return cards
  }

   /**
   * 
   * @param {*} currentPlayer is a single instance of a team
   * @returns type of search it is: AND, OR, or NO SEARCH
   */
    getSearchStatus(currentPlayer) {
      if(currentPlayer.search_result === "and") {
        return "AND Search";
      } else if(currentPlayer.search_result === "or") {
        return "OR Search";
      }
      return "No Search";
    }

  /**
   * Sets the cards on the page in a row of 5
   * These are the cards that will be displayed 
   * on the page in that row
   * @param {*} playerArray the grid of pre-constructed Cards
   * @returns the constructed row
   */
  setCards(playerArray){
    let rows = [];
    for(let i = 0; i < this.page_size / 5; i++) {
      let players = [];
      for(let j = i * 5; j < i * 5 + 5; j++) {
        players.push(playerArray[j]);
      }
      rows.push(
        <Row style={{backgroundColor:"", paddingLeft:"30px", paddingRight:"30px", paddingBottom:"50px"}}>       
          {players[0]}
          {players[1]}
          {players[2]}
          {players[3]}
          {players[4]}
        </Row>
        )
    }
    return rows;
  }

  /**
   * Creates a filtering string query
   * @param {*} attribute the attribute to check against for filtering
   * @param {*} filter the actual filter value to be applied
   */
  setFilter(attribute, filter){
    if (attribute === "age"){
      this.ageFilter = "&age=" + filter;
    }
    else if (attribute === "height"){
      this.heightFilter = "&height=" + filter;
    }
    else if (attribute === "weight"){
      this.weightFilter = "&weight=" + filter;
    }
    else if (attribute === "position"){
      this.positionFilter = "&position=" + filter;
    }
    else if (attribute === "current_team_id"){
      this.teamFilter = "&current_team_id=" + filter;
    }
    else { {/* reset case */}
      this.ageFilter = "";
      this.heightFilter = "";
      this.weightFilter = "";
      this.positionFilter = "";
      this.teamFilter = "";
    }
    this.changePageState(1)
  }

  /**
   * Constructs the sorting query for fetching data from backedn
   * @param {*} filter the filter/sorting attribute to be applied
   * @param {*} direction ascending/descending direction
   */
  setSort(filter, direction){
    if (filter === "reset"){
      this.sortBy = "";
    }
    else {
      this.sortBy = "&sort_by=" + filter + "&sort_order=" + direction;
    }
    this.changePageState(1)
  }

  /**
   * Sets the size of the current page to be rendered
   * @param {*} size the size of the current page
   */
  setPageSize(size) {
    this.page_size = size;
    this.changePageState(1)
  }

  /**
   * Sets the search criteria for the players
   * Also resets the filters and sort options
   * since the search needs to be fresh
   */
  setSearch(){
    this.sortBy = "";
    this.setFilter("reset", "");
  }


  render() {
    let playerArray = this.renderPage();
    return(
      
      <div style={{overflow:"auto", height:"93vh"}}>
        <Row style={{backgroundColor:"", Height:"fit-content", paddingTop:"30px", paddingLeft:"30px"}}>
          <h1>Players</h1>
        </Row>

        <Row style={{ backgroundColor:"", Height:"fit-content", paddingLeft:"30px", paddingBottom:"15px", paddingRight:'50px', justifyContent: "flex-end"}}>

          <DropdownButton id="dropdown-basic-button" variant="secondary" title="Filter By" style={{paddingRight:'10px' }}>
            <DropdownSubmenu id="dropdown-basic-button" title="Age">
              <Dropdown.Item onClick={() => this.setFilter("age", "ge_25")}>25+</Dropdown.Item>
              <Dropdown.Item onClick={() => this.setFilter("age", "ge_30")}>30+</Dropdown.Item>
              <Dropdown.Item onClick={() => this.setFilter("age", "ge_35")}>35+</Dropdown.Item>
            </DropdownSubmenu>
            <DropdownSubmenu id="dropdown-basic-button" title="Height">
              <Dropdown.Item onClick={() => this.setFilter("height", "ge_70")}>70+</Dropdown.Item>
              <Dropdown.Item onClick={() => this.setFilter("height", "ge_75")}>75+</Dropdown.Item>
              <Dropdown.Item onClick={() => this.setFilter("height", "ge_80")}>80+</Dropdown.Item>
              <Dropdown.Item onClick={() => this.setFilter("height", "ge_85")}>85+</Dropdown.Item>
              <Dropdown.Item onClick={() => this.setFilter("height", "ge_90")}>90+</Dropdown.Item>
            </DropdownSubmenu>
            <DropdownSubmenu id="dropdown-basic-button" title="Weight">
              <Dropdown.Item onClick={() => this.setFilter("weight", "ge_160")}>160+</Dropdown.Item>
              <Dropdown.Item onClick={() => this.setFilter("weight", "ge_200")}>200+</Dropdown.Item>
              <Dropdown.Item onClick={() => this.setFilter("weight", "ge_240")}>240+</Dropdown.Item>
              <Dropdown.Item onClick={() => this.setFilter("weight", "ge_280")}>280+</Dropdown.Item>
            </DropdownSubmenu>
            <DropdownSubmenu id="dropdown-basic-button" title="Position">
              <Dropdown.Item onClick={() => this.setFilter("position", "center-forward")}>Center-Forward</Dropdown.Item>
              <Dropdown.Item onClick={() => this.setFilter("position", "center")}>Center</Dropdown.Item>
              <Dropdown.Item onClick={() => this.setFilter("position", "guard")}>Guard</Dropdown.Item>
              <Dropdown.Item onClick={() => this.setFilter("position", "forward-center")}>Forward-Center</Dropdown.Item>
              <Dropdown.Item onClick={() => this.setFilter("position", "guard-forward")}>Guard-Forward</Dropdown.Item>
              <Dropdown.Item onClick={() => this.setFilter("position", "forward-guard")}>Forward-Guard</Dropdown.Item>
              <Dropdown.Item onClick={() => this.setFilter("position", "forward")}>Forward</Dropdown.Item>
            </DropdownSubmenu>
            <DropdownSubmenu id="dropdown-basic-button" title="Team" style={{overflowY: 'scroll'}}>
              <Dropdown.Item onClick={() => this.setFilter("current_team_id", "1")}>Atlanta Hawks</Dropdown.Item>
              <Dropdown.Item onClick={() => this.setFilter("current_team_id", "2")}>Boston Celtics</Dropdown.Item>
              <Dropdown.Item onClick={() => this.setFilter("current_team_id", "3")}>Brooklyn Nets</Dropdown.Item>
              <Dropdown.Item onClick={() => this.setFilter("current_team_id", "4")}>Charlotte Hornets</Dropdown.Item>
              <Dropdown.Item onClick={() => this.setFilter("current_team_id", "5")}>Chicago Bulls</Dropdown.Item>
              <Dropdown.Item onClick={() => this.setFilter("current_team_id", "6")}>Cleveland Cavaliers</Dropdown.Item>
              <Dropdown.Item onClick={() => this.setFilter("current_team_id", "7")}>Dallas Maveriks</Dropdown.Item>
              <Dropdown.Item onClick={() => this.setFilter("current_team_id", "8")}>Denver Nuggets</Dropdown.Item>
              <Dropdown.Item onClick={() => this.setFilter("current_team_id", "9")}>Detroit Pistons</Dropdown.Item>
              <Dropdown.Item onClick={() => this.setFilter("current_team_id", "10")}>Golden State Warriors</Dropdown.Item>
              <Dropdown.Item onClick={() => this.setFilter("current_team_id", "11")}>Houston Rockets</Dropdown.Item>
              <Dropdown.Item onClick={() => this.setFilter("current_team_id", "12")}>Indiana Pacers</Dropdown.Item>
              <Dropdown.Item onClick={() => this.setFilter("current_team_id", "13")}>LA Clippers</Dropdown.Item>
              <Dropdown.Item onClick={() => this.setFilter("current_team_id", "14")}>Los Angeles Lakers</Dropdown.Item>
              <Dropdown.Item onClick={() => this.setFilter("current_team_id", "15")}>Memphis Grizzlies</Dropdown.Item>
              <Dropdown.Item onClick={() => this.setFilter("current_team_id", "16")}>Miami Heat</Dropdown.Item>
              <Dropdown.Item onClick={() => this.setFilter("current_team_id", "17")}>Milwaukee Bucks</Dropdown.Item>
              <Dropdown.Item onClick={() => this.setFilter("current_team_id", "18")}>Minnesota Timberwolves</Dropdown.Item>
              <Dropdown.Item onClick={() => this.setFilter("current_team_id", "19")}>New Orleans Pelicans</Dropdown.Item>
              <Dropdown.Item onClick={() => this.setFilter("current_team_id", "20")}>New York Knicks</Dropdown.Item>
              <Dropdown.Item onClick={() => this.setFilter("current_team_id", "21")}>Oklahoma City Thunder</Dropdown.Item>
              <Dropdown.Item onClick={() => this.setFilter("current_team_id", "22")}>Orlando Magic</Dropdown.Item>
              <Dropdown.Item onClick={() => this.setFilter("current_team_id", "23")}>Philadelphia 76ers</Dropdown.Item>
              <Dropdown.Item onClick={() => this.setFilter("current_team_id", "24")}>Pheonix Suns</Dropdown.Item>
              <Dropdown.Item onClick={() => this.setFilter("current_team_id", "25")}>Portland Trail Blazers</Dropdown.Item>
              <Dropdown.Item onClick={() => this.setFilter("current_team_id", "26")}>Sacramento Kings</Dropdown.Item>
              <Dropdown.Item onClick={() => this.setFilter("current_team_id", "27")}>San Antonio Sputs</Dropdown.Item>
              <Dropdown.Item onClick={() => this.setFilter("current_team_id", "28")}>Toronto Raptors</Dropdown.Item>
              <Dropdown.Item onClick={() => this.setFilter("current_team_id", "29")}>Utah Jazz</Dropdown.Item>
              <Dropdown.Item onClick={() => this.setFilter("current_team_id", "30")}>Washington Wizards</Dropdown.Item>
            </DropdownSubmenu>
            <Dropdown.Item onClick={() => this.setFilter("reset", "")}>Reset</Dropdown.Item>
          </DropdownButton>

          <DropdownButton id="dropdown-basic-button" variant="secondary" title="Sort By" style={{paddingRight:'10px'}}>
            <DropdownSubmenu id="dropdown-basic-button" title="Age">
              <Dropdown.Item onClick={() => this.setSort("age", "ascending")}>Ascending</Dropdown.Item>
              <Dropdown.Item onClick={() => this.setSort("age", "descending")}>Descending</Dropdown.Item>
            </DropdownSubmenu>
            <DropdownSubmenu id="dropdown-basic-button" title="Height">
              <Dropdown.Item onClick={() => this.setSort("height", "ascending")}>Ascending</Dropdown.Item>
              <Dropdown.Item onClick={() => this.setSort("height", "descending")}>Descending</Dropdown.Item>
            </DropdownSubmenu>
            <DropdownSubmenu id="dropdown-basic-button" title="Weight">
              <Dropdown.Item onClick={() => this.setSort("weight", "ascending")}>Ascending</Dropdown.Item>
              <Dropdown.Item onClick={() => this.setSort("weight", "descending")}>Descending</Dropdown.Item>
            </DropdownSubmenu>
            <DropdownSubmenu id="dropdown-basic-button" title="Position">
              <Dropdown.Item onClick={() => this.setSort("position", "ascending")}>Ascending</Dropdown.Item>
              <Dropdown.Item onClick={() => this.setSort("position", "descending")}>Descending</Dropdown.Item>
            </DropdownSubmenu>
            <DropdownSubmenu id="dropdown-basic-button" title="Team">
              <Dropdown.Item onClick={() => this.setSort("current_team_id", "ascending")}>Ascending</Dropdown.Item>
              <Dropdown.Item onClick={() => this.setSort("current_team_id", "descending")}>Descending</Dropdown.Item>
            </DropdownSubmenu>
            <Dropdown.Item onClick={() => this.setSort("reset", "")}>Reset</Dropdown.Item>
          </DropdownButton>

          <DropdownButton id="dropdown-basic-button" variant="secondary" title="Players per Page">
            <Dropdown.Item onClick={() => this.setPageSize(5)}>5</Dropdown.Item>
            <Dropdown.Item onClick={() => this.setPageSize(10)}>10</Dropdown.Item>
            <Dropdown.Item onClick={() => this.setPageSize(25)}>25</Dropdown.Item>
            <Dropdown.Item onClick={() => this.setPageSize(50)}>50</Dropdown.Item>
          </DropdownButton>

          <Form className="d-flex" style={{paddingLeft: '15px', paddingRight: '10px'}}>
            <FormControl
              type="search"
              placeholder="Search Players"
              className="mr-2"
              aria-label="Search"
              onChange={e => this.search = e.target.value}
              onKeyPress={e => {
                console.log(e.key)
                if (e.key === "Enter") {
                  e.preventDefault();
                  this.setSearch();
                }
              }}
            />
            <Button variant="outline-dark" onClick={() => this.setSearch()}>Search</Button>
            </Form>

        </Row>
        
        {this.setCards(playerArray)} 

        <Row style={{paddingBottom:"30px"}}>
          <Col style={{ display: "flex", justifyContent: "center" }}>
            <Pagination className="mx-auto my-2">
              <Pagination.First onClick={() => this.changePageState(1)}/>
              <Pagination.Prev onClick={() => this.changePageState(this.state.meta.current_page-1)}/>
              {this.setPaginationBar()}   
              <Pagination.Next onClick={() => this.changePageState(this.state.meta.current_page+1)}/> 
              <Pagination.Last onClick={() => this.changePageState(this.state.meta.total_pages)} />
            </Pagination>
          </Col>
        </Row>
        
      </div>
      
    );
  }
}

export default Players;