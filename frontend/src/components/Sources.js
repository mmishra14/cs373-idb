import React from "react";
import { Col, Row, Card, Button } from 'react-bootstrap';
import gitlab from '../assets/img/sources/gitlab.png';
import postman from '../assets/img/sources/postman.png';
import react from '../assets/img/sources/react.png';
import vscode from '../assets/img/sources/vscode.png';
import anyapi from '../assets/img/sources/anyapi.png';
import ball from '../assets/img/sources/ball2.jpg';
import newsdata from '../assets/img/sources/newsdata.png';
import discord from '../assets/img/sources/discord.webp';
import rbootstrap from '../assets/img/sources/rboot.png';

class Sources extends React.Component {
  constructor() {
    super();
    this.state = {

    }
  }

  render() {

    const baseStyle = {
      paddingLeft:"30px",
      paddingRight:"30px",
      paddingTop:"10px",
      paddingBottom:"15px"
    }

    const buttonStyle = {
      textDecoration:"none",
      color:"white"
    }

    return(
      <div style={{overflow:"auto", height:"93vh"}}>
        <Row style={baseStyle}><h1>APIs and Sources</h1></Row>
        <Row style={baseStyle}><h2>Our APIs and Data Sources</h2></Row>

        <Row style={baseStyle}>

          <Col style={{backgroundColor:""}}>
            <Card style={{ width: '18rem', height:"31rem", margin:"0 auto"}}>
              <Card.Img variant="top" src={anyapi} />
                <Card.Body>
                  <Card.Title>Any-API</Card.Title>
                  <Card.Text>An open-source API that provides data in many domains, including sports.</Card.Text>
                  <Button variant="dark"><a style={buttonStyle} href="https://any-api.com/">More Info</a></Button>
                </Card.Body>
              </Card>
          </Col>

          <Col style={{backgroundColor:""}}>
            <Card style={{ width: '18rem', height:"31rem", margin:"0 auto" }}>
              <Card.Img variant="top" src={ball} />
                <Card.Body>
                  <Card.Title>balldontlie.io</Card.Title>
                  <Card.Text>An open-source API that provides pointed information regarding the NBA.</Card.Text>
                  <Button variant="dark"><a style={buttonStyle} href="https://www.balldontlie.io/#introduction">More Info</a></Button>
                </Card.Body>
              </Card>
          </Col>

          <Col style={{backgroundColor:""}}>
            <Card style={{ width: '18rem', height:"31rem", margin:"0 auto" }}>
              <Card.Img variant="top" src={newsdata} />
                <Card.Body>
                  <Card.Title>newsdata.io</Card.Title>
                  <Card.Text>An open-source API that provides news data in many domains, including sports.</Card.Text>
                  <Button variant="dark"><a style={buttonStyle} href="https://newsdata.io/">More Info</a></Button>
                </Card.Body>
              </Card>
          </Col>

        </Row>

        <Row style={baseStyle}><h4>Our Data Scraping Process</h4></Row>
        <Row style={baseStyle}>
          <Col>
            <p style={{textAlign:"justify"}}>Data was scraped through a programmatic but lengthy sequence. First since NBA-Stats is not well documented we had to experiment and find our own endpoints. These are defined in endpoints.py. Then by running scrape.py we are able to firstly, scrape team data and note down player and game ids. During this time a sleep period is entered to prevent us from reaching rate limits. Then in a similar fashion the players and games data is scraped and saved to players.json, teams.json, games.json. Then certain stats such as total wins are generated and then saved again to the JSON. In addition since we have two disparate sources: NBA-stats and ball don't lie we preprocess by storing and matching the different ids for player, teams and games in apidefs.py.</p>
          </Col>
        </Row>

        <Row style={baseStyle}><h2>Our Tools</h2></Row>

        <Row style={baseStyle}>

          <Col style={{backgroundColor:""}}>
            <Card style={{ width: '18rem', height:"34rem", margin:"0 auto"}}>
              <Card.Img variant="top" src={rbootstrap} style={{height:"15rem"}}/>
                <Card.Body>
                  <Card.Title>React-Bootstrap</Card.Title>
                  <Card.Text>For creating responsive designs in our website.</Card.Text>
                  <Button variant="dark"><a style={buttonStyle} href="https://react-bootstrap.github.io/">More Info</a></Button>
                </Card.Body>
              </Card>
          </Col>

          <Col style={{backgroundColor:""}}>
            <Card style={{ width: '18rem', height:"34rem", margin:"0 auto" }}>
              <Card.Img variant="top" src={gitlab} style={{height:"15rem"}}/>
                <Card.Body>
                  <Card.Title>GitLab</Card.Title>
                  <Card.Text>
                    Common repo used for shared development, tracking issues, and generally following agile project methodologies.<br/>
                    - <a href="https://gitlab.com/mmishra14/cs373-idb/-/issues">Our GitLab Issue Tracker</a><br/>
                    - <a href="https://gitlab.com/mmishra14/cs373-idb">Our GitLab Repo</a><br/>
                    - <a href="https://gitlab.com/mmishra14/cs373-idb/-/wikis/p3">Our GitLab Wiki</a><br/>
                  </Card.Text>
                  <Button variant="dark"><a style={buttonStyle} href="https://gitlab.com/mmishra14/cs373-idb">More Info</a></Button>
                </Card.Body>
              </Card>
          </Col>

          <Col style={{backgroundColor:""}}>
            <Card style={{ width: '18rem', height:"34rem", margin:"0 auto" }}>
              <Card.Img variant="top" src={postman} style={{height:"15rem"}}/>
                <Card.Body>
                  <Card.Title>Postman</Card.Title>
                  <Card.Text>For creating and testing endpoints of our APIs.</Card.Text>
                  <Button variant="dark"><a style={buttonStyle} href="https://documenter.getpostman.com/view/16329964/TzedfPVr">Check out our Postman API</a></Button>
                </Card.Body>
              </Card>
          </Col>

          <Col style={{backgroundColor:""}}>
            <Card style={{ width: '18rem', height:"34rem", margin:"0 auto" }}>
              <Card.Img variant="top" src={react} style={{height:"15rem"}}/>
                <Card.Body>
                  <Card.Title>React</Card.Title>
                  <Card.Text>For fronend developement of our website.</Card.Text>
                  <Button variant="dark"><a style={buttonStyle} href="https://reactjs.org/">More Info</a></Button>
                </Card.Body>
              </Card>
          </Col>

        </Row>

        <Row style={baseStyle}>

          <Col style={{backgroundColor:""}}>
            <Card style={{ width: '18rem', height:"28rem", margin:"0 auto"}}>
              <Card.Img variant="top" src={vscode} style={{height:"15rem"}}/>
                <Card.Body>
                  <Card.Title>VSCode</Card.Title>
                  <Card.Text>The main IDE used for developement of this project.</Card.Text>
                  <Button variant="dark"><a style={buttonStyle} href="https://code.visualstudio.com/">More Info</a></Button>
                </Card.Body>
              </Card>
          </Col>

          <Col style={{backgroundColor:""}}>
            <Card style={{ width: '18rem', height:"28rem", margin:"0 auto" }}>
              <Card.Img variant="top" src={discord} style={{height:"15rem"}}/>
                <Card.Body>
                  <Card.Title>Discord</Card.Title>
                  <Card.Text>The primary medium of communication among group members</Card.Text>
                  <Button variant="dark"><a style={buttonStyle} href="https://discord.com/">More Info</a></Button>
                </Card.Body>
              </Card>
          </Col>

          </Row>

          <Row style={{padding:"40px"}}><Col style={{textAlign:"center"}}><Button variant="outline-dark" style={{width:"100%", height:"60px", fontSize:"20px"}}><a style={{textDecoration:"none"}} href="https://nbatoday.xyz/api/run_test">Run Unit Tests on our API</a></Button></Col></Row>

          <Row style={{paddingBottom:"30px"}}><Col style={{textAlign:"center"}}><i>Please note that it will take at least 2 minutes to run the unit tests. As long as the loading sign displays in the tab above, the tests are being executed.</i></Col></Row>
      </div>
    );
  }
}

export default Sources;