import React from "react";
import { Row, Col, Card, Button, Pagination, Dropdown, DropdownButton, Table } from 'react-bootstrap';
import moment from "moment"

// range function implemented for ease of implementation
// in later parts
const range = (from, to, step = 1) => {
  let i = from;
  const range = [];

  while (i <= to) {
    range.push(i);
    i += step;
  }

  return range;
}

class SearchResults extends React.Component {
  constructor() {
    super();
    this.page_size = 5;
    this.playersFilter = true;
    this.teamsFilter = true;
    this.gamesFilter = true;
  }

  /**
   * to format the date given by backend in a more user-friendly style
   * @returns the formatted date
   */
  formatDate(date) {
      let time = moment(date);
      let time_formatted = time.format("YYYY-MM-DD");
      return time_formatted;
  }

  /**
   * Builds the pagination bar at the bottom of the page
   * Will be called on every render of this component
   * @returns the pagination bar to be rendered
   */
  setPaginationBar(){
    let items = [];
    if (this.props.data.current_page <= 2){
      for (let i = 1; i <= 5; i++){
        items.push( 
          <Pagination.Item key={i} active={i === this.props.data.current_page} onClick={() => {this.props.search(i);}}>
            {i}
          </Pagination.Item>
        )
      }
    }
    else if (this.props.data.current_page >= this.total - 2){
      for (let i = this.total - 4; i <= this.total; i++){
        items.push( 
          <Pagination.Item key={i} active={i === this.props.data.current_page} onClick={() => {this.props.search(i);}}>
            {i}
          </Pagination.Item>
        )
      }
    }
    else {
      let start = this.props.data.current_page - 2;
      let end = this.props.data.current_page + 2;
      for (const i of range(start, end)){
        items.push( 
        <Pagination.Item key={i} active={i === this.props.data.current_page} onClick={() => {this.props.search(i);}}>
          {i}
        </Pagination.Item>
        )
      }
    }
    return items
  }

     /**
   * 
   * @param {*} currentPlayer is a single instance of a team
   * @returns type of search it is: AND, OR, or NO SEARCH
   */
      getSearchStatus(currentData) {
        if(currentData.search_result === "and") {
          return "AND Search";
        } else if(currentData.search_result === "or") {
          return "OR Search";
        }
        return "No Search";
      }

  /**
   * Constructs the grid of Cards for Players and Teams
   * Constructs rows for the Games table
   * @returns grid of Cards to be rendered
   */
  renderPage(){ 
    let search = {
        playerCards: [],
        teamCards: [],
        gameTable: []
    }
    let q = this.props.data.search.split(" ");
    if (this.playersFilter) {
      for (let i = 0; i < this.page_size && this.props.data.players.data[i]; i++){
        let searchMode = this.getSearchStatus(this.props.data.players.data[i]);

        let highlightTitle = "";
        let highlightBody = "";
  
        if(searchMode !== "No Search") {
          let d = this.props.data.players.data[i];
          for(let j = 0; j < q.length; j++) {
            let check = q[j].toLowerCase();
  
            if(d.name.toLowerCase().includes(check)) highlightTitle = "yellow";
  
            if (((d.age + "").includes(check)) || 
                ((d.height + "").includes(check)) ||
                ((d.weight + "").includes(check)) ||
                (d.position.toLowerCase().includes(check)) ||
                (d.current_team_name.toLowerCase().includes(check))) { highlightBody = "yellow"; }
          }
        }


        search.playerCards.push( 
        <Col style={{backgroundColor:""}}>
            <Card style={{margin:"0 auto", maxWidth:"14rem", maxHeight:"24rem"}} >
            <Card.Img variant="top" src={this.props.data.players.data[i].headshot_embed_link} style={{height:"10rem"}}/>
            <Card.Body>
                <Card.Title style={{fontSize:"1.0em", backgroundColor:highlightTitle}}>{this.props.data.players.data[i].name}</Card.Title>
                <Card.Text style={{fontSize:"0.75em", backgroundColor:highlightBody}}>
                - Age: {this.props.data.players.data[i].age} <br/>
                - Height: {this.props.data.players.data[i].height} <br />
                - Weight: {this.props.data.players.data[i].weight} <br />
                - Position: {this.props.data.players.data[i].position} <br />
                - Team: {this.props.data.players.data[i].current_team_name} <br/>
                - Search Mode: {searchMode} <br/>
                </Card.Text>
                <Button style={{fontSize:"0.8em"}} variant="dark" onClick={() => this.props.toggle(6, this.props.data.players.data[i].id)}>More Info</Button>
            </Card.Body>
            </Card>
        </Col>
        )
      }
    }

    if (this.teamsFilter)
        for (let i = 0; i < this.page_size && this.props.data.teams.data[i]; i++){
        let searchMode = this.getSearchStatus(this.props.data.teams.data[i]);


        let highlightTitle = "";
        let highlightBody = "";

        if(searchMode !== "No Search") {

          let d = this.props.data.teams.data[i];
          for(let j = 0; j < q.length; j++) {
            let check = q[j].toLowerCase();

            if(d.name.toLowerCase().includes(check)) highlightTitle = "yellow";

            if (((d.wins + "").includes(check)) || 
                ((d.losses + "").includes(check)) ||
                ((d.fg_pct + "").includes(check)) ||
                ((d.fg3_pct + "").includes(check)) ||
                ((d.ft_pct + "").includes(check)) ||
                ((d.conference_rank + "").includes(check))) { highlightBody = "yellow"; }
          }
        }


        search.teamCards.push( 
        <Col style={{backgroundColor:""}}>
            <Card style={{margin:"0 auto", maxWidth:"14rem", maxHeight:"24rem"}} >
            <Card.Img variant="top" src={this.props.data.teams.data[i].logo_embed_link} style={{height:"10rem"}}/>
            <Card.Body>
                <Card.Title style={{fontSize:"1.0em", backgroundColor:highlightTitle}}>{this.props.data.teams.data[i].name}</Card.Title>
                <Card.Text style={{fontSize:"0.75em", backgroundColor:highlightBody}}>
                  - Wins: {this.props.data.teams.data[i].wins} <br/>
                  - Losses: {this.props.data.teams.data[i].losses} <br />
                  - FG percentage: {this.props.data.teams.data[i].fg_pct}<br/>
                  - 3 point percentage: {this.props.data.teams.data[i].fg3_pct}<br/>
                  - FT percentage: {this.props.data.teams.data[i].ft_pct}<br/>
                  - Conference rank: {this.props.data.teams.data[i].conference_rank}<br/>
                  - Search Mode: {searchMode} <br />
                </Card.Text>
                <Button style={{fontSize:"0.8em"}} variant="dark" onClick={() => this.props.toggle(7, this.props.data.teams.data[i].id)}>More Info</Button>
            </Card.Body>
            </Card>
        </Col>
        )
        }
    if (this.gamesFilter)
        for (let i = 0; i < this.page_size && this.props.data.games.data[i]; i++){
            let searchMode = this.getSearchStatus(this.props.data.games.data[i]);


            let h1 = "";
            let h2 = "";
            let h3 = "";
            let h4 = "";
            let h5 = "";
            let h6 = "";
            let h7 = "";

            if(searchMode !== "No Search") {

              let d = this.props.data.games.data[i];
              for(let j = 0; j < q.length; j++) {
                let check = q[j].toLowerCase();

                if( (this.formatDate(d.date) + "").includes(check) ) h1 = "yellow";
                if( ((d.season + "").includes(check)) ) h2 = "yellow";
                if( (d.h_team_name.toLowerCase().includes(check)) || (d.v_team_name.toLowerCase().includes(check)) ) h3 = "yellow";
                if( ((d.h_score + "").includes(check)) || ((d.v_score + "").includes(check)) ) h4 = "yellow";
                if( ((d.h_fgm + "").includes(check)) || ((d.v_fgm + "").includes(check)) ) h5 = "yellow";
                if( ((d.h_fg3m + "").includes(check)) || ((d.v_fg3m + "").includes(check)) ) h6 = "yellow";
                if( ((d.h_ftm + "").includes(check)) || ((d.v_ftm + "").includes(check)) ) h7 = "yellow";
              }
            }


            search.gameTable.push(
                <tr onClick={() => this.props.toggle(2, this.props.data.games.data[i].id)}>
                    <td style={{backgroundColor:h1}}>{this.formatDate(this.props.data.games.data[i].date)}</td>
                    <td style={{backgroundColor:h2}}>{this.props.data.games.data[i].season}</td>
                    <td style={{backgroundColor:h3}}>{this.props.data.games.data[i].h_team_name} vs. {this.props.data.games.data[i].v_team_name}</td>
                    <td style={{backgroundColor:h4}}>{this.props.data.games.data[i].h_score} : {this.props.data.games.data[i].v_score}</td>
                    <td style={{backgroundColor:h5}}>{this.props.data.games.data[i].h_fgm} : {this.props.data.games.data[i].v_fgm}</td>
                    <td style={{backgroundColor:h6}}>{this.props.data.games.data[i].h_fg3m} : {this.props.data.games.data[i].v_fg3m}</td>
                    <td style={{backgroundColor:h7}}>{this.props.data.games.data[i].h_ftm} : {this.props.data.games.data[i].v_ftm}</td>
                    <td>{searchMode}</td>
                </tr>
            )
        } 
    return search;
  }

  /**
   * Sets the cards on the page in a row of 5
   * These are the cards that will be displayed 
   * on the page in that row
   * @param {*} search the grid of pre-constructed Cards
   * @returns the constructed row
   */
  setCards(cards){
    let rows = [];
    for(let i = 0; i < this.page_size / 5; i++) {
      let players = [];
      for(let j = i * 5; j < i * 5 + 5; j++) {
        players.push(cards[j]);
      }
      rows.push(
        <Row style={{backgroundColor:"", paddingLeft:"30px", paddingRight:"30px", paddingBottom:"50px"}}>       
          {players[0]}
          {players[1]}
          {players[2]}
          {players[3]}
          {players[4]}
        </Row>
        )
    }
    return rows;
  }

  /**
   * Filters by Teams, Players, and Games
   * Only renders for the option(s) that have 
   * been selected by the user
   * @param {*} filter the filter query for this search
   */
  setFilter(filter){
    this.playersFilter = false;
    this.teamsFilter = false;
    this.gamesFilter = false;
    if (filter === "players"){
      this.playersFilter = true;
    }
    else if (filter === "teams"){
      this.teamsFilter = true;
    }
    else if (filter === "games"){
      this.gamesFilter = true;
    }
    else { {/* reset case */}
      this.playersFilter = true;
      this.teamsFilter = true;
      this.gamesFilter = true;
    }
    this.props.search(1)
  }

  render() {
    let search = this.renderPage();
    let players;
    if (this.playersFilter)
      players = 
        <div>
          <Row style={{backgroundColor:"", Height:"fit-content", paddingTop:"30px", paddingLeft:"30px"}}>
            <h1>Players</h1>
          </Row>

          {this.setCards(search.playerCards)} 
        </div>

    let teams;
    if (this.teamsFilter)
      teams =
      <div>
        <Row style={{backgroundColor:"", Height:"fit-content", paddingTop:"30px", paddingLeft:"30px"}}>
          <h1>Teams</h1>
        </Row>

        {this.setCards(search.teamCards)} 
      </div>

    let games; 
    if (this.gamesFilter)
      games =
      <div>
        <Row style={{backgroundColor:"", Height:"fit-content", paddingTop:"30px", paddingLeft:"30px"}}>
          <h1>Games</h1>
        </Row>

        <Row style={{paddingLeft:"30px", paddingRight:"30px"}}>
            <Table striped bordered hover>
                <thead>
                    <tr>
                    <th>Date</th>
                    <th>Season</th>
                    <th>Teams (Home vs. Visitor)</th>
                    <th>Score</th>
                    <th>Field Goals</th>
                    <th>Three Pointers</th>
                    <th>Free Throws</th>
                    <th>Search Mode</th>
                    </tr>
                </thead>
                <tbody>
                    {search.gameTable}
                </tbody>
            </Table>
        </Row>
      </div>

      return (
        <div style={{overflow:"auto", height:"93vh"}}>
          <Row style={{ backgroundColor:"", Height:"fit-content", paddingLeft:"30px", paddingBottom:"15px", paddingRight:'50px', justifyContent: "flex-end"}}>
              <DropdownButton id="dropdown-basic-button" variant="secondary" title="Filter By" style={{paddingRight:'10px' }}>
                  <Dropdown.Item onClick={() => this.setFilter("players")}>Players</Dropdown.Item>
                  <Dropdown.Item onClick={() => this.setFilter("teams")}>Teams</Dropdown.Item>
                  <Dropdown.Item onClick={() => this.setFilter("games")}>Games</Dropdown.Item>
                  <Dropdown.Item onClick={() => this.setFilter("reset")}>Reset</Dropdown.Item>
              </DropdownButton>
          </Row>

          {players}

          {teams}

          {games}

          <Row style={{paddingBottom:"30px"}}>
            <Col style={{ display: "flex", justifyContent: "center" }}>
              <Pagination className="mx-auto my-2">
                <Pagination.First onClick={() => this.props.search(1)}/>
                <Pagination.Prev onClick={() => this.props.search(this.props.data.current_page-1)}/>
                {this.setPaginationBar()}   
                <Pagination.Next onClick={() => this.props.search(this.props.data.current_page+1)}/> 
                <Pagination.Last onClick={() => this.props.search(this.props.data.total)} />
              </Pagination>
            </Col>
          </Row>

        </div>    
      );
  }
}

export default SearchResults;