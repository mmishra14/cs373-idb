import React from 'react';
import { Row, Col, Table, Pagination, Dropdown, DropdownButton, Form, FormControl, Button } from 'react-bootstrap'
import { DropdownSubmenu } from "react-bootstrap-submenu";
import moment from "moment"

// range function implemented for ease of implementation
// in later parts
const range = (from, to, step = 1) => {
    let i = from;
    const range = [];
  
    while (i <= to) {
      range.push(i);
      i += step;
    }
  
    return range;
  }

class Games extends React.Component {
    constructor(){
        super();
        this.state = {
            data: [],
            meta: { current_page: 1, total_pages: 110 }
        }
        this.page_size = 10;
        this.date_filter = "";
        this.season_filter = "";
        this.h_team_filter = "";
        this.v_team_filter = "";
        this.h_score_filter = "";
        this.v_score_filter = "";
        this.h_fg_filter = "";
        this.v_fg_filter = "";
        this.h_3p_filter = "";
        this.v_3p_filter = "";
        this.h_ft_filter = "";
        this.v_ft_filter = "";
        this.sort_by = "";
        this.search = "";

        this.setPaginationBar = this.setPaginationBar.bind(this);
        this.changePageState = this.changePageState.bind(this);
        this.renderPage = this.renderPage.bind(this);
        this.setFilter = this.setFilter.bind(this);
        this.setSort = this.setSort.bind(this);
        this.setPageSize = this.setPageSize.bind(this);
        this.formatDate = this.formatDate.bind(this);
    }

    /**
     * Will be called as soon as this component is rendered
     * Call the backend API upon render to fetch all necessary data
     */
    componentDidMount() {
        fetch("https://nbatoday.xyz/api/games?per_page=10&page=1")
            .then(response => response.json())
            .then(response => {
                if(response.data.length !== 0) this.setState({ data: response.data, meta: response.meta })
                else this.setState({ data: response.data, meta: { current_page: 1, total_pages: 110 } })
            });
    }

    /**
     * Builds the pagination bar at the bottom of the page
     * Will be called on every render of this component
     * @returns the pagination bar to be rendered
     */
    setPaginationBar(){
        let items = [];
        let total = this.state.meta.total_pages;
        if (this.state.meta.current_page <= 2){
          for (let i = 1; i <= 5; i++){
            items.push( 
              <Pagination.Item key={i} active={i === this.state.meta.current_page} onClick={() => {this.changePageState(i);}}>
                {i}
              </Pagination.Item>
            )
          }
        }
        else if (this.state.meta.current_page >= total - 2){
          for (let i = total - 4; i <= total; i++){
            items.push( 
              <Pagination.Item key={i} active={i === this.state.meta.current_page} onClick={() => {this.changePageState(i);}}>
                {i}
              </Pagination.Item>
            )
          }
        }
        else {
          let start = this.state.meta.current_page - 2;
          let end = this.state.meta.current_page + 2;
          for (const i of range(start, end)){
            items.push( 
            <Pagination.Item key={i} active={i === this.state.meta.current_page} onClick={() => {this.changePageState(i);}}>
              {i}
            </Pagination.Item>
            )
          }
        }
        return items
    }

    /**
     * Fetches data from the backend using filtering, sorting, and pagination queries
     * Called whenever the user sets filtering, sorting, and page size
     * @param {*} page the page # that should be fetched from backend
     */
    changePageState(page){
        if(page > 0 && page < this.state.meta.total_pages + 1) {
          let link = "https://nbatoday.xyz/api/games?per_page=" + this.page_size + "&page=" + page + this.date_filter + this.season_filter +
            this.h_team_filter + this.v_team_filter + this.h_score_filter + this.v_score_filter + this.h_fg_filter + this.v_fg_filter + this.h_3p_filter +
            this.v_3p_filter + this.h_ft_filter + this.v_ft_filter + this.sort_by;
          if (this.search.length != 0){
              link += "&q=" + this.search;
          }
          fetch(link)
              .then(response => response.json())
              .then(response => {
                  if(response.data.length !== 0) this.setState({ data: response.data, meta: response.meta })
                  else this.setState({ data: response.data, meta: { current_page: 1, total_pages: 110 } })
              });
        }
    }

    /**
     * to format the date given by backend in a more user-friendly style
     * @returns the formatted date
     */
    formatDate(date) {
      let time = moment(date);
      let time_formatted = time.format("YYYY-MM-DD");
      return time_formatted;
    }

    /**
     * Contstructs the set of rows to be displayed in the Games
     * table on the Games page
     * @returns the rows to be rendered for the Games table
     */
    renderPage(){ 
        let rows = [];
        if (this.state.data.length === 0){
          return rows;
        }
        let q = this.search.split(" ");
        for (let i = 0; i < this.page_size && this.state.data[i]; i++){
            let searchMode = this.getSearchStatus(this.state.data[i]);

            let h1 = "";
            let h2 = "";
            let h3 = "";
            let h4 = "";
            let h5 = "";
            let h6 = "";
            let h7 = "";

            if(searchMode !== "No Search") {
              let d = this.state.data[i];
              for(let j = 0; j < q.length; j++) {
                let check = q[j].toLowerCase();

                if( (this.formatDate(d.date) + "").includes(check) ) h1 = "yellow";
                if( ((d.season + "").includes(check)) ) h2 = "yellow";
                if( (d.h_team_name.toLowerCase().includes(check)) || (d.v_team_name.toLowerCase().includes(check)) ) h3 = "yellow";
                if( ((d.h_score + "").includes(check)) || ((d.v_score + "").includes(check)) ) h4 = "yellow";
                if( ((d.h_fgm + "").includes(check)) || ((d.v_fgm + "").includes(check)) ) h5 = "yellow";
                if( ((d.h_fg3m + "").includes(check)) || ((d.v_fg3m + "").includes(check)) ) h6 = "yellow";
                if( ((d.h_ftm + "").includes(check)) || ((d.v_ftm + "").includes(check)) ) h7 = "yellow";
              }
            }


            rows.push(
                <tr onClick={() => this.props.toggle(2, this.state.data[i].id)}>
                    <td style={{backgroundColor:h1}}>{this.formatDate(this.state.data[i].date)}</td>
                    <td style={{backgroundColor:h2}}>{this.state.data[i].season}</td>
                    <td style={{backgroundColor:h3}}>{this.state.data[i].h_team_name} vs. {this.state.data[i].v_team_name}</td>
                    <td style={{backgroundColor:h4}}>{this.state.data[i].h_score} : {this.state.data[i].v_score}</td>
                    <td style={{backgroundColor:h5}}>{this.state.data[i].h_fgm} : {this.state.data[i].v_fgm}</td>
                    <td style={{backgroundColor:h6}}>{this.state.data[i].h_fg3m} : {this.state.data[i].v_fg3m}</td>
                    <td style={{backgroundColor:h7}}>{this.state.data[i].h_ftm} : {this.state.data[i].v_ftm}</td>
                    <td>{searchMode}</td>
                </tr>
            );
        }   
        return rows
    }

     /**
      * 
      * @param {*} currentGame is a single instance of a team
      * @returns type of search it is: AND, OR, or NO SEARCH
      */
      getSearchStatus(currentGame) {
        if(currentGame.search_result === "and") {
          return "AND Search";
        } else if(currentGame.search_result === "or") {
          return "OR Search";
        }
        return "No Search";
      }

    /**
     * Creates a filtering string query
     * @param {*} attribute the attribute to check against for filtering
     * @param {*} filter the actual filter value to be applied
     */
    setFilter(attribute, filter){
        if (attribute === "date"){
          this.date_filter = "&date=" + filter;
        }
        else if (attribute === "season"){
          this.season_filter = "&season=" + filter;
        }
        else if (attribute === "h_team"){
          this.h_team_filter = "&h_team_id=" + filter;
        }
        else if (attribute === "v_team"){
          this.v_team_filter = "&v_team_id=" + filter;
        }
        else if (attribute === "h_score"){
          this.h_score_filter = "&h_score=" + filter;
        }
        else if (attribute === "v_score"){
            this.v_score_filter = "&v_score=" + filter;
        }
        else if (attribute === "h_fg"){
            this.h_fg_filter = "&h_fgm=" + filter;
        }
        else if (attribute === "v_fg"){
            this.v_fg_filter = "&v_fgm=" + filter;
        }
        else if (attribute === "h_3p"){
            this.h_3p_filter = "&h_fg3m=" + filter;
        }
        else if (attribute === "v_3p"){
            this.v_3p_filter = "&v_fg3m=" + filter;
        }
        else if (attribute === "h_ft"){
            this.h_ft_filter = "&h_ftm=" + filter;
        }
        else if (attribute === "v_ft"){
            this.v_ft_filter = "&v_ftm=" + filter;
        } else { {/* reset option */}
            this.date_filter = "";
            this.season_filter = "";
            this.h_team_filter = "";
            this.v_team_filter = "";
            this.h_score_filter = "";
            this.v_score_filter = "";
            this.h_fg_filter = "";
            this.v_fg_filter = "";
            this.h_3p_filter = "";
            this.v_3p_filter = "";
            this.h_ft_filter = "";
            this.v_ft_filter = "";
        }
        this.changePageState(1)
    }

    /**
     * Constructs the sorting query for fetching data from backedn
     * @param {*} filter the filter/sorting attribute to be applied
     * @param {*} direction ascending/descending direction
     */
    setSort(filter, direction){
        if (filter === "reset"){
          this.sort_by = "";
        }
        else {
          this.sort_by = "&sort_by=" + filter + "&sort_order=" + direction;
        }
        this.changePageState(1)
    }

    /**
     * Sets the size of the current page to be rendered
     * @param {*} size the size of the current page
     */
    setPageSize(size) {
        this.page_size = size;
        this.changePageState(1)
    }

  /**
   * Sets the search criteria for the games
   * Also resets the filters and sort options
   * since the search needs to be fresh
   */
    setSearch(){
      this.sortBy = "";
      this.setFilter("reset", "");
    }

    render() {

        return(
            <div style={{overflow:"auto", height:"93vh"}}>
                <Row style={{paddingLeft:"30px", paddingRight:"30px", paddingBottom:"30px"}}><h1>Games</h1></Row>
                
                <Row style={{ backgroundColor:"", Height:"fit-content", paddingLeft:"30px", paddingBottom:"15px", paddingRight:'50px', justifyContent: "flex-end"}}>
                    {/* how to filter by dates */}
                    <DropdownButton id="dropdown-basic-button" variant="secondary" title="Filter By" style={{paddingRight:'10px' }}>
                        <DropdownSubmenu id="dropdown-basic-button" title="Date">
                        <Dropdown.Item onClick={() => this.setFilter("date", "ge_2020-01-01")}>Jan. 2020 - Present</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setFilter("date", "ge_2020-02-01")}>Feb. 2020 - Present</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setFilter("date", "ge_2020-03-01")}>Mar. 2020 - Present</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setFilter("date", "ge_2020-04-01")}>Apr. 2020 - Present</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setFilter("date", "ge_2020-05-01")}>May 2020 - Present</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setFilter("date", "ge_2020-06-01")}>Jun. 2020 - Present</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setFilter("date", "ge_2020-07-01")}>Jul. 2020 - Present</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setFilter("date", "ge_2020-08-01")}>Aug. 2020 - Present</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setFilter("date", "ge_2020-09-01")}>Sept. 2020 - Present</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setFilter("date", "ge_2020-10-01")}>Oct. 2020 - Present</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setFilter("date", "ge_2020-11-01")}>Nov. 2020 - Present</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setFilter("date", "ge_2020-12-01")}>Dec. 2020 - Present</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setFilter("date", "ge_2021-01-01")}>Jan. 2021 - Present</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setFilter("date", "ge_2021-02-01")}>Feb. 2021 - Present</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setFilter("date", "ge_2021-03-01")}>Mar. 2021 - Present</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setFilter("date", "ge_2021-04-01")}>Apr. 2021 - Present</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setFilter("date", "ge_2021-05-01")}>May 2021 - Present</Dropdown.Item>
                        </DropdownSubmenu>
                        <DropdownSubmenu id="dropdown-basic-button" title="Season">
                        <Dropdown.Item onClick={() => this.setFilter("season", "ge_2021")}>2021 - Present</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setFilter("season", "ge_2020")}>2020 - Present</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setFilter("season", "ge_2019")}>2019 - Present</Dropdown.Item>
                        </DropdownSubmenu>
                        <DropdownSubmenu id="dropdown-basic-button" title="Home Team" style={{overflowY: 'scroll'}}>
                        <Dropdown.Item onClick={() => this.setFilter("h_team", "1")}>Atlanta Hawks</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setFilter("h_team", "2")}>Boston Celtics</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setFilter("h_team", "3")}>Brooklyn Nets</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setFilter("h_team", "4")}>Charlotte Hornets</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setFilter("h_team", "5")}>Chicago Bulls</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setFilter("h_team", "6")}>Cleveland Cavaliers</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setFilter("h_team", "7")}>Dallas Maveriks</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setFilter("h_team", "8")}>Denver Nuggets</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setFilter("h_team", "9")}>Detroit Pistons</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setFilter("h_team", "10")}>Golden State Warriors</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setFilter("h_team", "11")}>Houston Rockets</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setFilter("h_team", "12")}>Indiana Pacers</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setFilter("h_team", "13")}>LA Clippers</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setFilter("h_team", "14")}>Los Angeles Lakers</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setFilter("h_team", "15")}>Memphis Grizzlies</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setFilter("h_team", "16")}>Miami Heat</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setFilter("h_team", "17")}>Milwaukee Bucks</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setFilter("h_team", "18")}>Minnesota Timberwolves</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setFilter("h_team", "19")}>New Orleans Pelicans</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setFilter("h_team", "20")}>New York Knicks</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setFilter("h_team", "21")}>Oklahoma City Thunder</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setFilter("h_team", "22")}>Orlando Magic</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setFilter("h_team", "23")}>Philadelphia 76ers</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setFilter("h_team", "24")}>Pheonix Suns</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setFilter("h_team", "25")}>Portland Trail Blazers</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setFilter("h_team", "26")}>Sacramento Kings</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setFilter("h_team", "27")}>San Antonio Sputs</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setFilter("h_team", "28")}>Toronto Raptors</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setFilter("h_team", "29")}>Utah Jazz</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setFilter("h_team", "30")}>Washington Wizards</Dropdown.Item>
                        </DropdownSubmenu>
                        <DropdownSubmenu id="dropdown-basic-button" title="Visitor Team" style={{overflowY: 'scroll'}}>
                        <Dropdown.Item onClick={() => this.setFilter("v_team", "1")}>Atlanta Hawks</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setFilter("v_team", "2")}>Boston Celtics</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setFilter("v_team", "3")}>Brooklyn Nets</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setFilter("v_team", "4")}>Charlotte Hornets</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setFilter("v_team", "5")}>Chicago Bulls</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setFilter("v_team", "6")}>Cleveland Cavaliers</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setFilter("v_team", "7")}>Dallas Maveriks</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setFilter("v_team", "8")}>Denver Nuggets</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setFilter("v_team", "9")}>Detroit Pistons</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setFilter("v_team", "10")}>Golden State Warriors</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setFilter("v_team", "11")}>Houston Rockets</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setFilter("v_team", "12")}>Indiana Pacers</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setFilter("v_team", "13")}>LA Clippers</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setFilter("v_team", "14")}>Los Angeles Lakers</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setFilter("v_team", "15")}>Memphis Grizzlies</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setFilter("v_team", "16")}>Miami Heat</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setFilter("v_team", "17")}>Milwaukee Bucks</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setFilter("v_team", "18")}>Minnesota Timberwolves</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setFilter("v_team", "19")}>New Orleans Pelicans</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setFilter("v_team", "20")}>New York Knicks</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setFilter("v_team", "21")}>Oklahoma City Thunder</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setFilter("v_team", "22")}>Orlando Magic</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setFilter("v_team", "23")}>Philadelphia 76ers</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setFilter("v_team", "24")}>Pheonix Suns</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setFilter("v_team", "25")}>Portland Trail Blazers</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setFilter("v_team", "26")}>Sacramento Kings</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setFilter("v_team", "27")}>San Antonio Sputs</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setFilter("v_team", "28")}>Toronto Raptors</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setFilter("v_team", "29")}>Utah Jazz</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setFilter("v_team", "30")}>Washington Wizards</Dropdown.Item>
                        </DropdownSubmenu>
                        <DropdownSubmenu id="dropdown-basic-button" title="Home Score">
                        <Dropdown.Item onClick={() => this.setFilter("h_score", "ge_20")}>20+</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setFilter("h_score", "ge_40")}>40+</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setFilter("h_score", "ge_60")}>60+</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setFilter("h_score", "ge_80")}>80+</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setFilter("h_score", "ge_100")}>100+</Dropdown.Item>
                        </DropdownSubmenu>
                        <DropdownSubmenu id="dropdown-basic-button" title="Visitor Score">
                        <Dropdown.Item onClick={() => this.setFilter("v_score", "ge_20")}>20+</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setFilter("v_score", "ge_40")}>40+</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setFilter("v_score", "ge_60")}>60+</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setFilter("v_score", "ge_80")}>80+</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setFilter("v_score", "ge_100")}>100+</Dropdown.Item>
                        </DropdownSubmenu>
                        <DropdownSubmenu id="dropdown-basic-button" title="Home Field Goals">
                        <Dropdown.Item onClick={() => this.setFilter("h_fg", "ge_2")}>2+</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setFilter("h_fg", "ge_5")}>5+</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setFilter("h_fg", "ge_7")}>7+</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setFilter("h_fg", "ge_10")}>10+</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setFilter("h_fg", "ge_15")}>15+</Dropdown.Item>
                        </DropdownSubmenu>
                        <DropdownSubmenu id="dropdown-basic-button" title="Visitor Field Goals">
                        <Dropdown.Item onClick={() => this.setFilter("v_fg", "ge_2")}>2+</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setFilter("v_fg", "ge_5")}>5+</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setFilter("v_fg", "ge_7")}>7+</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setFilter("v_fg", "ge_10")}>10+</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setFilter("v_fg", "ge_15")}>15+</Dropdown.Item>
                        </DropdownSubmenu>
                        <DropdownSubmenu id="dropdown-basic-button" title="Home 3-Pointers">
                        <Dropdown.Item onClick={() => this.setFilter("h_3p", "ge_2")}>2+</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setFilter("h_3p", "ge_5")}>5+</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setFilter("h_3p", "ge_7")}>7+</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setFilter("h_3p", "ge_10")}>10+</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setFilter("h_3p", "ge_15")}>15+</Dropdown.Item>
                        </DropdownSubmenu>
                        <DropdownSubmenu id="dropdown-basic-button" title="Visitor 3-Pointers">
                        <Dropdown.Item onClick={() => this.setFilter("v_3p", "ge_2")}>2+</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setFilter("v_3p", "ge_5")}>5+</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setFilter("v_3p", "ge_7")}>7+</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setFilter("v_3p", "ge_10")}>10+</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setFilter("v_3p", "ge_15")}>15+</Dropdown.Item>
                        </DropdownSubmenu>
                        <DropdownSubmenu id="dropdown-basic-button" title="Home Free Throws">
                        <Dropdown.Item onClick={() => this.setFilter("h_ft", "ge_2")}>2+</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setFilter("h_ft", "ge_5")}>5+</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setFilter("h_ft", "ge_7")}>7+</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setFilter("h_ft", "ge_10")}>10+</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setFilter("h_ft", "ge_15")}>15+</Dropdown.Item>
                        </DropdownSubmenu>
                        <DropdownSubmenu id="dropdown-basic-button" title="Visitor Free Throws">
                        <Dropdown.Item onClick={() => this.setFilter("v_ft", "ge_2")}>2+</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setFilter("v_ft", "ge_5")}>5+</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setFilter("v_ft", "ge_7")}>7+</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setFilter("v_ft", "ge_10")}>10+</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setFilter("v_ft", "ge_15")}>15+</Dropdown.Item>
                        </DropdownSubmenu>
                        <Dropdown.Item onClick={() => this.setFilter("reset", "")}>Reset</Dropdown.Item>
                    </DropdownButton>

                    <DropdownButton id="dropdown-basic-button" variant="secondary" title="Sort By" style={{paddingRight:'10px'}}>
                        <DropdownSubmenu id="dropdown-basic-button" title="Date">
                        <Dropdown.Item onClick={() => this.setSort("date", "ascending")}>Ascending</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setSort("date", "descending")}>Descending</Dropdown.Item>
                        </DropdownSubmenu>
                        <DropdownSubmenu id="dropdown-basic-button" title="Season">
                        <Dropdown.Item onClick={() => this.setSort("season", "ascending")}>Ascending</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setSort("season", "descending")}>Descending</Dropdown.Item>
                        </DropdownSubmenu>
                        <DropdownSubmenu id="dropdown-basic-button" title="Home Team">
                        <Dropdown.Item onClick={() => this.setSort("h_team_id", "ascending")}>Ascending</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setSort("h_team_id", "descending")}>Descending</Dropdown.Item>
                        </DropdownSubmenu>
                        <DropdownSubmenu id="dropdown-basic-button" title="Visitor Team">
                        <Dropdown.Item onClick={() => this.setSort("v_team_id", "ascending")}>Ascending</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setSort("v_team_id", "descending")}>Descending</Dropdown.Item>
                        </DropdownSubmenu>
                        <DropdownSubmenu id="dropdown-basic-button" title="Home Score">
                        <Dropdown.Item onClick={() => this.setSort("h_score", "ascending")}>Ascending</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setSort("h_score", "descending")}>Descending</Dropdown.Item>
                        </DropdownSubmenu>
                        <DropdownSubmenu id="dropdown-basic-button" title="Visitor Score">
                        <Dropdown.Item onClick={() => this.setSort("v_score", "ascending")}>Ascending</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setSort("v_score", "descending")}>Descending</Dropdown.Item>
                        </DropdownSubmenu>
                        <DropdownSubmenu id="dropdown-basic-button" title="Home Field Goals">
                        <Dropdown.Item onClick={() => this.setSort("h_fgm", "ascending")}>Ascending</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setSort("h_fgm", "descending")}>Descending</Dropdown.Item>
                        </DropdownSubmenu>
                        <DropdownSubmenu id="dropdown-basic-button" title="Visitor Field Goals">
                        <Dropdown.Item onClick={() => this.setSort("v_fgm", "ascending")}>Ascending</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setSort("v_fgm", "descending")}>Descending</Dropdown.Item>
                        </DropdownSubmenu>
                        <DropdownSubmenu id="dropdown-basic-button" title="Home 3-Pointers">
                        <Dropdown.Item onClick={() => this.setSort("h_fg3m", "ascending")}>Ascending</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setSort("h_fg3m", "descending")}>Descending</Dropdown.Item>
                        </DropdownSubmenu>
                        <DropdownSubmenu id="dropdown-basic-button" title="Visitor 3-Pointers">
                        <Dropdown.Item onClick={() => this.setSort("v_fg3m", "ascending")}>Ascending</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setSort("v_fg3m", "descending")}>Descending</Dropdown.Item>
                        </DropdownSubmenu>
                        <DropdownSubmenu id="dropdown-basic-button" title="Home Free Throws">
                        <Dropdown.Item onClick={() => this.setSort("h_ftm", "ascending")}>Ascending</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setSort("h_ftm", "descending")}>Descending</Dropdown.Item>
                        </DropdownSubmenu>
                        <DropdownSubmenu id="dropdown-basic-button" title="Visitor Free Throws">
                        <Dropdown.Item onClick={() => this.setSort("v_ftm", "ascending")}>Ascending</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setSort("v_ftm", "descending")}>Descending</Dropdown.Item>
                        </DropdownSubmenu>
                        <Dropdown.Item onClick={() => this.setSort("reset", "")}>Reset</Dropdown.Item>
                    </DropdownButton>

                    <DropdownButton id="dropdown-basic-button" variant="secondary" title="Games per Page">
                        <Dropdown.Item onClick={() => this.setPageSize(5)}>5</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setPageSize(10)}>10</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setPageSize(25)}>25</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setPageSize(50)}>50</Dropdown.Item>
                    </DropdownButton>

                    <Form className="d-flex" style={{paddingLeft: '15px', paddingRight: '10px'}}>
                      <FormControl
                        type="search"
                        placeholder="Search Teams"
                        className="mr-2"
                        aria-label="Search"
                        onChange={e => this.search = e.target.value}
                        onKeyPress={e => {
                          console.log(e.key)
                          if (e.key === "Enter") {
                            e.preventDefault();
                            this.setSearch();
                          }
                        }}
                      />
                      <Button variant="outline-dark" onClick={() => this.setSearch()}>Search</Button>
                    </Form>

                </Row>
                
                <Row style={{paddingLeft:"30px", paddingRight:"30px"}}>
                    <Table striped bordered hover>
                        <thead>
                            <tr>
                            <th>Date</th>
                            <th>Season</th>
                            <th>Teams (Home vs. Visitor)</th>
                            <th>Score</th>
                            <th>Field Goals</th>
                            <th>Three Pointers</th>
                            <th>Free Throws</th>
                            <th>Search Mode</th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.renderPage()}
                        </tbody>
                    </Table>
                </Row>

                <Row style={{paddingBottom:"30px"}}>
                    <Col style={{ display: "flex", justifyContent: "center" }}>
                        <Pagination className="mx-auto my-2">
                        <Pagination.First onClick={() => this.changePageState(1)}/>
                        <Pagination.Prev onClick={() => this.changePageState(this.state.meta.current_page-1)}/>
                        {this.setPaginationBar()}   
                        <Pagination.Next onClick={() => this.changePageState(this.state.meta.current_page+1)}/> 
                        <Pagination.Last onClick={() => this.changePageState(this.state.meta.total_pages)} />
                        </Pagination>
                    </Col>
                </Row>

            </div>
        );
    }
}

export default Games;