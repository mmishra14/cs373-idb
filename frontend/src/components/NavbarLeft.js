import React from 'react';
import '../css/NavbarLeft.css'
import { Col, Image } from 'react-bootstrap'
import NBATodayLogo from "../assets/img/NBATodayLogo.png"

class NavbarLeft extends React.Component {
    constructor() {
      super();
      this.state = {
  
      }
    }
  
    render() {
      return (
            <Col xs={2} className="navbar-left">
                <br /><br />
                <Image src = {NBATodayLogo} alt="NBAToday Logo" style={{width:"10rem", height:"10rem"}}/>
                <br />
                <br />
                <h1>NBAToday</h1>
                <br /><br />
                <button className="navbar-left-button" onClick={() => this.props.toggle(0)}>Home</button>
                <button className="navbar-left-button" onClick={() => this.props.toggle(1)}>Players</button>
                <button className="navbar-left-button" onClick={() => this.props.toggle(8)}>Games</button>
                <button className="navbar-left-button" onClick={() => this.props.toggle(3)}>Teams</button>
                <button className="navbar-left-button" onClick={this.props.booksAPI}>Provider Visualizations</button>
            </Col>
      );
    }
  }
  export default NavbarLeft;