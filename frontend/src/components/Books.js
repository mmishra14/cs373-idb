import React from 'react';
import { Bar, Doughnut } from 'react-chartjs-2';
import { Row, Col } from 'react-bootstrap';

class App extends React.Component {
  constructor() {
    super();
    this.state = {
      
    }
  }

  render() {

    const page_state = {
      labels: ['0-100', '100-200', '200-300', '300-400', '400-500', '500-600', '600-700', '700-800', '800-900',
               '900-1000', '1000-1100', '1100-1200', '1200-1300', '1300-1400', '>1400'],
      datasets: [
        {
          label: 'Page Count',
          backgroundColor: 'rgb(216, 123, 9)',
          borderColor: 'rgba(0,0,0,0.5)',
          borderWidth: 2,
          data: this.props.data.p_count
        }
      ]
    }

    const alpha_state = {
      labels: ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'Other'],
      datasets: [
        {
          label: 'Rainfall',
          backgroundColor: [
            '#0000CD',
            '#006400',
            '#007FFF',
            '#2E0854',
            '#2FAA96',
            '#362819',
            '#4F2F4F',
            '#551011',
            '#5C3317',
            '#615E3F',
            '#6C7B8B',
            '#777733',
            '#8B3A3A',
            '#8B7500',
            '#8DB6CD',
            '#92CCA6',
            '#9E0508',
            '#A2627A',
            '#A2BC13',
            '#B5A642',
            '#B7C8B6',
            '#BC8F8F',
            '#C1FFC1',
            '#CDC673',
            '#D1E231',
            '#D9D9F3',
            '#DFFFA5',
          ],
          data: this.props.data.a_count
        }
      ]
    }

    return (
        <div style={{overflow:"auto", height:"93vh"}}>

          <Row style={{backgroundColor:"", Height:"fit-content", paddingTop:"30px", paddingLeft:"30px", paddingBottom:"30px"}}>
              <Col><h1>Provider Visualizations</h1></Col>
          </Row>

          <Row style={{backgroundColor:"", Height:"fit-content", paddingLeft:"30px", paddingBottom:"20px"}}>
              <Col style={{textAlign:"center"}}><h3>Number of Books by Page Count Range</h3></Col>
          </Row>

          <Bar
            data={page_state}
            options={{
              title:{
                display:true,
                text:'Number of Books by Page Count Range',
                fontSize:20
              },
              legend:{
                display:true,
                position:'right'
              }
            }}
          />

          <Row style={{backgroundColor:"", Height:"fit-content", paddingTop:"20px", paddingLeft:"30px", paddingBottom:"20px"}}>
              <Col style={{textAlign:"center"}}><h3>Number of Books by Alphabet</h3></Col>
          </Row>
          
          <Doughnut
            data={alpha_state}
            options={{
              title:{
                display:true,
                text:'Number of Books by Alphabet',
                fontSize:20
              },
              legend:{
                display:true,
                position:'right'
              }
            }}
          />

        </div>
    );
  }
}
export default App;

