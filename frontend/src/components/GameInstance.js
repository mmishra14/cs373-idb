import React from "react";
import { Col, Row, Card, Button, Table } from 'react-bootstrap'
import moment from "moment"

class GameInstance extends React.Component {
  constructor() {
    super();
    this.state = {
      data: {}
    }
  }

  /**
   * Will be called as soon as this component is rendered
   * Call the backend API upon render to fetch all necessary data
   */
  componentDidMount() {
    fetch("https://nbatoday.xyz/api/games/" + this.props.instance)
        .then(response => response.json())
        .then(response => {
            this.setState({ data: response });
        });
  }
  
  render() {

    // to format the date given by backend in a more user-friendly style
    let formatDate = () => {
      let time = moment(this.state.data.date);
      let time_formatted = time.format("YYYY-MM-DD");
      return time_formatted;
    }

    return(
      <div style={{overflow:"auto", height:"93vh"}}> 
        <Row style={{backgroundColor:"", height:"fit-content", paddingTop:"30px", paddingLeft:"30px", paddingBottom:"50px"}}>
          <h1>{this.state.data.h_team_name} vs. {this.state.data.v_team_name}</h1>
        </Row>

        <Row style={{backgroundColor:"", height:"fit-content", paddingLeft:"30px", paddingRight:"30px", paddingBottom:"30px"}}>
          <Col style={{backgroundColor:""}}>
            <h2 style={{textAlign:"center"}}>Home</h2>
            <Card style={{ width: '23rem', margin:"0 auto"}}>
              {/* this should actually be the embed link that we get from backend */}
              <Card.Img variant="top" src={this.state.data.h_logo} style={{height:"18rem"}}/>
                <Card.Body>
                  <Card.Title>{this.state.data.h_team_name}</Card.Title>
                  <Button variant="dark" onClick={() => this.props.toggle(7,this.state.data.h_team_id)}>More Info</Button>
                </Card.Body>
            </Card>
            <br></br><br></br><br></br>
            <Table striped bordered>
              <tbody>
                  <tr><td><strong>Field Goals Made</strong></td><td>{this.state.data.h_fgm}</td></tr>
                  <tr><td><strong>Field Goals Attempted</strong></td><td>{this.state.data.h_fga}</td></tr>
                  <tr><td><strong>Three Pointers Made</strong></td><td>{this.state.data.h_fg3m}</td></tr>
                  <tr><td><strong>Three Pointers Attempted</strong></td><td>{this.state.data.h_fg3a}</td></tr>
                  <tr><td><strong>Free Throws Made</strong></td><td>{this.state.data.h_ftm}</td></tr>
                  <tr><td><strong>Free Throws Attempted</strong></td><td>{this.state.data.h_fta}</td></tr>   
              </tbody>
            </Table>
          </Col>

          <Col xs={2} style={{textAlign:"center", marginTop:"10rem"}}>
            <h3>{formatDate()}</h3>
            <h3>vs.</h3>
            <h3><i>{this.state.data.h_score} - {this.state.data.v_score}</i></h3>
          </Col>

          <Col style={{backgroundColor:""}}>
            <h2 style={{textAlign:"center"}}>Visitor</h2>
            <Card style={{ width: '23rem', margin:"0 auto"}}>
              {/* this should actually be the embed link that we get from backend */}
              <Card.Img variant="top" src={this.state.data.v_logo} style={{height:"18rem"}}/>
                <Card.Body>
                  <Card.Title>{this.state.data.v_team_name}</Card.Title>
                  <Button variant="dark" onClick={() => this.props.toggle(7,this.state.data.v_team_id)}>More Info</Button>
                </Card.Body>
              </Card>

              <br></br><br></br><br></br>
              <Table striped bordered>
                <tbody>
                <tr><td><strong>Field Goals Made</strong></td><td>{this.state.data.v_fgm}</td></tr>
                  <tr><td><strong>Field Goals Attempted</strong></td><td>{this.state.data.v_fga}</td></tr>
                  <tr><td><strong>Three Pointers Made</strong></td><td>{this.state.data.v_fg3m}</td></tr>
                  <tr><td><strong>Three Pointers Attempted</strong></td><td>{this.state.data.v_fg3a}</td></tr>
                  <tr><td><strong>Free Throws Made</strong></td><td>{this.state.data.v_ftm}</td></tr>
                  <tr><td><strong>Free Throws Attempted</strong></td><td>{this.state.data.v_fta}</td></tr>   
                </tbody>
              </Table>
          </Col>
        </Row>

        <Row style={{width:"100%", paddingLeft:"30px", paddingRight:"30px", paddingBottom:"30px", position:"relative", margin:"0 auto", display:"block"}}>
          <iframe src={this.state.data.googlemap_embed_link} style={{border:"0", margin:"0 auto", display:"block", width:"50vw", height:"500px", position:"relative"}} allowfullscreen="" loading="lazy"></iframe>
        </Row>

      </div>
    );
  }
}

export default GameInstance;
