import React from 'react';
import { Col, Row, Table } from 'react-bootstrap'
import '../css/PlayerInstance.css'
import moment from "moment";

class PlayerInstance extends React.Component {
    constructor() {
      super();

      // stores the content that will be rendered for 
      // this player, as determined by this.props.instance
      this.state = {
        data: [
          {
            "age": 28, 
            "average_points_per_game": 9.52284263959391, 
            "block_pct": null, 
            "current_team": 19, 
            "current_team_name": "New Orleans Pelicans", 
            "fg3_total": 1, 
            "fg_total": 2346, 
            "ft_total": 935, 
            "game_dates": ["Sun, 16 May 2021 00:00:00 GMT","Sun, 16 May 2021 00:00:00 GMT","Sun, 16 May 2021 00:00:00 GMT","Sun, 16 May 2021 00:00:00 GMT","Sun, 16 May 2021 00:00:00 GMT","Sun, 16 May 2021 00:00:00 GMT","Sun, 16 May 2021 00:00:00 GMT",],
            "game_ids": [
              264797, 
              264776, 
              264764, 
              264745, 
              264741, 
              264723, 
              264702, 
              264694, 
              264676, 
              264658, 
              264653, 
              264637, 
              264602, 
              264584, 
              264570, 
              264551, 
              264540, 
              264521, 
              264510, 
              264494, 
              264486, 
              264474, 
              264461, 
              264443, 
              264436, 
              264406, 
              264401, 
              264386, 
              264363, 
              264349, 
              264324, 
              264309, 
              264295, 
              264274, 
              264267, 
              128055, 
              128050, 
              128030, 
              128014, 
              128004, 
              127996, 
              127952, 
              127936, 
              127929, 
              219052, 
              127904, 
              127885, 
              127877, 
              127856, 
              127844, 
              127837, 
              127814, 
              127781, 
              127760, 
              127744, 
              127727, 
              127715, 
              127697, 
              127682, 
              156680, 
              127625, 
              127615, 
              127594, 
              127580, 
              127564, 
              127551, 
              127537, 
              127519, 
              127509
            ], 
            "game_names": [
              "New Orleans Pelicans vs Los Angeles Lakers", 
              "Golden State Warriors vs New Orleans Pelicans", 
              "Dallas Mavericks vs New Orleans Pelicans", 
              "Memphis Grizzlies vs New Orleans Pelicans", 
              "Charlotte Hornets vs New Orleans Pelicans", 
              "Philadelphia 76ers vs New Orleans Pelicans", 
              "New Orleans Pelicans vs Golden State Warriors", 
              "New Orleans Pelicans vs Golden State Warriors", 
              "Minnesota Timberwolves vs New Orleans Pelicans", 
              "Oklahoma City Thunder vs New Orleans Pelicans", 
              "Denver Nuggets vs New Orleans Pelicans", 
              "New Orleans Pelicans vs LA Clippers", 
              "Orlando Magic vs New Orleans Pelicans", 
              "New Orleans Pelicans vs Brooklyn Nets", 
              "New York Knicks vs New Orleans Pelicans", 
              "Washington Wizards vs New Orleans Pelicans", 
              "New Orleans Pelicans vs New York Knicks", 
              "New Orleans Pelicans vs Sacramento Kings", 
              "Cleveland Cavaliers vs New Orleans Pelicans", 
              "New Orleans Pelicans vs Philadelphia 76ers", 
              "Brooklyn Nets vs New Orleans Pelicans", 
              "Atlanta Hawks vs New Orleans Pelicans", 
              "Houston Rockets vs New Orleans Pelicans", 
              "New Orleans Pelicans vs Atlanta Hawks", 
              "New Orleans Pelicans vs Orlando Magic", 
              "Boston Celtics vs New Orleans Pelicans", 
              "New Orleans Pelicans vs Dallas Mavericks", 
              "New Orleans Pelicans vs Denver Nuggets", 
              "New Orleans Pelicans vs Los Angeles Lakers", 
              "Denver Nuggets vs New Orleans Pelicans", 
              "Portland Trail Blazers vs New Orleans Pelicans", 
              "Portland Trail Blazers vs New Orleans Pelicans", 
              "New Orleans Pelicans vs LA Clippers", 
              "New Orleans Pelicans vs Cleveland Cavaliers", 
              "New Orleans Pelicans vs Minnesota Timberwolves", 
              "New Orleans Pelicans vs Miami Heat", 
              "New Orleans Pelicans vs Chicago Bulls", 
              "New Orleans Pelicans vs Utah Jazz", 
              "San Antonio Spurs vs New Orleans Pelicans", 
              "Milwaukee Bucks vs New Orleans Pelicans", 
              "New Orleans Pelicans vs Detroit Pistons", 
              "New Orleans Pelicans vs Phoenix Suns", 
              "New Orleans Pelicans vs Portland Trail Blazers", 
              "Memphis Grizzlies vs New Orleans Pelicans", 
              "Detroit Pistons vs New Orleans Pelicans", 
              "Dallas Mavericks vs New Orleans Pelicans", 
              "Chicago Bulls vs New Orleans Pelicans", 
              "New Orleans Pelicans vs Houston Rockets", 
              "New Orleans Pelicans vs Memphis Grizzlies", 
              "Indiana Pacers vs New Orleans Pelicans", 
              "New Orleans Pelicans vs Phoenix Suns", 
              "New Orleans Pelicans vs Sacramento Kings", 
              "New Orleans Pelicans vs Washington Wizards", 
              "New Orleans Pelicans vs San Antonio Spurs", 
              "Minnesota Timberwolves vs New Orleans Pelicans", 
              "Utah Jazz vs New Orleans Pelicans", 
              "Utah Jazz vs New Orleans Pelicans", 
              "Sacramento Kings vs New Orleans Pelicans", 
              "Los Angeles Lakers vs New Orleans Pelicans", 
              "LA Clippers vs New Orleans Pelicans", 
              "New Orleans Pelicans vs Charlotte Hornets", 
              "New Orleans Pelicans vs Oklahoma City Thunder", 
              "New Orleans Pelicans vs Indiana Pacers", 
              "New Orleans Pelicans vs Toronto Raptors", 
              "Oklahoma City Thunder vs New Orleans Pelicans", 
              "Phoenix Suns vs New Orleans Pelicans", 
              "New Orleans Pelicans vs San Antonio Spurs", 
              "Miami Heat vs New Orleans Pelicans", 
              "Toronto Raptors vs New Orleans Pelicans"
            ], 
            "game_scores": ["98-110", "98-110","98-110","98-110","98-110","98-110","98-110","98-110","98-110"],
            "height": 83, 
            "id": 3, 
            "name": "Steven Adams", 
            "position": "Center", 
            "weight": 265
          }
        ], 
        "meta": {
          "current_page": 1, 
          "per_page": 25, 
          "total_items": 1, 
          "total_pages": 1
        }
      }

      this.formatDate = this.formatDate.bind(this);
    }

    // determine the content to be rendered right before 
    // this component is rendered
    componentDidMount() {
      let link = "https://nbatoday.xyz/api/players?id=" + this.props.instance;
      fetch(link)
      .then(response => response.json())
      .then(response => {
        console.log(response.data)
        this.setState({ data: response.data, meta: response.meta })
      });
    }

    /**
     * to format the date given by backend in a more user-friendly style
     * @returns the formatted date
     */
     formatDate(date) {
      let time = moment(date);
      let time_formatted = time.format("YYYY-MM-DD");
      return time_formatted;
    }

    /**
     * Constructs a row for the Recent Games
     * Played table
     * @returns a row constructed for Recent Games Played
     */
    generateTable() {
      let table = []
      for (let i = 0; i < 5 && i < this.state.data[0].game_ids.length; i++){
        table.push(
          <tr onClick={() => this.props.toggle(2, this.state.data[0].game_ids[i])}>
            <td>{this.formatDate(this.state.data[0].game_dates[i])}</td>
            <td>{this.state.data[0].game_names[i].split(" vs ")[0]}</td>
            <td>{this.state.data[0].game_names[i].split(" vs ")[1]}</td>
            <td>{this.state.data[0].game_scores[i]}</td>
          </tr>
        )
      }
      return table
    }

    render() {
        let statsStyle = { fontSize:"22px" }

        // determines the team in which a player is and then redirects
        // to the appropriate team page
        const goToTeam = () => {
          this.props.toggle(7, this.state.data[0].current_team_id);
        }

        return(
          <div style={{overflowX:"auto", overflowY:"auto", height:"93vh"}}> 
          <h1 style={{textAlign:"center"}}>{this.state.data[0].name}</h1>
            <Row style={{backgroundColor:"", height:"fit-content", paddingTop:"30px", paddingLeft:"30px", paddingBottom:"15px"}}>
                <Col><img className="d-block m-auto" src={this.state.data[0].headshot_embed_link} alt='' style={{width:"35rem", height:"25rem"}}/></Col>
                <Col>
                  <h2>Key Stats</h2>
                  <p style={statsStyle}>Age: {this.state.data[0].age}</p>
                  <p style={statsStyle}>Height: {this.state.data[0].height}</p>
                  <p style={statsStyle}>Weight: {this.state.data[0].weight}</p>
                  <p style={statsStyle}>Position: {this.state.data[0].position}</p>
                  <p style={statsStyle}>Field Goals Made: {this.state.data[0].fg_total}</p>
                  <p style={statsStyle}>Three Pointers Made: {this.state.data[0].fg3_total}</p>
                  <p style={statsStyle}>Free Throws Made: {this.state.data[0].ft_total}</p>
                  <p style={statsStyle} className="team" onClick={goToTeam}>Team: {this.state.data[0].current_team_name}</p>
                </Col>
            </Row>

            <Row style={{backgroundColor:"", height:"fit-content", paddingTop:"30px", paddingLeft:"30px", paddingBottom:"15px"}}><h2>Recent Games</h2></Row>
            <Row style={{backgroundColor:"", height:"fit-content", paddingLeft:"30px", paddingBottom:"15px"}}>
              <Table striped bordered hover style={{width:"95%"}}>
                <thead>
                  <tr>
                    <th>Date Played</th>
                    <th>Home</th>
                    <th>Away</th>
                    <th>Score</th>
                  </tr>
                </thead>
                <tbody>
                  {this.generateTable()}
                </tbody>
              </Table>
            </Row>

          </div>
        );
      }

}

export default PlayerInstance;