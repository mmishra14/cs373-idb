import React from "react";
import { Row, Col, Card, Button, Pagination, DropdownButton, ButtonGroup, Dropdown, Form, FormControl } from 'react-bootstrap';
import { DropdownSubmenu } from "react-bootstrap-submenu";
import '../css/Teams.css'

// range function implemented for ease of implementation
// in later parts
const range = (from, to, step = 1) => {
  let i = from;
  const range = [];

  while (i <= to) {
    range.push(i);
    i += step;
  }

  return range;
}

class Teams extends React.Component {
  constructor() {
    super();
    this.state = {
      data: [],
      meta: { current_page: 1, total_pages: 5}
    }
    this.page_size = 5;
    this.winsFilter = "";
    this.lossFilter = "";
    this.fgFilter = "";
    this.fg3Filter = "";
    this.ftFilter = "";
    this.cFilter = "";
    this.sortBy = "";
    this.search = "";


    this.renderPage = this.renderPage.bind(this);
    this.setPaginationBar = this.setPaginationBar.bind(this);
    this.changePageState = this.changePageState.bind(this);
    this.setSort = this.setSort.bind(this);
    this.setPageSize = this.setPageSize.bind(this);
  }

  /**
   * Will be called as soon as this component is rendered
   * Call the backend API upon render to fetch all necessary data
   */
  componentDidMount() {
    fetch("https://nbatoday.xyz/api/teams?per_page=5&page=1")
        .then(response => response.json())
        .then(response => {
          this.setState({ data: response.data, meta: response.meta })
        });
  }

  /**
   * Constructs the grid of Cards to be rendered on a page
   * Constructs a Card per team
   * @returns the grid of Cards to be rendered
   */
  renderPage() {
    if (this.state.data.length === 0){
      return [];
    }
    let teamCards = [];
    let q = this.search.split(" ");
    for (let i = 0; i < this.page_size && this.state.data[i]; i++){
        let searchStatus = this.getSearchStatus(this.state.data[i]);


        let highlightTitle = "";
        let highlightBody = "";

        if(searchStatus !== "No Search") {
          let d = this.state.data[i];
          for(let j = 0; j < q.length; j++) {
            let check = q[j].toLowerCase();

            if(d.name.toLowerCase().includes(check)) highlightTitle = "yellow";

            if (((d.wins + "").includes(check)) || 
                ((d.losses + "").includes(check)) ||
                ((d.fg_pct + "").includes(check)) ||
                ((d.fg3_pct + "").includes(check)) ||
                ((d.ft_pct + "").includes(check)) ||
                ((d.conference_rank + "").includes(check))) { highlightBody = "yellow"; }
          }
        }


        teamCards.push( 
          <Col style={{backgroundColor:""}}>
          <Card style={{margin:"0 auto", maxWidth:"16rem", maxHeight:"39rem"}}>
            <Card.Img variant="top" src={this.state.data[i].logo_embed_link} style={{height:"15rem"}}/>
              <Card.Body>
                <Card.Title style={{backgroundColor:highlightTitle}}>{this.state.data[i].name}</Card.Title>
                <Card.Text style={{backgroundColor:highlightBody}}>
                  - Wins: {this.state.data[i].wins} <br/>
                  - Losses: {this.state.data[i].losses} <br />
                  - FG percentage: {this.state.data[i].fg_pct}<br/>
                  - 3 point percentage: {this.state.data[i].fg3_pct}<br/>
                  - FT percentage: {this.state.data[i].ft_pct}<br/>
                  - Conference rank: {this.state.data[i].conference_rank}<br/>
                  {/*- Division rank: {this.state.data[i].division_rank} <br/>*/} {/* All of the Division ranks are 0 */}
                  {/* - Season: {this.state.data[i].season} <br/> WE CAN'T SORT BY THIS SINCE ALL SEASONS ARE THE SAME, SO REMOVING IT ENTIRELY FOR CLARITY*/}
                  - Search Mode: {searchStatus} <br />
                </Card.Text>
                <Button variant="dark" onClick={() => this.props.toggle(7, this.state.data[i].id)}>More Info</Button>
              </Card.Body>
            </Card>
        </Col>
        )
      //}
    }
    return teamCards
  }

  /**
   * 
   * @param {*} currentTeam is a single instance of a team
   * @returns type of search it is: AND, OR, or NO SEARCH
   */
  getSearchStatus(currentTeam) {
    if(currentTeam.search_result === "and") {
      return "AND Search";
    } else if(currentTeam.search_result === "or") {
      return "OR Search";
    }
    return "No Search";
  }

  /**
   * Sets the Cards on the page in a row of 5
   * These are the Cards that will be displayed 
   * on the page in that row
   * @param {*} teamArray the pre-constructed grid of Cards
   * @returns the grid of Cards
   */
  setCards(teamArray){
    let rows = [];
    for(let i = 0; i < this.page_size / 5; i++) {
      let teams = [];
      for(let j = i * 5; j < i * 5 + 5; j++) {
        teams.push(teamArray[j]);
      }
      rows.push(
        <Row style={{backgroundColor:"", paddingLeft:"30px", paddingRight:"30px", paddingBottom:"50px"}}>       
          {teams[0]}
          {teams[1]}
          {teams[2]}
          {teams[3]}
          {teams[4]}
        </Row>
        )
    }
    return rows;
  }

  /**
   * Creates a filtering string query
   * @param {*} attribute the attribute to check against for filtering
   * @param {*} filter the actual filter value to be applied
   */
  setFilter(attribute, filter){
    if (attribute === "wins"){
      this.winsFilter = "&wins=" + filter;
    }
    else if (attribute === "losses"){
      this.lossFilter = "&losses=" + filter;
    }
    else if (attribute === "fg_percent"){
      this.fgFilter = "&fg_pct=" + filter;
    }
    else if (attribute === "3p_percent"){
      this.fg3Filter = "&fg3_pct=" + filter;
    }
    else if (attribute === "ft_percent"){
      this.ftFilter = "&ft_pct=" + filter;
    }
    else if (attribute === "c_rank"){
      this.cFilter = "&conference_rank=" + filter;
    }
    else { {/* reset case */}
      this.winsFilter = "";
      this.lossFilter = "";
      this.fgFilter = "";
      this.fg3Filter = "";
      this.ftFilter = "";
      this.cFilter = "";
    }
    this.changePageState(1)
  }

  /**
   * Constructs the sorting query for fetching data from backedn
   * @param {*} filter the filter/sorting attribute to be applied
   * @param {*} direction ascending/descending direction
   */
  setSort(filter, direction){
    if (filter === "reset"){
      this.sortBy = "";
    }
    else {
      this.sortBy = "&sort_by=" + filter + "&sort_order=" + direction;
    }
    this.changePageState(1)
  }

  /**
   * Builds the pagination bar at the bottom of the page
   * Will be called on every render of this component
   * @returns the pagination bar to be rendered
   */
  setPaginationBar(){
    let items = [];
    let total = this.state.meta.total_pages;
    if (this.state.meta.current_page <= 2){
      for (let i = 1; i <= 5; i++){
        items.push( 
          <Pagination.Item key={i} active={i === this.state.meta.current_page} onClick={() => {this.changePageState(i);}}>
            {i}
          </Pagination.Item>
        )
      }
    }
    else if (this.state.meta.current_page >= total - 2){
      for (let i = total - 4; i <= total; i++){
        items.push( 
          <Pagination.Item key={i} active={i === this.state.meta.current_page} onClick={() => {this.changePageState(i);}}>
            {i}
          </Pagination.Item>
        )
      }
    }
    else {
      let start = this.state.meta.current_page - 2;
      let end = this.state.meta.current_page + 2;
      for (const i of range(start, end)){
        items.push( 
        <Pagination.Item key={i} active={i === this.state.meta.current_page} onClick={() => {this.changePageState(i);}}>
          {i}
        </Pagination.Item>
        )
      }
    }
    return items
  }

  /**
   * Fetches data from the backend using filtering, sorting, and pagination queries
   * Called whenever the user sets filtering, sorting, and page size
   * @param {*} page the page # that should be fetched from backend
   */
  changePageState(page){
    if(page > 0 && page < this.state.meta.total_pages + 1) {
      let link = "https://nbatoday.xyz/api/teams?per_page=" + this.page_size + "&page=" + page + this.winsFilter + this.lossFilter + this.fgFilter + this.fg3Filter + this.ftFilter + this.cFilter + this.sortBy;
      if (this.search.length != 0){
        link += "&q=" + this.search;
      }
      fetch(link)
        .then(response => response.json())
        .then(response => {
          if(response.data.length !== 0) this.setState({ data: response.data, meta: response.meta })
          else this.setState({ data: response.data, meta: { current_page: 1, total_pages: 5 } })
        });
    }
  }

  /**
   * Sets the size of the current page to be rendered
   * @param {*} size the size of the current page
   */
  setPageSize(size) {
    this.page_size = size;
    this.changePageState(1)
  }

  /**
   * Sets the search criteria for the teams
   * Also resets the filters and sort options
   * since the search needs to be fresh
   */
  setSearch(){
    this.sortBy = "";
    this.setFilter("reset", "");
  }

  render() {
    let teamArray = this.renderPage();
    return(
      
      <div style={{overflow:"auto", height:"93vh"}}>
        <Row style={{backgroundColor:"", Height:"fit-content", paddingTop:"30px", paddingLeft:"30px"}}>
          <h1>Teams</h1>
        </Row>

        <Row style={{ backgroundColor:"", Height:"fit-content", paddingLeft:"30px", paddingBottom:"15px", paddingRight:'50px', justifyContent: "flex-end"}}>

          <DropdownButton id="dropdown-basic-button" variant="secondary" title="Filter By" style={{paddingRight:'10px' }}>
            <DropdownSubmenu id="dropdown-basic-button" title="Wins">
              <Dropdown.Item onClick={() => this.setFilter("wins", "ge_5")}>5+</Dropdown.Item>
              <Dropdown.Item onClick={() => this.setFilter("wins", "ge_10")}>10+</Dropdown.Item>
              <Dropdown.Item onClick={() => this.setFilter("wins", "ge_15")}>15+</Dropdown.Item>
              <Dropdown.Item onClick={() => this.setFilter("wins", "ge_20")}>20+</Dropdown.Item>
              <Dropdown.Item onClick={() => this.setFilter("wins", "ge_25")}>25+</Dropdown.Item>
            </DropdownSubmenu>
            <DropdownSubmenu id="dropdown-basic-button" title="Losses">
              <Dropdown.Item onClick={() => this.setFilter("losses", "ge_5")}>5+</Dropdown.Item>
              <Dropdown.Item onClick={() => this.setFilter("losses", "ge_10")}>10+</Dropdown.Item>
              <Dropdown.Item onClick={() => this.setFilter("losses", "ge_15")}>15+</Dropdown.Item>
              <Dropdown.Item onClick={() => this.setFilter("losses", "ge_20")}>20+</Dropdown.Item>
              <Dropdown.Item onClick={() => this.setFilter("losses", "ge_25")}>25+</Dropdown.Item>
            </DropdownSubmenu>
            <DropdownSubmenu id="dropdown-basic-button" title="Field Goal %">
              <Dropdown.Item onClick={() => this.setFilter("fg_percent", "ge_0.1")}>0.10+</Dropdown.Item>
              <Dropdown.Item onClick={() => this.setFilter("fg_percent", "ge_0.2")}>0.20+</Dropdown.Item>
              <Dropdown.Item onClick={() => this.setFilter("fg_percent", "ge_0.3")}>0.30+</Dropdown.Item>
              <Dropdown.Item onClick={() => this.setFilter("fg_percent", "ge_0.4")}>0.40+</Dropdown.Item>
              <Dropdown.Item onClick={() => this.setFilter("fg_percent", "ge_0.5")}>0.50+</Dropdown.Item>
            </DropdownSubmenu>
            <DropdownSubmenu id="dropdown-basic-button" title="3-Point %">
              <Dropdown.Item onClick={() => this.setFilter("3p_percent", "ge_0.1")}>0.10+</Dropdown.Item>
              <Dropdown.Item onClick={() => this.setFilter("3p_percent", "ge_0.2")}>0.20+</Dropdown.Item>
              <Dropdown.Item onClick={() => this.setFilter("3p_percent", "ge_0.3")}>0.30+</Dropdown.Item>
              <Dropdown.Item onClick={() => this.setFilter("3p_percent", "ge_0.4")}>0.40+</Dropdown.Item>
              <Dropdown.Item onClick={() => this.setFilter("3p_percent", "ge_0.5")}>0.50+</Dropdown.Item>
            </DropdownSubmenu>
            <DropdownSubmenu id="dropdown-basic-button" title="Free Throw %">
              <Dropdown.Item onClick={() => this.setFilter("ft_percent", "ge_0.1")}>0.10+</Dropdown.Item>
              <Dropdown.Item onClick={() => this.setFilter("ft_percent", "ge_0.2")}>0.20+</Dropdown.Item>
              <Dropdown.Item onClick={() => this.setFilter("ft_percent", "ge_0.3")}>0.30+</Dropdown.Item>
              <Dropdown.Item onClick={() => this.setFilter("ft_percent", "ge_0.4")}>0.40+</Dropdown.Item>
              <Dropdown.Item onClick={() => this.setFilter("ft_percent", "ge_0.5")}>0.50+</Dropdown.Item>
            </DropdownSubmenu>
            <DropdownSubmenu id="dropdown-basic-button" title="Conference Rank">
              <Dropdown.Item onClick={() => this.setFilter("c_rank", "le_10")}>Top 10</Dropdown.Item>
              <Dropdown.Item onClick={() => this.setFilter("c_rank", "le_20")}>Top 20</Dropdown.Item>
              <Dropdown.Item onClick={() => this.setFilter("c_rank", "le_30")}>Top 30</Dropdown.Item>
              <Dropdown.Item onClick={() => this.setFilter("c_rank", "le_40")}>Top 40</Dropdown.Item>
              <Dropdown.Item onClick={() => this.setFilter("c_rank", "le_50")}>Top 50</Dropdown.Item>
            </DropdownSubmenu>
            <Dropdown.Item onClick={() => this.setFilter("reset", "")}>Reset</Dropdown.Item>
          </DropdownButton>

          <DropdownButton id="dropdown-basic-button" variant="secondary" title="Sort By" style={{paddingRight:'10px'}}>
            <DropdownSubmenu id="dropdown-basic-button" title="Wins">
              <Dropdown.Item onClick={() => this.setSort("wins", "ascending")}>Ascending</Dropdown.Item>
              <Dropdown.Item onClick={() => this.setSort("wins", "descending")}>Descending</Dropdown.Item>
            </DropdownSubmenu>
            <DropdownSubmenu id="dropdown-basic-button" title="Losses">
              <Dropdown.Item onClick={() => this.setSort("losses", "ascending")}>Ascending</Dropdown.Item>
              <Dropdown.Item onClick={() => this.setSort("losses", "descending")}>Descending</Dropdown.Item>
            </DropdownSubmenu>
            <DropdownSubmenu id="dropdown-basic-button" title="Field Goal %">
              <Dropdown.Item onClick={() => this.setSort("fg_pct", "ascending")}>Ascending</Dropdown.Item>
              <Dropdown.Item onClick={() => this.setSort("fg_pct", "descending")}>Descending</Dropdown.Item>
            </DropdownSubmenu>
            <DropdownSubmenu id="dropdown-basic-button" title="3-Point %">
              <Dropdown.Item onClick={() => this.setSort("fg3_pct", "ascending")}>Ascending</Dropdown.Item>
              <Dropdown.Item onClick={() => this.setSort("fg3_pct", "descending")}>Descending</Dropdown.Item>
            </DropdownSubmenu>
            <DropdownSubmenu id="dropdown-basic-button" title="Free Throw %">
              <Dropdown.Item onClick={() => this.setSort("ft_pct", "ascending")}>Ascending</Dropdown.Item>
              <Dropdown.Item onClick={() => this.setSort("ft_pct", "descending")}>Descending</Dropdown.Item>
            </DropdownSubmenu>
            <DropdownSubmenu id="dropdown-basic-button" title="Conference Rank">
              <Dropdown.Item onClick={() => this.setSort("conference_rank", "ascending")}>Ascending</Dropdown.Item>
              <Dropdown.Item onClick={() => this.setSort("conference_rank", "descending")}>Descending</Dropdown.Item>
            </DropdownSubmenu>
            <Dropdown.Item onClick={() => this.setSort("reset", "")}>Reset</Dropdown.Item>
          </DropdownButton>

          <DropdownButton id="dropdown-basic-button" variant="secondary" title="Teams per Page">
            <Dropdown.Item onClick={() => this.setPageSize(5)}>5</Dropdown.Item>
            <Dropdown.Item onClick={() => this.setPageSize(10)}>10</Dropdown.Item>
            <Dropdown.Item onClick={() => this.setPageSize(15)}>15</Dropdown.Item>
          </DropdownButton>

          <Form className="d-flex" style={{paddingLeft: '15px', paddingRight: '10px'}}>
            <FormControl
              type="search"
              placeholder="Search Teams"
              className="mr-2"
              aria-label="Search"
              onChange={e => this.search = e.target.value}
              onKeyPress={e => {
                console.log(e.key)
                if (e.key === "Enter") {
                  e.preventDefault();
                  this.setSearch();
                }
              }}
            />
            <Button variant="outline-dark" onClick={() => this.setSearch()}>Search</Button>
            </Form>

        </Row>

        {this.setCards(teamArray)}

        <Row style={{paddingBottom:"30px"}}>
          <Col style={{ display: "flex", justifyContent: "center" }}>
            <Pagination className="mx-auto my-2">
              <Pagination.First onClick={() => this.changePageState(1)}/>
              <Pagination.Prev onClick={() => this.changePageState(this.state.meta.current_page-1)}/>
              {this.setPaginationBar()}   
              <Pagination.Next onClick={() => this.changePageState(this.state.meta.current_page+1)}/> 
              <Pagination.Last onClick={() => this.changePageState(this.state.meta.total_pages)} />
            </Pagination>
          </Col>
        </Row>
        
      </div>
      
    );
  }
}

export default Teams;
