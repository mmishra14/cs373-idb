import React from 'react';
import './App.css';
import NavbarLeft from './components/NavbarLeft';
import Home from './components/Home';
import Players from './components/Players';
import GameInstance from './components/GameInstance';
import Games from './components/Games';
import Teams from './components/Teams';
import Sources from './components/Sources';
import About from './components/About';
import SearchResults from './components/SearchResults'; 
import { Container, Row, Col, Form, FormControl, Button } from 'react-bootstrap';
import PlayerInstance from './components/PlayerInstance';
import TeamInstance from './components/TeamInstance';
import Books from './components/Books';
import Loading from './components/Loading'


class App extends React.Component {
  constructor() {
    super();
    this.state = {
      content: [<Home toggle={this.toggleContent.bind(this)}></Home>] // the content to be rendered
    }
    this.query = "";
    this.toggleContent = this.toggleContent.bind(this);
    this.globalSearch = this.globalSearch.bind(this);
    this.booksAPI = this.booksAPI.bind(this);
    this.api_call_state = {
      players: {
          data: [],
          meta: { current_page: 1, total_pages: 5}
      },
      teams: {
          data: [],
          meta: { current_page: 1, total_pages: 5}
      },
      games: {
          data: [],
          meta: { current_page: 1, total_pages: 5}
      },
      total: 5,
      players_total: 5,
      teams_total: 5,
      games_total: 5,
      current_page: 1,
      search: ""
    }
    this.toggleContent = this.toggleContent.bind(this);
  }

  booksAPI() {
    this.toggleContent(11);
    let page_counts = [];
    let alphabet_counts = []
    for(let i = 0; i < 15; i++) page_counts.push(0);
    for(let i = 0; i < 27; i++) alphabet_counts.push(0);
    fetch("https://cs373-idb-317819.uc.r.appspot.com/api/work/all")
    .then(res => res.json())
    .then(books => {
        for(let i = 0; i < books.length; i++) {
          let count = books[i].page_count;
          let title = books[i].title.toLowerCase();

          if (count >= 0 && count < 100) page_counts[0]++;
          else if (count >= 100 && count < 200) page_counts[1]++;
          else if (count >= 200 && count < 300) page_counts[2]++;
          else if (count >= 300 && count < 400) page_counts[3]++;
          else if (count >= 400 && count < 500) page_counts[4]++;
          else if (count >= 500 && count < 600) page_counts[5]++;
          else if (count >= 600 && count < 700) page_counts[6]++;
          else if (count >= 700 && count < 800) page_counts[7]++;
          else if (count >= 800 && count < 900) page_counts[8]++;
          else if (count >= 900 && count < 1000) page_counts[9]++;
          else if (count >= 1000 && count < 1100) page_counts[10]++;
          else if (count >= 1100 && count < 1200) page_counts[11]++;
          else if (count >= 1200 && count < 1300) page_counts[12]++;
          else if (count >= 1300 && count < 1400) page_counts[13]++;
          else if (count >= 1400) page_counts[14]++;

          if(title.startsWith("a")) alphabet_counts[0]++;
          else if(title.startsWith("b")) alphabet_counts[1]++;
          else if(title.startsWith("c")) alphabet_counts[2]++;
          else if(title.startsWith("d")) alphabet_counts[3]++;
          else if(title.startsWith("e")) alphabet_counts[4]++;
          else if(title.startsWith("f")) alphabet_counts[5]++;
          else if(title.startsWith("g")) alphabet_counts[6]++;
          else if(title.startsWith("h")) alphabet_counts[7]++;
          else if(title.startsWith("i")) alphabet_counts[8]++;
          else if(title.startsWith("j")) alphabet_counts[9]++;
          else if(title.startsWith("k")) alphabet_counts[10]++;
          else if(title.startsWith("l")) alphabet_counts[11]++;
          else if(title.startsWith("m")) alphabet_counts[12]++;
          else if(title.startsWith("n")) alphabet_counts[13]++;
          else if(title.startsWith("o")) alphabet_counts[14]++;
          else if(title.startsWith("p")) alphabet_counts[15]++;
          else if(title.startsWith("q")) alphabet_counts[16]++;
          else if(title.startsWith("r")) alphabet_counts[17]++;
          else if(title.startsWith("s")) alphabet_counts[18]++;
          else if(title.startsWith("t")) alphabet_counts[19]++;
          else if(title.startsWith("u")) alphabet_counts[20]++;
          else if(title.startsWith("v")) alphabet_counts[21]++;
          else if(title.startsWith("w")) alphabet_counts[22]++;
          else if(title.startsWith("x")) alphabet_counts[23]++;
          else if(title.startsWith("y")) alphabet_counts[24]++;
          else if(title.startsWith("z")) alphabet_counts[25]++;
          else  alphabet_counts[26]++;   
        }
        this.toggleContent(10, {p_count: page_counts, a_count: alphabet_counts});
    });
  }

  /**
   * Executes a global search for Teams, Players, and Games
   * @param {*} page the page number to be rendered
   */
  async globalSearch(page) {
    this.toggleContent(11);

    let players = { data: [], meta: { current_page: 1, total_pages: this.api_call_state.players_total} }
    let teams = { data: [], meta: { current_page: 1, total_pages: this.api_call_state.teams_total} }
    let games = { data: [], meta: { current_page: 1, total_pages: this.api_call_state.games_total} }
    
    if(page > 0 && page < this.api_call_state.total + 1) {
      let link_one = "https://nbatoday.xyz/api/games?per_page=" + 5 + "&page=" + page + "&q=" + this.query;
      let link_two = "https://nbatoday.xyz/api/teams?per_page=" + 5 + "&page=" + page + "&q=" + this.query;
      let link_three = "https://nbatoday.xyz/api/players?per_page=" + 5 + "&page=" + page + "&q=" + this.query;
      if(page <= this.api_call_state.games_total)
        games = await fetch(link_one).then(res => res.json());
      if(page <= this.api_call_state.teams_total)
        teams = await fetch(link_two).then(res => res.json());
      if(page <= this.api_call_state.players_total)
        players = await fetch(link_three).then(res => res.json());
      this.api_call_state.games.data = games.data;
      this.api_call_state.games.meta = games.meta;
      this.api_call_state.teams.data = teams.data;
      this.api_call_state.teams.meta = teams.meta;
      this.api_call_state.players.data = players.data;
      this.api_call_state.players.meta = players.meta;
      this.api_call_state.total = players.meta.total_pages > teams.meta.total_pages ? players.meta.total_pages : teams.meta.total_pages;
      this.api_call_state.total = this.api_call_state.total > games.meta.total_pages ? this.api_call_state.total : games.meta.total_pages;
      this.api_call_state.players_total = players.meta.total_pages;
      this.api_call_state.teams_total = teams.meta.total_pages;
      this.api_call_state.games_total = games.meta.total_pages;
      this.api_call_state.current_page = page;
      this.api_call_state.search = this.query;
      this.toggleContent(9, this.api_call_state)
    }
  }

  /** 
     * To toggle the content/page displayed. Uses pageId to determine
     * the content to render, and instance to render specialized
     * content for certain pages/components
     * 
     * pageId maps as follows
     * 0 -> Home | 1 -> Players | 2 -> GameInstance | 3 -> Teams
     * 4 -> APIs and Sources | 5 -> About Us | 6 -> PlayerInstance
     * 7 -> TeamInstance | 8 -> Games | 9 -> SearchResults | default -> home
     * */ 
   toggleContent(pageId, instance) {
     console.log("in toggleContent")
    switch(pageId) {
      case 0:
        this.setState({ content:[<Home toggle={this.toggleContent}></Home>] }); break;
      case 1:
        this.setState({ content:[<Players toggle={this.toggleContent}></Players>] }); break;
      case 2:
        this.setState({ content:[<GameInstance instance={instance} toggle={this.toggleContent}></GameInstance>] }); break;
      case 3:
        this.setState({ content:[<Teams toggle={this.toggleContent}></Teams>] }); break;
      case 4:
        this.setState({ content:[<Sources></Sources>] }); break;
      case 5:
        this.setState({ content:[<About></About>] }); break;
      case 6:
        this.setState({ content:[<PlayerInstance instance={instance} toggle={this.toggleContent}></PlayerInstance>]}); break;
      case 7:
        this.setState({ content:[<TeamInstance instance={instance} toggle={this.toggleContent}></TeamInstance>]}); break;
      case 8:
        this.setState({ content:[<Games toggle={this.toggleContent}></Games>]}); break;
      case 9:
        this.setState({ content:[<SearchResults search={this.globalSearch} data={instance} toggle={this.toggleContent}></SearchResults>]}); break;
      case 10:
        this.setState({ content:[<Books data={instance}></Books>] }); break;
      case 11:
          this.setState({ content:[<Loading></Loading>] }); break;
      default:
        this.setState({ content:[<Home></Home>] }); break;
    }
  }

  render() {

    return (
        // set up the overall grid structure
        <Container fluid className="App">
          
          <Row>
            <NavbarLeft toggle={this.toggleContent} booksAPI={this.booksAPI}></NavbarLeft>

            <Col style={{backgroundColor:"", height:"100vh"}}>

              <Row>
                <Col className="top-nav">
                  <button className="top-nav-button" onClick={() => this.toggleContent(5)}>About Us</button>
                  <button className="top-nav-button" style={{width:"200px"}} onClick={() => this.toggleContent(4)}>APIs and Sources</button>
                </Col>
                <Col>
                  <Form className="d-flex" style={{paddingTop:"15px"}}>
                    <FormControl
                      type="search"
                      placeholder="Search"
                      className="mr-2"
                      aria-label="Search"
                      onChange={e => this.query = e.target.value}
                      onKeyPress={e => {
                        console.log(e.key)
                        if (e.key === "Enter") {
                          e.preventDefault();
                          this.globalSearch(1);
                        }
                      }}
                    />
                    <Button variant="outline-dark" onClick={() => this.globalSearch(1)}>Search</Button>
                  </Form>
                </Col>
              </Row>

              {/* the rest of the grid will be determined by the content that is rendered */}
              {this.state.content}

            </Col>
          </Row>
        </Container>
    );
  }
}
export default App;

