ifeq ($(shell uname), Darwin)
	PYTHON      := python3
	PIP         := pip3
	PYLINT      := pylint
	COVERAGE    := coverage
	PYDOC       := pydoc3
	AUTOPEP8    := autopep8
	YARN        := yarn
	NPM 		:= npm
else ifeq ($(shell uname -p), unknown)
	PYTHON      := python
	PIP         := pip3
	PYLINT      := pylint
	COVERAGE    := coverage
	PYDOC       := python -m pydoc
	AUTOPEP8    := autopep8
	YARN        := yarn
	NPM 		:= npm
else
	PYTHON      := python3
	PIP         := pip3
	PYLINT      := pylint3
	COVERAGE    := coverage
	PYDOC       := pydoc3
	AUTOPEP8    := autopep8
	YARN        := yarn
	NPM 		:= npm
endif


models.html: backend/app/models.py
	cd backend;							\
	$(PYDOC) -w app.models;				\
	mv app.models.html ../models.html;	\

start_frontend:
	cd frontend;		\
	$(YARN) install;	\
	$(YARN) build;		\
	$(YARN) start;		\

start_backend:
	cd backend;								\
	$(PIP) install -r requirements.txt;		\
	$(PYTHON) run.py;						\

deploy_backend:
	bash deploy-backend.sh

deploy_frontend:
	bash deploy-frontend.sh

deploy_dispatch:
	gcloud app deploy dispatch.yaml

deploy_stack:
	bash deploy-stack.sh

scrape: backend/scrape/scrape.py
	$(PYTHON) backend/scrape/scrape.py teams games players

scrape_clean:
	rm -f backend/scrape/data/*

postman_test:
	newman run backend/nbatoday_tests.json -e backend/nbatoday_environment.json

model_tests:
	cd backend;							\
	$(PIP) install -r requirements.txt;	\
	$(COVERAGE) run run_test.py 2>&1;	\
	$(COVERAGE) report -m;				\

IDB1.log:
	git log > IDB1.log