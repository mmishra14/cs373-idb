import os
import unittest
os.environ['DB_ENV'] = 'test'
HOST = os.environ.get('DB_HOST', 'localhost')
os.environ['DB_STRING'] = os.environ.get('TEST_DB_STRING', f"postgresql+psycopg2://postgres:admin@{HOST}:5432/unit_tests")
from app import *


if __name__ == "__main__":
    unittest.main()