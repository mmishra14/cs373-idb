from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import create_engine, Column, String, Integer, Date, Float, ForeignKey, Table
import sqlalchemy
from sqlalchemy.orm import sessionmaker, close_all_sessions, backref, relationship, relation
from sqlalchemy.ext.declarative import declarative_base
import os

'''
Set up a db session specified by the environment variable DB_STRING.
'''

if os.environ.get('DB_ENV') == 'test':  # pragma: no cover
    engine = create_engine(os.environ.get(
        'DB_STRING', 'postgresql://postgres:admin@localhost:5432/test'))
    Session = sessionmaker(bind=engine)
    session = Session()
    Base = declarative_base()

else:  # pragma: no cover
    from app import app
    app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get(
        'DB_STRING', 'postgresql://postgres:admin@localhost:5432/test')
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
    db = SQLAlchemy(app)
    Base = db.Model
    session = db.session


'''
Relationship Table for the Many to Many relationship between players and games
'''
played_in = Table('played_in', Base.metadata,
                  Column('game_id', Integer, ForeignKey('game.id')),
                  Column('player_id', Integer, ForeignKey('player.id'))
                  )


class Team(Base):
    '''
    The Team model which represents the Team page.
    Contains the various information such as name, season, losses, etc.
    Contains relationship with games being a home_team or visitor_team.
    Contains relationship with teams being the curren_team.
    '''
    __tablename__ = 'team'

    id = Column(Integer, primary_key=True)
    name = Column(String(80), nullable=False)
    season = Column(String(80), nullable=False)
    wins = Column(Integer)
    losses = Column(Integer)
    fg_pct = Column(Float)
    fg3_pct = Column(Float)
    ft_pct = Column(Float)
    conference_rank = Column(Integer)
    division_rank = Column(Integer)
    finals_appearance = Column(String(80), nullable=False)
    logo_embed_link = Column(String(100))
    color = Column(String(80))
    video_link = Column(String(200))
    googlemap_embed_link = Column(String(400))
    # one to many relationship (populate column attribute in players)
    players = relationship('Player', backref='team',
                           order_by='Player.name.desc()')

    def serialize(self):  # pragma: no cover
        '''
        Serializes the team class and introduces extra information.
        Adds the list of players names and ids/positions as well as
        the list of games and their game names.
        '''
        # generate the list of player ids, names and positions
        player_ids = []
        player_names = []
        player_positions = []

        for player in self.players:
            player_ids.append(player.id)
            player_names.append(player.name)
            player_positions.append(player.position)

        # add game ids and names sorted by dates
        games = self.home_games + self.visitor_games
        game_ids = []
        game_names = []
        game_dates = []
        game_scores = []
        games.sort(key=lambda i: i.date, reverse=True)
        for g in games:
            game_ids.append(g.id)
            game_names.append(g.home.name + ' vs ' +
                              g.visitor.name)
            game_dates.append(g.date.isoformat())
            game_scores.append(str(g.h_score) + '-'+str(g.v_score))
        return {
            "season": self.season,
            "id": self.id,
            "name": self.name,
            "wins": self.wins,
            "losses": self.losses,
            "fg_pct": self.fg_pct,
            "fg3_pct": self.fg3_pct,
            "ft_pct": self.ft_pct,
            "conference_rank": self.conference_rank,
            "division_rank": self.division_rank,
            "finals_appearance": self.finals_appearance,
            "logo_embed_link": self.logo_embed_link,
            "color": self.color,
            "player_names": player_names,
            "player_ids": player_ids,
            "game_ids": game_ids,
            "game_names": game_names,
            "player_positions": player_positions,
            "video_link": self.video_link,
            "googlemap_embed_link": self.googlemap_embed_link,
            "game_dates": game_dates,
            "game_scores": game_scores
        }


class Player(Base):
    '''
    Player model contains the various player stats such as name, weight, etc.
    Contains a many to one relationship with teams with foreign key current_team.
    Contains a many to many relationship with games.
    '''
    __tablename__ = 'player'

    id = Column(Integer, primary_key=True, autoincrement="auto")
    name = Column(String(80), nullable=False)
    height = Column(Integer)
    weight = Column(Integer)
    age = Column(Integer)
    position = Column(String(80), nullable=False)
    headshot_embed_link = Column(String(80))
    fg_total = Column(Integer)
    fg3_total = Column(Integer)
    ft_total = Column(Integer)
    avg_points_per_game = Column(Float)
    # one to many relationship from teams and players
    current_team = Column(Integer, ForeignKey('team.id'))

    def serialize(self):  # pragma: no cover
        '''
        Seriailizes the class attributes into a dictionary.
        Then also adds a list of games sorted by date descending.
        '''
        # generate id and name list of games
        self.games.sort(key=lambda i: i.date, reverse=True)
        game_ids = []
        game_names = []
        game_dates = []
        game_scores = []
        for game in self.games:
            game_ids.append(game.id)
            h_team_name = game.home.name
            v_team_name = game.visitor.name
            game_names.append(h_team_name + ' vs ' + v_team_name)
            game_dates.append(game.date.isoformat())
            game_scores.append(str(game.h_score)+'-'+str(game.v_score))

        return{
            "id": self.id,
            "name": self.name,
            "age": self.age,
            "weight": self.weight,
            "height": self.height,
            "position": self.position,
            "headshot_embed_link": self.headshot_embed_link,
            "current_team_id": self.current_team,
            "current_team_name": self.team.name,
            "fg_total": self.fg_total,
            "fg3_total": self.fg3_total,
            "ft_total": self.ft_total,
            "average_points_per_game": self.avg_points_per_game,
            "game_ids": game_ids,
            "game_names": game_names,
            "game_scores": game_scores,
            "game_dates": game_dates
        }


class Game(Base):
    '''
    The game model that contains various game attributes such as season, date, score.
    The model contains 2 many to one relationships with teams with foreign keys 
    home_team and visitor_team. The model contains a many to many relationship with players.
    '''
    __tablename__ = 'game'

    id = Column(Integer, primary_key=True, autoincrement="auto")
    date = Column(Date)
    season = Column(String(80), nullable=False)
    h_score = Column(Integer)
    h_fga = Column(Integer)
    h_fgm = Column(Integer)
    h_fg3m = Column(Integer)
    h_fg3a = Column(Integer)
    h_fta = Column(Integer)
    h_ftm = Column(Integer)
    v_score = Column(Integer)
    v_fga = Column(Integer)
    v_fgm = Column(Integer)
    v_fg3m = Column(Integer)
    v_fg3a = Column(Integer)
    v_fta = Column(Integer)
    v_ftm = Column(Integer)
    # manage relationships
    h_team = Column(Integer, ForeignKey('team.id'))
    v_team = Column(Integer, ForeignKey('team.id'))
    players = relationship("Player", secondary=played_in,
                           backref="games")

    home = relationship(Team, lazy='joined',
                        foreign_keys='Game.h_team', backref='home_games')
    visitor = relationship(Team, lazy='joined',
                           foreign_keys='Game.v_team', backref='visitor_games')

    def serialize(self):  # pragma: no cover
        '''
        Serializes the attributes into a dictionary.
        Adds the team names to dict and the roster of players on each team
        '''

        # get home team name and id
        h_team_name = self.home.name
        v_team_name = self.visitor.name

        h_player_ids = []
        h_player_names = []
        v_player_ids = []
        v_player_names = []

        # get home players id and names
        for player in self.players:
            if player.current_team == self.h_team:
                h_player_ids.append(player.id)
                h_player_names.append(player.name)
            else:
                v_player_ids.append(player.id)
                v_player_names.append(player.name)

        return {
            "id": self.id,
            "date": self.date.isoformat(),
            "season": self.season,
            "h_team_id": self.h_team,
            "h_team_name": h_team_name,
            "h_score": self.h_score,
            "h_fga": self.h_fga,
            "h_fgm": self.h_fgm,
            "h_fg3a": self.h_fg3a,
            "h_fg3m": self.h_fg3m,
            "h_fta": self.h_fta,
            "h_ftm": self.h_ftm,
            "v_team_id": self.v_team,
            "v_team_name": v_team_name,
            "v_score": self.v_score,
            "v_fga": self.v_fga,
            "v_fgm": self.v_fgm,
            "v_fg3a": self.v_fg3a,
            "v_fg3m": self.v_fg3m,
            "v_fta": self.v_fta,
            "v_ftm": self.v_ftm,
            "h_player_ids": h_player_ids,
            "h_player_names": h_player_names,
            "v_player_ids": v_player_ids,
            "v_player_names": v_player_names,
            "googlemap_embed_link": self.home.googlemap_embed_link,
            "h_logo": self.home.logo_embed_link,
            "v_logo": self.visitor.logo_embed_link
        }


'''
Don't recreate DB if on production env.
'''
if (not os.environ.get('DB_ENV') == 'prod'):  # pragma: no cover
    if os.environ.get('DB_ENV') == 'test':
        close_all_sessions()
        Base.metadata.drop_all(engine)
        Base.metadata.create_all(engine)
    else:
        db.drop_all()
        db.create_all()
