from flask import Flask
from flask_cors import CORS
import os
import logging

if os.environ.get('DB_ENV') == 'test':
    from .tests import *

else: # pragma: no cover
    logging.basicConfig(level=logging.DEBUG)
    app = Flask(__name__, instance_relative_config=True)
    CORS(app)
    from app import main
    app.config.from_object('config')