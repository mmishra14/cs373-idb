from datetime import datetime
import json

from sqlalchemy.sql.sqltypes import String
from app import app
from .models import Team, Game, Player, db
from operator import eq
from sqlalchemy import asc, desc, union_all
from flask import Response
import datetime
from operator import eq, le, ge, lt, gt, ne
from sqlalchemy import or_, cast, func, and_, except_

'''
    This file contains all parsing operations,
    for the arguments in an api call.
'''

PAGESIZE = 25

# functions called to filter query
filter_functions = {
    'eq': eq,
    'le': le,
    'ge': ge,
    'lt': lt,
    'gt': gt,
    'ne': ne
}

# get data relating to pagination throw a 404 if failed


def get_pagination_data(args):
    pagesize = PAGESIZE
    page = 1

    if 'per_page' in args:
        try:
            pagesize = int(args.get("per_page"))
        except ValueError:
            return Response(json.dumps({"error-response": 'Invalid per_page value'}), 404,)
        if pagesize <= 0:
            return Response(json.dumps({"error-response": 'Invalid per_page value'}), 404,)

    if 'page' in args:
        try:
            page = int(args.get('page'))
        except ValueError:
            return Response(json.dumps({"error-response": 'Invalid page value'}), 404,)
        if page <= 0:
            return Response(json.dumps({"error-response": 'Invalid page value'}), 404,)

    return pagesize, page

# parse all data related to sorting throw 404 if error


def get_sorting_data(fields, sort_functions, args):
    sort_by = 'id'
    sort_order = asc
    if 'sort_by' in args:
        field = args.get('sort_by')
        if field in fields:
            sort_by = fields[field][0]
        else:
            return Response(json.dumps({"error-response": 'Invalid sort by value'}), 404,)
    if 'sort_order' in args:
        order = args.get('sort_order')
        if order in sort_functions:
            sort_order = sort_functions[order]
        else:
            return Response(json.dumps({"error-response": 'Invalid sort order'}), 404,)
    return sort_by, sort_order

# parse all data and create filter list for filtering throw 404 if error


def get_filters(fields, args):
    filters = []
    for key, val in args.items():
        if key in fields:
            filter_function = eq
            filter_val = val
            exp = val.split('_')
            if len(exp) == 2:
                if exp[0] in filter_functions:
                    filter_function = filter_functions[exp[0]]
                    filter_val = exp[1]
                else:
                    return Response(json.dumps({"error-response": 'Invalid filter function'}), 404,)
            elif len(exp) != 1:
                return Response(json.dumps({"error-response": 'Invalid filter value for attribute'}), 404,)
            try:
                if fields[key][1] is datetime.date:
                    filter_val = datetime.datetime.strptime(
                        filter_val, '%Y-%m-%d').date()
                else:
                    filter_val = fields[key][1](filter_val)
                    if fields[key][1] == str:
                        filter_val = filter_val.casefold()
            except:
                return Response(json.dumps({"error-response": 'Invalid filter value for attribute'}), 404,)
            filters.append(filter_function(fields[key][0], filter_val))
    return filters

# standardizes field as lower case string for searching


def standardize_field(field):
    return func.lower(cast(field, String))

# return search query for player objects


def search_players(q):
    words = q.strip().lower().split()
    multi_word_query = []
    for word in words:
        search_queries = []
        search_queries.append(standardize_field(Player.name).contains(word))
        search_queries.append(standardize_field(Player.height).contains(word))
        search_queries.append(standardize_field(Player.weight).contains(word))
        search_queries.append(standardize_field(Player.age).contains(word))
        search_queries.append(standardize_field(
            Player.position).contains(word))
        search_queries.append(standardize_field(
            Player.fg_total).contains(word))
        search_queries.append(standardize_field(
            Player.fg3_total).contains(word))
        search_queries.append(standardize_field(
            Player.ft_total).contains(word))
        search_queries.append(standardize_field(
            Player.avg_points_per_game).contains(word))
        # team name search
        search_queries.append(Player.team.has(
            standardize_field(Team.name).contains(word)))
        multi_word_query.append(or_(*search_queries))
    return or_(*multi_word_query), and_(*multi_word_query)

# return search query for team objects


def search_teams(q):
    words = q.strip().lower().split()
    multi_word_query = []
    for word in words:
        search_queries = []
        search_queries.append(standardize_field(Team.name).contains(word))
        search_queries.append(standardize_field(Team.season).contains(word))
        search_queries.append(standardize_field(Team.wins).contains(word))
        search_queries.append(standardize_field(Team.losses).contains(word))
        search_queries.append(standardize_field(Team.fg_pct).contains(word))
        search_queries.append(standardize_field(Team.fg3_pct).contains(word))
        search_queries.append(standardize_field(Team.ft_pct).contains(word))
        search_queries.append(standardize_field(
            Team.conference_rank).contains(word))
        search_queries.append(standardize_field(
            Team.division_rank).contains(word))
        search_queries.append(standardize_field(
            Team.finals_appearance).contains(word))
        # complex player search
        search_queries.append(Team.players.any(
            standardize_field(Player.name).contains(word)))
        multi_word_query.append(or_(*search_queries))
    return or_(*multi_word_query), and_(*multi_word_query)

# return search query for game objects


def search_games(q):
    words = q.strip().lower().split()
    multi_word_query = []
    for word in words:
        search_queries = []
        search_queries.append(standardize_field(Game.date).contains(word))
        search_queries.append(standardize_field(Game.season).contains(word))
        search_queries.append(standardize_field(Game.h_score).contains(word))
        search_queries.append(standardize_field(Game.h_fga).contains(word))
        search_queries.append(standardize_field(Game.h_fgm).contains(word))
        search_queries.append(standardize_field(Game.h_fg3m).contains(word))
        search_queries.append(standardize_field(Game.h_fg3a).contains(word))
        search_queries.append(standardize_field(Game.h_fta).contains(word))
        search_queries.append(standardize_field(Game.h_ftm).contains(word))
        search_queries.append(standardize_field(Game.v_score).contains(word))
        search_queries.append(standardize_field(Game.v_fga).contains(word))
        search_queries.append(standardize_field(Game.v_fgm).contains(word))
        search_queries.append(standardize_field(Game.v_fg3m).contains(word))
        search_queries.append(standardize_field(Game.v_fg3a).contains(word))
        search_queries.append(standardize_field(Game.v_fta).contains(word))
        search_queries.append(standardize_field(Game.v_ftm).contains(word))
        # more complex player/team search
        search_queries.append(Game.home.has(
            standardize_field(Team.name).contains(word)))
        search_queries.append(Game.visitor.has(
            standardize_field(Team.name).contains(word)))
        search_queries.append(Game.players.any(
            standardize_field(Player.name).contains(word)))
        multi_word_query.append(or_(*search_queries))
    return or_(*multi_word_query), and_(*multi_word_query)
