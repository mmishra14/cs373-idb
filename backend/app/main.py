from flask import render_template, jsonify, request, Response
from flask.wrappers import Request
import flask_sqlalchemy
from sqlalchemy import asc, desc
import sqlalchemy
from app import app
import json
from .create_db import Team, Game, Player, session, create_all, db
from subprocess import Popen, PIPE, STDOUT
from .fields import player_fields, team_fields, game_fields, sort_functions
from .parse_query import get_pagination_data, get_sorting_data, get_filters, search_teams, search_players, search_games
import requests

'''
    This file handles all endpoints serving 
    the required API data or a 404 if error.
'''

'''
Default endpoint
'''


@app.route('/')
def root():
    return 'Welcome to NBAToday, to learn more about the API visit the Postman documentation on our website.'


'''
Endpoint for teams data.
Specify a team by id.
Specify to get all teams, then search, sort, filter and paginate.
'''


@app.route('/api/teams', defaults={"id": None})
@app.route('/api/teams/<int:id>')
def getTeam(id):
    # get all teams
    if id is None:

        # get pagination data error if invalid
        pagesize_number = get_pagination_data(request.args)

        if type(pagesize_number) is Response:
            return pagesize_number

        per_page = pagesize_number[0]
        page_number = pagesize_number[1]

        # get sorting data error if invalid
        sorting_info = get_sorting_data(
            team_fields, sort_functions, request.args)

        if type(sorting_info) is Response:
            return sorting_info

        sort_by = sorting_info[0]
        sort_order = sorting_info[1]

        # get filter data error if invalid
        filters = get_filters(team_fields, request.args)

        if type(filters) is Response:
            return filters

        # apply filters
        try:
            teams = Team.query.filter(*filters)
        except:
            session.rollback()
            return Response(json.dumps({"error-response": 'Error With Query'}), 500,)

        # search specific changes
        and_count = 0

        if('q' in request.args):
            q = request.args['q']
            try:
                or_filter, and_filter = search_teams(q)
                and_query = teams.filter(and_filter).order_by(sort_order(sort_by))
                or_query = teams.filter(or_filter).order_by(sort_order(sort_by)).except_(and_query)

                # apply ordering
                and_query = and_query
                or_query = or_query
                # get count
                and_count = and_query.count()

                # apply union
                teams = and_query.union_all(or_query)

            except:
                session.rollback()
                return Response(json.dumps({"error-response": 'Error With Query'}), 500,)

        # apply ordering on non-search
        else:
            try:
                teams = teams.order_by(sort_order(sort_by))
            except:
                return Response(json.dumps({"error-response": 'Error With Query'}), 500,)
        # apply pagination
        teams = teams.paginate(page_number, per_page)

        # extract pagination metadata and return json
        meta = {
            'current_page': teams.page,
            'total_pages': teams.pages,
            'per_page': per_page,
            'total_items': teams.total,
            'and_items': and_count
        }
        # get serialized query
        data = []
        count = 0
        for t in teams.items:
            val = t.serialize()
            count += 1
            if 'q' in request.args:
                if count <= and_count:
                    val['search_result'] = 'and'
                else: 
                    val['search_result'] = 'or'
            else:
                val['search_result'] = None
            data.append(val)

        # return serialized version
        return jsonify(data=data, meta=meta)

    # get team by input id
    t = Team.query.get_or_404(id)
    return jsonify(t.serialize())


'''
Endpoint serving games data.
Specify a single game by id.
Specify to get all games and then filter, sort, search and paginate
'''


@app.route('/api/games', defaults={"id": None})
@app.route('/api/games/<int:id>')
def getGame(id):
    # multi-item call
    if id is None:
        # get pagination data, error if invalid
        pagesize_number = get_pagination_data(request.args)

        if type(pagesize_number) is Response:
            return pagesize_number

        per_page = pagesize_number[0]
        page_number = pagesize_number[1]

        # get sorting data, error if invalid
        sorting_info = get_sorting_data(
            game_fields, sort_functions, request.args)

        if type(sorting_info) is Response:
            return sorting_info

        sort_by = sorting_info[0]
        sort_order = sorting_info[1]

        # get filter data, error if invalid
        filters = get_filters(game_fields, request.args)

        if type(filters) is Response:
            return filters

        # apply filters
        try:
            games = Game.query.filter(*filters)
        except:
            session.rollback()
            return Response(json.dumps({"error-response": 'Error With Query'}), 500,)

        # search specific changes
        and_count = 0

        if('q' in request.args):
            q = request.args['q']
            try:
                or_filter, and_filter = search_games(q)
                and_query = games.filter(and_filter).order_by(sort_order(sort_by))
                or_query = games.filter(or_filter).order_by(sort_order(sort_by)).except_(and_query)

                # apply ordering
                and_query = and_query
                or_query = or_query
                # get count
                and_count = and_query.count()

                # apply union
                games = and_query.union_all(or_query)

            except:
                session.rollback()
                return Response(json.dumps({"error-response": 'Error With Query'}), 500,)

        # apply ordering on non-search
        else:
            try:
                games = games.order_by(sort_order(sort_by))
            except:
                return Response(json.dumps({"error-response": 'Error With Query'}), 500,)
        # apply pagination
        games = games.paginate(page_number, per_page)

        # extract pagination metadata
        meta = {
            'current_page': games.page,
            'total_pages': games.pages,
            'per_page': games.per_page,
            'total_items': games.total,
            'and_items': and_count
        }

        # get serialized query
        data = []
        count = 0
        for g in games.items:
            val = g.serialize()
            count += 1
            if 'q' in request.args:
                if count <= and_count:
                    val['search_result'] = 'and'
                else: 
                    val['search_result'] = 'or'
            else:
                val['search_result'] = None
            data.append(val)

        # return serialized version
        return jsonify(data=data, meta=meta)

    # get player by id
    g = Game.query.get_or_404(id)
    return jsonify(g.serialize())


'''
Endpoint that serves players objects.
Specify a single player by id or specify
a complex filter, pagination, search and/or sort
'''


@app.route('/api/players', defaults={"id": None})
@app.route('/api/players/<int:id>')
def getPlayer(id):
    # multi-item route
    if id is None:

        # get pagination arguments return error if bad request
        pagesize_number = get_pagination_data(request.args)

        if type(pagesize_number) is Response:
            return pagesize_number

        per_page = pagesize_number[0]
        page_number = pagesize_number[1]

        # get sorting arguments return error if bad request
        sorting_info = get_sorting_data(
            player_fields, sort_functions, request.args)

        if type(sorting_info) is Response:
            return sorting_info

        sort_by = sorting_info[0]
        sort_order = sorting_info[1]

        # get filter data return error if bad response
        filters = get_filters(player_fields, request.args)

        if type(filters) is Response:
            return filters

        # apply filters
        try:
            players = Player.query.filter(*filters)
        except:
            session.rollback()
            return Response(json.dumps({"error-response": 'Error With Query'}), 500,)

        # search specific changes
        and_count = 0

        if('q' in request.args):
            q = request.args['q']
            try:
                or_filter, and_filter = search_players(q)
                and_query = players.filter(and_filter).order_by(sort_order(sort_by))
                or_query = players.filter(or_filter).order_by(sort_order(sort_by)).except_(and_query)

                # apply ordering
                and_query = and_query
                or_query = or_query
                # get count
                and_count = and_query.count()

                # apply union
                players = and_query.union_all(or_query)

            except:
                session.rollback()
                return Response(json.dumps({"error-response": 'Error With Query'}), 500,)

        # apply ordering on non-search
        else:
            try:
                players = players.order_by(sort_order(sort_by))
            except:
                return Response(json.dumps({"error-response": 'Error With Query'}), 500,)

        # paginate query
        players = players.paginate(page_number, per_page)

        # extract pagination metadata
        meta = {
            'current_page': players.page,
            'total_pages': players.pages,
            'per_page': players.per_page,
            'total_items': players.total,
        }

        # return serialized query
        data = []
        count = 0
        for p in players.items:
            val = p.serialize()
            count += 1
            if 'q' in request.args:
                if count <= and_count:
                    val['search_result'] = 'and'
                else: 
                    val['search_result'] = 'or'
            else:
                val['search_result'] = None
            data.append(val)
        
        return jsonify(data=data, meta=meta)

    # single item route
    p = Player.query.get_or_404(id)
    return jsonify(p.serialize())


'''
Endpoint runs the unit tests as a subprocess.
'''


@app.route('/api/run_test')
def run_test():
    test_cmd = 'coverage run run_test.py'
    report_cmd = 'coverage report -m'
    test_output = ''
    report = ''
    with Popen(test_cmd.split(' '), stdout=PIPE, stderr=STDOUT) as proc:
        test_output = proc.stdout.read().decode()
    with Popen(report_cmd.split(' '), stdout=PIPE, stderr=STDOUT) as proc:
        report = proc.stdout.read().decode()

    test_output = test_output.replace('\n', '<br>')
    report = report.replace('\n', '<br>')

    return '<pre>'+test_output+'<br>'+report+'</pre>'


'''
Endpoint that provides forwarding of news api.
Provides the top 9 articles with the relevant search
criteria 'NBA'.
'''


@app.route('/api/news')
def get_latest_news():
    parameters = {
        'apikey': 'pub_4843c6ae79eceea7a5ddd5ee7b3a3001258',
        'country': 'us',
        'category': 'sports',
        'qInTitle': 'nba'
    }
    response = requests.get(
        url='https://newsdata.io/api/1/news', params=parameters)
    if response.status_code != 200:
        return Response(json.dumps({"error-response": 'Error With News API Call'}), 500,)
    data = response.json()
    return data
