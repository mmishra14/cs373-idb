#!/usr/bin/env python3

import os
import sys
import json
import pathlib
import unittest
import datetime
from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine

#ugly hack to make the imports work
#"better" solutions would be more disruptive to the project structure
from .models import Team, Player, Game, session, Session, engine, Base, played_in


TEST_DATA_DIR = 'test_data/'

def load_json(path):
    with open(path) as file:
        return json.load(file)

def load_team(path, db):
    """
    load 1 or more teams into database from
    suitable json file located at path.
    return a list of the team IDs for later
    use
    """
    teams = load_json(TEST_DATA_DIR + path)
    ids = []
    for team in teams["data"]:
        t = Team()
        t.id                = team["_bdl_id"]
        t.season            = team["season"]
        t.name              = team["name"]
        t.wins              = team["wins"]
        t.losses            = team["losses"]
        t.conference_rank   = team["conference_rank"]
        t.division_rank     = team["division_rank"]
        t.finals_appearance = team["finals_appearance"]
        t.fg3_pct           = team["fg3_pct"]
        t.fg_pct            = team["fg_pct"]
        t.ft_pct            = team["ft_pct"]
        db.session.add(t)
        db.session.commit()
        ids.append(t.id)
    return ids

def load_player(path, relations, db):
    """
    load 1 or more players into database from
    suitable json file located at path.
    return a list of the player IDs for later
    use.
    relations is a boolean indicating whether
    to populate fields that are relations with
    other models.
    """
    players = load_json(TEST_DATA_DIR + path)
    ids = []
    for player in players["data"]:
        p = Player()
        p.id                  = player["_bdl_id"]
        p.name                = player["name"]
        p.height              = player["_height"]
        p.weight              = player["weight"]
        p.age                 = 2021 - int(player["_birthdate"].split("-")[0])
        p.position            = player["position"]
        p.fg_total            = player["fg_total"]
        p.fg3_total           = player["fg3_total"]
        p.ft_total            = player["ft_total"]
        p.avg_points_per_game = player["average_points_per_game"]
        p.block_pct           = player["block_pct"]
        if relations:
            team = db.session.query(Team).filter(Team.name == player["team"]).first()
            p.current_team = team.id
        db.session.add(p)
        db.session.commit()
        ids.append(p.id)
    return ids

def load_game(path, relations, db):
    """
    load 1 or more games into database from
    suitable json file located at path.
    return a list of the game IDs for later
    use.
    relations is a boolean indicating whether
    to populate fields that are relations with
    other models.
    """
    games = load_json(TEST_DATA_DIR + path)
    ids = []
    for game in games["data"]:
        g = Game()
        g.id      = game["_bdl_id"]
        g.date    = datetime.datetime.strptime(game["date"], "%Y-%m-%d").date()
        g.season  = game["season"]
        g.h_score = game["h_score"]
        g.h_fga   = game["h_fga"]
        g.h_fgm   = game["h_fgm"]
        g.h_fg3m  = game["h_fg3m"]
        g.h_fg3a  = game["h_fg3a"]
        g.h_fta   = game["h_fta"]
        g.h_ftm   = game["h_ftm"]
        g.v_score = game["v_score"]
        g.v_fga   = game["v_fga"]
        g.v_fgm   = game["v_fgm"]
        g.v_fg3m  = game["v_fg3m"]
        g.v_fg3a  = game["v_fg3a"]
        g.v_fta   = game["v_fta"]
        g.v_ftm   = game["v_ftm"]
        if relations:
            g.h_team = game["_h_team_bdl_id"]
            g.v_team = game["_v_team_bdl_id"]
            home_team = db.session.query(Team).get(g.h_team)
            visitor_team = db.session.query(Team).get(g.v_team)

            for player in home_team.players:
                g.players.append(player)
            for player in visitor_team.players:
                g.players.append(player)
        db.session.add(g)
        db.session.commit()
        ids.append(g.id)
    return ids

class DBTestCases(unittest.TestCase):
    def setUp(self):

        # connect to the database
        self.connection = engine.connect()

        # begin a non-ORM transaction
        self.trans = self.connection.begin()

        # bind an individual Session to the connection
        self.session = Session(bind=self.connection)

    def tearDown(self):
        self.session.close()

        # rollback - everything that happened with the
        # Session above (including calls to commit())
        # is rolled back.
        self.trans.rollback()

        # return connection to the Engine
        self.connection.close()

    def test_team_single(self):
        ids = load_team("team_single.json", self)
        self.assertTrue(len(ids) == 1)
        t_id = ids[0]

        t = self.session.query(Team).get(t_id)
        self.assertEqual(t.season, "2020-21")
        self.assertEqual(t.name, "Atlanta Hawks")
        self.assertEqual(t.wins, 41)
        self.assertEqual(t.losses, 31)
        self.assertTrue(abs(t.fg_pct  - 0.468) < 0.001)
        self.assertTrue(abs(t.fg3_pct - 0.373) < 0.001)
        self.assertTrue(abs(t.ft_pct  - 0.812) < 0.001)
        self.assertEqual(t.conference_rank, 5)
        self.assertEqual(t.division_rank, 0)
        self.assertEqual(t.finals_appearance, "N/A")

        self.session.query(Team).delete()
        self.session.commit()

    def test_player_single(self):
        ids = load_player("player_single.json", False, self)
        self.assertTrue(len(ids) == 1)
        p_id = ids[0]

        p = self.session.query(Player).get(p_id)
        self.assertEqual(p.name, "Brandon Goodwin")
        self.assertEqual(p.age, 26)
        self.assertEqual(p.height, 72)
        self.assertEqual(p.weight, 180)
        self.assertEqual(p.position, "Guard")
        self.assertEqual(p.fg_total, 116)
        self.assertEqual(p.fg3_total, 42)
        self.assertEqual(p.ft_total, 54)
        self.assertTrue(abs(p.avg_points_per_game - 3.7701149425287355) < 0.001)

        self.session.query(Player).delete()
        self.session.commit()

    def test_game_single(self):
        ids = load_game("game_single.json", False, self)
        self.assertTrue(len(ids) == 1)
        g_id = ids[0]

        g = self.session.query(Game).get(g_id)
        self.assertEqual(g.date, datetime.datetime.strptime("2020-12-22", "%Y-%m-%d").date())
        self.assertEqual(g.season, "2020")
        self.assertEqual(g.h_score, 125)
        self.assertEqual(g.v_score, 99)
        self.assertEqual(g.h_fga, 92)
        self.assertEqual(g.h_fgm, 42)
        self.assertEqual(g.h_fg3a, 35)
        self.assertEqual(g.h_fg3m, 15)
        self.assertEqual(g.h_fta, 32)
        self.assertEqual(g.h_ftm, 26)
        self.assertEqual(g.v_fga, 99)
        self.assertEqual(g.v_fgm, 37)
        self.assertEqual(g.v_fg3a, 33)
        self.assertEqual(g.v_fg3m, 10)
        self.assertEqual(g.v_fta, 23)
        self.assertEqual(g.v_ftm, 15)

        self.session.query(Game).delete()
        self.session.commit()

    def test_team_multiple(self):
        ids = load_team("team_multiple.json", self)
        self.assertTrue(len(ids) == 2)
        self.assertNotEqual(ids[0], ids[1])
        t0 = self.session.query(Team).get(ids[0])
        t1 = self.session.query(Team).get(ids[1])
        self.assertNotEqual(t0.name, t1.name)
        self.assertEqual(t0.season, t1.season)
        self.session.query(Team).delete()
        self.session.query(Team).delete()
        self.session.commit()

    def test_player_multiple(self):
        ids = load_player("player_multiple.json", False, self)
        self.assertTrue(len(ids) == 2)
        self.assertNotEqual(ids[0], ids[1])
        p0 = self.session.query(Player).get(ids[0])
        p1 = self.session.query(Player).get(ids[1])
        self.assertNotEqual(p0.name, p1.name)
        self.session.query(Player).delete()
        self.session.query(Player).delete()
        self.session.commit()

    def test_game_multiple(self):
        ids = load_game("game_multiple.json", False, self)
        self.assertTrue(len(ids) == 2)
        self.assertNotEqual(ids[0], ids[1])
        g0 = self.session.query(Game).get(ids[0])
        g1 = self.session.query(Game).get(ids[1])
        self.assertEqual(g0.season, g1.season)
        self.session.query(Game).delete()
        self.session.query(Game).delete()
        self.session.commit()

    def test_team_all(self):
        ids = load_team("team_all.json", self)
        self.assertTrue(len(ids) == 30)
        conference_rank_counts = [0] * 15
        for t_id in ids:
            t = self.session.query(Team).get(t_id)
            conference_rank_counts[t.conference_rank - 1] += 1
        for conference_rank_count in conference_rank_counts:
            self.assertEqual(conference_rank_count, 2)

    def test_player_all(self):
        load_team("team_all.json", self)
        ids = load_player("player_all.json", True, self)
        self.assertTrue(len(ids) == 504)
        players = set()
        for p_id in ids:
            p = self.session.query(Player).get(p_id)
            players.add(p.current_team)
        self.assertEqual(len(players), 30)

    def test_game_all(self):
        load_team("team_all.json", self)
        load_player("player_all.json", True, self)
        ids = load_game("game_all.json", True, self)
        self.assertTrue(len(ids) == 1100)
        h = set()
        v = set()
        for g_id in ids:
            g = self.session.query(Game).get(g_id)
            h.add(g.h_team)
            v.add(g.v_team)
        self.assertEqual(len(h), 30)
        self.assertEqual(len(v), 30)
