from sqlalchemy.sql import visitors
from .models import Team, Player, Game, session, db
import os
from app import app
import json 
import datetime
from .fields import *

'''
This file contains all methods to populate 
and populates database based on sample data.
'''

def load_json(filename):
    with open(filename) as file:
        jsn = json.load(file)
        file.close()
    return jsn

# execute all operations to set up a database with the req data
def create_all():
    app.logger.info('Inserting data')
    create_teams()
    app.logger.info('Inserting teams finished')
    create_players()
    app.logger.info('Inserting players finished')
    create_games()
    app.logger.info('Inserting games finished')
    session.commit()
    return

# Add Teams
def create_teams():
    teams_file = load_json('scrape/data/teams.json')
    embed_file = load_json('scrape/data/embed_links.json')
    videos = embed_file['videos']
    maps = embed_file['maps']

    for oneTeam in teams_file['data']:
        newTeam = Team()
        newTeam.id = oneTeam['_bdl_id']
        newTeam.season = oneTeam['season']
        newTeam.name = oneTeam['name']
        newTeam.wins = oneTeam['wins']
        newTeam.losses = oneTeam['losses']
        newTeam.conference_rank = oneTeam['conference_rank']
        newTeam.division_rank = oneTeam['division_rank']
        newTeam.finals_appearance  = oneTeam['finals_appearance']
        newTeam.logo_embed_link = oneTeam['logo_embed_link']
        newTeam.color = oneTeam['color']
        newTeam.fg3_pct = oneTeam['fg3_pct']
        newTeam.fg_pct = oneTeam['fg_pct']
        newTeam.ft_pct = oneTeam['ft_pct']
        newTeam.googlemap_embed_link = maps[newTeam.name]
        newTeam.video_link = videos[newTeam.name]
        session.add(newTeam)
        session.commit()
    return

# Add Players (Link Player to game and Team)
def create_players():
    players_file = load_json('scrape/data/players.json')
    for onePlayer in players_file['data']:
        newPlayer = Player()
        newPlayer.id = onePlayer['_bdl_id']
        newPlayer.name = onePlayer['name']
        newPlayer.height = onePlayer['_height']
        newPlayer.weight = onePlayer['weight']
        newPlayer.age = 2021 - int(onePlayer['_birthdate'].split('-')[0])
        newPlayer.position = onePlayer['position']
        newPlayer.headshot_embed_link = onePlayer['headshot_embed_link']
        newPlayer.fg_total = onePlayer['fg_total']
        newPlayer.fg3_total = onePlayer['fg3_total']
        newPlayer.ft_total = onePlayer['ft_total']
        newPlayer.avg_points_per_game = onePlayer['average_points_per_game']
        team = session.query(Team).filter(Team.name == onePlayer['team']).first()
        newPlayer.current_team = team.id
        session.add(newPlayer)
        session.commit()
    return

# Add Games (Link Games to Teams)
def create_games():
    games_file = load_json('scrape/data/games.json')
    for oneGame in games_file['data']:
        newGame = Game()
        newGame.id = oneGame['_bdl_id']
        newGame.date = datetime.datetime.strptime(oneGame['date'],'%Y-%m-%d').date()
        newGame.season = oneGame['season']
        newGame.h_score = oneGame['h_score']
        newGame.h_fga = oneGame['h_fga']
        newGame.h_fgm = oneGame['h_fgm']
        newGame.h_fg3m = oneGame['h_fg3m']
        newGame.h_fg3a = oneGame['h_fg3a']
        newGame.h_fta = oneGame['h_fta']
        newGame.h_ftm = oneGame['h_ftm']
        newGame.v_score = oneGame['v_score']
        newGame.v_fga = oneGame['v_fga']
        newGame.v_fgm = oneGame['v_fgm']
        newGame.v_fg3m = oneGame['v_fg3m']
        newGame.v_fg3a = oneGame['v_fg3a']
        newGame.v_fta = oneGame['v_fta']
        newGame.v_ftm = oneGame['v_ftm']
        # manage linking
        newGame.h_team = oneGame['_h_team_bdl_id']
        newGame.v_team = oneGame['_v_team_bdl_id']

        home_team = session.query(Team).get(newGame.h_team)
        visitor_team = session.query(Team).get(newGame.v_team)

        for player in home_team.players:
            newGame.players.append(player)

        for player in visitor_team.players:
            newGame.players.append(player)

        session.add(newGame)
        session.commit()
    return

if(not os.environ.get('DB_ENV') == 'prod'):
    create_all()