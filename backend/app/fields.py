from .models import Team, Game, Player
from sqlalchemy import asc, desc, func
import datetime

# all possible sortable and filterable fields for each model

player_fields = {
    "id": [Player.id, int],
    "name": [func.lower(Player.name), str],
    "age": [Player.age, int],
    "weight": [Player.weight, int],
    "height": [Player.height, int],
    "position": [func.lower(Player.position), str],
    "current_team_id": [Player.current_team, int],
    "fg_total": [Player.fg_total, int],
    "fg3_total": [Player.fg3_total, int],
    "ft_total": [Player.ft_total, int],
    "average_points_per_game": [Player.avg_points_per_game, float]
}

team_fields = {
    "season": [func.lower(Team.season), str],
    "id": [Team.id, int],
    "name": [func.lower(Team.name), str],
    "wins": [Team.wins, int],
    "losses": [Team.losses, int],
    "fg_pct": [Team.fg_pct, float],
    "fg3_pct": [Team.fg3_pct, float],
    "ft_pct": [Team.ft_pct, float],
    "conference_rank": [Team.conference_rank, int],
    "division_rank": [Team.division_rank, int],
    "finals_appearance": [func.lower(Team.finals_appearance), str]
}

game_fields = {
    "id": [Game.id, int],
    "date": [Game.date, datetime.date],
    "season": [func.lower(Game.season), str],
    "h_team_id": [Game.h_team, int],
    "h_score": [Game.h_score, int],
    "h_fga": [Game.h_fga, int],
    "h_fgm": [Game.h_fgm, int],
    "h_fg3a": [Game.h_fg3a, int],
    "h_fg3m": [Game.h_fg3m, int],
    "h_fta": [Game.h_fta, int],
    "h_ftm": [Game.h_ftm, int],
    "v_team_id": [Game.v_team, int],
    "v_score": [Game.v_score, int],
    "v_fga": [Game.v_fga, int],
    "v_fgm": [Game.v_fgm, int],
    "v_fg3a": [Game.v_fg3a, int],
    "v_fg3m": [Game.v_fg3m, int],
    "v_fta": [Game.v_fta, int],
    "v_ftm": [Game.v_ftm, int]
}

sort_functions = {'ascending': asc, 'descending': desc}