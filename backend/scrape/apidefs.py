import endpoints as ep

#SNC = stats.nba.com
#BDL = balldontlie.io
#Variables, functions, etc. specific to a site/API should
#have the respective initialism prepended to their names.

SLEEP_PERIOD = 5

DATA_SAVE_PATH = "./data/"

SNC_DEFAULT_SEASON = "2020-21"

SNC_HEADERS = {
    "Host":               "stats.nba.com",
    "User-Agent":         "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0",
    "Accept":             "application/json",
    "Accept-Language":    "en-US,en;q=0.5",
    "Referer":            "https://stats.nba.com/",
    "Accept-Encoding":    "gzip, deflate, br",
    "Connection":         "keep-alive",
    "x-nba-stats-origin": "stats",
    "x-nba-stats-token":  "true",
}

SNC_TEAM_IDS = {
    "Atlanta Hawks":          "1610612737",
    "Boston Celtics":         "1610612738",
    "Brooklyn Nets":          "1610612751",
    "Charlotte Hornets":      "1610612766",
    "Chicago Bulls":          "1610612741",
    "Cleveland Cavaliers":    "1610612739",
    "Dallas Mavericks":       "1610612742",
    "Denver Nuggets":         "1610612743",
    "Detroit Pistons":        "1610612765",
    "Golden State Warriors":  "1610612744",
    "Houston Rockets":        "1610612745",
    "Indiana Pacers":         "1610612754",
    "Los Angeles Clippers":   "1610612746",
    "Los Angeles Lakers":     "1610612747",
    "Memphis Grizzlies":      "1610612763",
    "Miami Heat":             "1610612748",
    "Milwaukee Bucks":        "1610612749",
    "Minnesota Timberwolves": "1610612750",
    "New Orleans Pelicans":   "1610612740",
    "New York Knicks":        "1610612752",
    "Oklahoma City Thunder":  "1610612760",
    "Orlando Magic":          "1610612753",
    "Philadelphia 76ers":     "1610612755",
    "Phoenix Suns":           "1610612756",
    "Portland Trail Blazers": "1610612757",
    "Sacramento Kings":       "1610612758",
    "San Antonio Spurs":      "1610612759",
    "Toronto Raptors":        "1610612761",
    "Utah Jazz":              "1610612762",
    "Washington Wizards":     "1610612764"
}

BDL_NAME_FIXES = {
    "Cam Reddish": "Cameron Reddish",
    "Marcus Morris Sr.": "Marcus Morris",
    "P.J. Tucker": "PJ Tucker",
    "Didi Louzada": "Marcos Louzada Silva",
    "Kevin Knox II": "Kevin Knox",
    "Charlie Brown Jr.": "Charles Brown",
    "T.J. Leaf": "TJ Leaf"
}

SNC_FIXES = {
    "1628455": {
        "team": "Brooklyn Nets",
        "_bdl_id": "2196"
    },
    "1629740": {
        "team": "Dallas Mavericks",
        "_bdl_id": "667302"
    },
    "202322": {
        "team": "Houston Rockets",
        "_bdl_id": "467"
    },
    "1628382": {
        "team": "Milwaukee Bucks",
        "_bdl_id": "235"
    },
    "1629018": {
        "team": "Toronto Raptors",
        "_bdl_id": "3089"
    }
}

ESPN_PLAYER_HEADSHOT_FIXES = {
    "Jalen Harris": "https://a.espncdn.com/i/headshots/nba/players/full/4066998.png"
}

ESPN_TEAM_ABBREVIATION_FIXES = {
    "NOP": "NO",
    "NYK": "NY",
    "SAS": "SA",
    "UTA": "UTAH"
}

BDL_TEAM_ABBREVIATIONS = {
    "Atlanta Hawks":          "ATL",
    "Boston Celtics":         "BOS",
    "Brooklyn Nets":          "BKN",
    "Charlotte Hornets":      "CHA",
    "Chicago Bulls":          "CHI",
    "Cleveland Cavaliers":    "CLE",
    "Dallas Mavericks":       "DAL",
    "Denver Nuggets":         "DEN",
    "Detroit Pistons":        "DET",
    "Golden State Warriors":  "GSW",
    "Houston Rockets":        "HOU",
    "Indiana Pacers":         "IND",
    "LA Clippers":            "LAC",
    "Los Angeles Lakers":     "LAL",
    "Memphis Grizzlies":      "MEM",
    "Miami Heat":             "MIA",
    "Milwaukee Bucks":        "MIL",
    "Minnesota Timberwolves": "MIN",
    "New Orleans Pelicans":   "NOP",
    "New York Knicks":        "NYK",
    "Oklahoma City Thunder":  "OKC",
    "Orlando Magic":          "ORL",
    "Philadelphia 76ers":     "PHI",
    "Phoenix Suns":           "PHX",
    "Portland Trail Blazers": "POR",
    "Sacramento Kings":       "SAC",
    "San Antonio Spurs":      "SAS",
    "Toronto Raptors":        "TOR",
    "Utah Jazz":              "UTA",
    "Washington Wizards":     "WAS"
}