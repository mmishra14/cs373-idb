#SNC = stats.nba.com
#BDL = balldontlie.io
#ESPN = site.api.espn.com
#Variables, functions, etc. specific to a site/API should
#have the respective initialism prepended to their names.

SNC_BASE = "https://stats.nba.com/stats/"
SNC_COMMONTEAMROSTER = SNC_BASE + "commonteamroster"
SNC_TEAMYEARBYYEARSTATS = SNC_BASE + "teamyearbyyearstats"
SNC_COMMONPLAYERINFO = SNC_BASE + "commonplayerinfo"

BDL_BASE = "https://www.balldontlie.io/api/v1/"
BDL_TEAMS = BDL_BASE + "teams"
BDL_PLAYERS = BDL_BASE + "players"
BDL_GAMES = BDL_BASE + "games"
BDL_STATS = BDL_BASE + "stats"

ESPN_BASE = "http://site.api.espn.com/apis/site/v2/sports/basketball/nba/"
ESPN_TEAMS = ESPN_BASE + "teams/"
