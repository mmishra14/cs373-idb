import json
import requests
import apidefs as ad
import time
import datetime
import sys
import os
import re
#SNC = stats.nba.com
#BDL = balldontlie.io
#Variables, functions, etc. specific to a site/API should
#have the respective initialism prepended to their names.

def sleep(reason):
    """
    because ratelimits
    """
    now = datetime.datetime.now().strftime("%H:%M:%S")
    print(f"[{now}] begin sleep of {ad.SLEEP_PERIOD} seconds for {reason}")
    time.sleep(ad.SLEEP_PERIOD)
    now = datetime.datetime.now().strftime("%H:%M:%S")
    print(f"[{now}]   end sleep of {ad.SLEEP_PERIOD} seconds for {reason}")

def parse_height(height):
    """
    turns height string into integer number of inches
    EX: "6-0" -> 72
    """
    feet, inches = re.findall("\\d+", height)
    return int(feet) * 12 + int(inches)

def format_height(inches):
    """
    """
    return f"{inches // 12}'{inches % 12}\""

#####################
# LEVEL 2 : per API #
#####################

def snc_get_roster_for_team(snc_team_id):
    """
    team_id the SNC team ID for the desired team
    
    get a list of SNC player IDs comprising the desired team's current roster
    """
    params = {"TeamID": snc_team_id, "Season": ad.SNC_DEFAULT_SEASON}
    res = requests.get(ad.ep.SNC_COMMONTEAMROSTER, headers=ad.SNC_HEADERS, params=params)
    if res.status_code != 200:
        raise RuntimeError(f"request failed for snc_get_roster_for_team({snc_team_id})")
    result_sets = res.json()["resultSets"]
    common_team_roster = [result_set for result_set in result_sets if result_set["name"] == "CommonTeamRoster"]
    assert len(common_team_roster) == 1
    row_set = common_team_roster[0]["rowSet"]
    return [row_set[i][-1] for i in range(len(row_set))]

def snc_get_id_for_all_players():
    """
    get a list of all SNC player IDs for current players
    """
    data = []
    for snc_team_id in ad.SNC_TEAM_IDS.values():
        data += snc_get_roster_for_team(snc_team_id)
        sleep(f"SNC team ID {snc_team_id}")
    return data

def snc_get_all_data_for_team(snc_team_id):
    """
    """
    params = {
        "TeamID":     snc_team_id,
        "LeagueID":   "00",
        "SeasonType": "Regular Season",
        "PerMode":    "Totals"
    }
    res = requests.get(ad.ep.SNC_TEAMYEARBYYEARSTATS, headers=ad.SNC_HEADERS, params=params)
    if res.status_code != 200:
        raise RuntimeError(f"request failed for snc_get_all_data_for_team({snc_team_id})")
    result_sets = res.json()["resultSets"]
    team_stats = [result_set for result_set in result_sets if result_set["name"] == "TeamStats"]
    assert len(team_stats) == 1
    latest_available_season_data = team_stats[0]["rowSet"][-1]
    field_indices = {
        "season":          3,
        "_snc_id":         0,
        "wins":            5,
        "losses":          6,
        "fg_pct":         17,
        "fg3_pct":        20,
        "ft_pct":         23,
        "conference_rank": 8,
        "division_rank":   9
    }
    data = {key: latest_available_season_data[val] for key, val in field_indices.items()}

    #get some other attributes
    second_latest_available_season_data = team_stats[0]["rowSet"][-2]
    data["finals_appearance"] = second_latest_available_season_data[14]
    data["name"] = latest_available_season_data[1] + " " + latest_available_season_data[2]
    data["roster"] = snc_get_roster_for_team(snc_team_id)
    return data

def bdl_get_id_for_team(data):
    """
    only modifies; returns nothing
    """
    res = requests.get(ad.ep.BDL_TEAMS)
    teams = res.json()["data"]

    bdl_ids = [team["id"] for team in teams if team["full_name"].lower() == data["name"].lower()]
    bdl_id = None
    if len(bdl_ids) == 1:
        bdl_id = bdl_ids[0]
    else:
        print(f"[BDL] no match for {data['name']}. provide the BDL ID manually (see {ad.ep.BDL_TEAMS}):")
        bdl_id = int(input())
    data["_bdl_id"] = bdl_id

def bdl_get_abbreviation_for_team(data):
    """
    only modifies; returns nothing
    """
    res = requests.get(ad.ep.BDL_TEAMS)
    teams = res.json()["data"]

    abbreviations = [team["abbreviation"] for team in teams if team["full_name"].lower() == data["name"].lower()]
    abbreviation = None
    if len(abbreviations) == 1:
        abbreviation = abbreviations[0]
    else:
        print(f"[BDL] no match for {data['name']}. provide the abbreviation manually (see {ad.ep.BDL_TEAMS}):")
        abbreviation = int(input())
    data["_abbreviation"] = abbreviation

def espn_get_logo_embed_link_for_team(data):
    """
    only modifies; returns nothing
    """
    abbreviation = ad.ESPN_TEAM_ABBREVIATION_FIXES[data["_abbreviation"]] \
        if data["_abbreviation"] in ad.ESPN_TEAM_ABBREVIATION_FIXES \
        else data["_abbreviation"]

    res = requests.get(ad.ep.ESPN_TEAMS + abbreviation)
    team = res.json()["team"]
    data["color"] = team["color"]
    data["logo_embed_link"] = team["logos"][0]["href"]

def snc_get_all_data_for_player(snc_player_id):
    """
    """
    params = {"PlayerID": snc_player_id}
    res = requests.get(ad.ep.SNC_COMMONPLAYERINFO, headers=ad.SNC_HEADERS, params=params)
    if res.status_code != 200:
        raise RuntimeError(f"request failed for snc_get_all_data_for_player({snc_player_id})")
    result_sets = res.json()["resultSets"]
    common_player_info = [result_set for result_set in result_sets if result_set["name"] == "CommonPlayerInfo"]
    assert len(common_player_info) == 1
    latest_available_season_data = common_player_info[0]["rowSet"][0] #TODO should this be 0 or -1; haven't seen case where array has length != 1
    field_indices = {
        "_snc_id":    0,
        "name":       3,
        "_birthdate": 7,
        "height":    11,
        "weight":    12,
        "position":  15
    }
    data = {key: latest_available_season_data[val] for key, val in field_indices.items()}
    data["_height"] = parse_height(data["height"])
    data["height"] = format_height(data["_height"])
    data["weight"] = int(data["weight"])
    return data

def bdl_get_team_for_player(data):
    """
    modifies data, which should be populated by
    snc_get_all_data_for_player already.
    returns nothing
    """
    name = data["name"]
    params = {"search": name}
    if name in ad.BDL_NAME_FIXES:
        params["search"] = ad.BDL_NAME_FIXES[name]

    res = requests.get(ad.ep.BDL_PLAYERS, params=params)
    if res.status_code != 200:
        raise RuntimeError(f"request failed for bdl_get_all_data_for_player({data['name']})")
    players = res.json()["data"]
    team = None
    bdl_id = None
    n_matches = len(players)

    #if BDL query does not find a unique match, manually input
    snc_player_id = str(data["_snc_id"])
    if snc_player_id in ad.SNC_FIXES:
        team = ad.SNC_FIXES[snc_player_id]["team"]
        bdl_id = ad.SNC_FIXES[snc_player_id]["_bdl_id"]
    elif n_matches != 1:
        print(f"[BDL] {n_matches} matches for query \"{data['name']}\"")
        for player in players:
            print(f"    {player['id']} | {player['first_name']} {player['last_name']} | {player['team']['full_name']}")
        print("[BDL] provide the team name manually:")
        team = input()
        print("[BDL] provide the BDL player ID manually:")
        bdl_id = int(input())
    else:
        team = players[0]["team"]["full_name"]
        bdl_id = players[0]["id"]

    data["team"] = team
    data["_team_abbreviation"] = ad.BDL_TEAM_ABBREVIATIONS[team]
    data["_bdl_id"] = bdl_id

def bdl_get_stats_for_player(player_data):
    params = {
        "page": 1,
        "per_page": 100,
        "player_ids[]": player_data["_bdl_id"]
    }
    fg_total = 0
    fg3_total = 0
    ft_total = 0
    _total_points = 0
    _n_games = 0
    most_recent_game = None

    while True:
        res = requests.get(ad.ep.BDL_STATS, params=params)
        if res.status_code != 200:
            raise RuntimeError(f"request failed for bdl_get_stats_for_player({player_data['name']} / {player_data['_bdl_id']})")
        jsn = res.json()
        meta = jsn["meta"]
        data = jsn["data"]
        if meta["total_pages"] <= meta["current_page"]:
            try:
                most_recent_game = data[-1]["game"]["id"]
            except IndexError:
                most_recent_game = -1 #NOTE this in docs and @nalin
            break

        if len(data) != 0:
            none_to_0 = lambda x : x if x != None else 0

            fg_total      += sum(none_to_0(item["fgm"])  for item in data)
            fg3_total     += sum(none_to_0(item["fg3m"]) for item in data)
            ft_total      += sum(none_to_0(item["ftm"])  for item in data)
            _total_points += sum(none_to_0(item["pts"])  for item in data)
            _n_games      += sum(1 for item in data if item["pts"] != None)

        time.sleep(1)
        params["page"] += 1

    player_data["_most_recent_game_bdl_id"] = most_recent_game
    player_data["average_points_per_game"] = (_total_points / _n_games) if _n_games != 0 else -1
    player_data["fg_total"] = fg_total
    player_data["fg3_total"] = fg3_total
    player_data["ft_total"] = ft_total

def get_all_data_for_player(snc_player_id):
    """
    """
    data = snc_get_all_data_for_player(snc_player_id)
    bdl_get_team_for_player(data)
    bdl_get_stats_for_player(data)
    return data

def bdl_get_stats_for_game(game_data):
    params = {
        "game_ids[]": game_data["_bdl_id"],
        "per_page": 100
    }
    res = requests.get(ad.ep.BDL_STATS, params=params)
    if res.status_code != 200:
        raise RuntimeError(f"request failed for bdl_get_stats_for_game({game_data['_bdl_id']})")

    h_fga  = v_fga  = 0
    h_fgm  = v_fgm  = 0
    h_fg3a = v_fg3a = 0
    h_fg3m = v_fg3m = 0
    h_fta  = v_fta  = 0
    h_ftm  = v_ftm  = 0

    none_to_0 = lambda x : x if x != None else 0

    players = res.json()["data"]
    for player in players:
        if player["team"]["id"] == player["game"]["home_team_id"]:
            h_fga  += none_to_0(player["fga" ])
            h_fgm  += none_to_0(player["fgm" ])
            h_fg3a += none_to_0(player["fg3a"])
            h_fg3m += none_to_0(player["fg3m"])
            h_fta  += none_to_0(player["fta" ])
            h_ftm  += none_to_0(player["ftm" ])
        elif player["team"]["id"] == player["game"]["visitor_team_id"]:
            v_fga  += none_to_0(player["fga" ])
            v_fgm  += none_to_0(player["fgm" ])
            v_fg3a += none_to_0(player["fg3a"])
            v_fg3m += none_to_0(player["fg3m"])
            v_fta  += none_to_0(player["fta" ])
            v_ftm  += none_to_0(player["ftm" ])
        else:
            print(f"player's team has ID {player['team']['id']}")
            print(f"home team has ID {player['game']['home_team_id']}")
            print(f"visitor team has ID {player['game']['visitor_team_id']}")
            assert False #"impossible" situation

    game_data["h_fga" ] = h_fga
    game_data["h_fgm" ] = h_fgm
    game_data["h_fg3a"] = h_fg3a
    game_data["h_fg3m"] = h_fg3m
    game_data["h_fta" ] = h_fta
    game_data["h_ftm" ] = h_ftm
    game_data["v_fga" ] = v_fga
    game_data["v_fgm" ] = v_fgm
    game_data["v_fg3a"] = v_fg3a
    game_data["v_fg3m"] = v_fg3m
    game_data["v_fta "] = v_fta
    game_data["v_ftm "] = v_ftm

def espn_get_headshot_embed_link_for_all_players(data):
    """
    only modifies; returns nothing
    """
    for abbreviation in ad.BDL_TEAM_ABBREVIATIONS.values():
        players = [player for player in data if player["_team_abbreviation"] == abbreviation]
        espn_abbreviation = ad.ESPN_TEAM_ABBREVIATION_FIXES[abbreviation] \
            if abbreviation in ad.ESPN_TEAM_ABBREVIATION_FIXES \
            else abbreviation
        print("[ESPN] " + ad.ep.ESPN_TEAMS + espn_abbreviation + "/roster")
        res = requests.get(ad.ep.ESPN_TEAMS + espn_abbreviation + "/roster")
        athletes = res.json()["athletes"]

        for player in players:
            headshot = None
            matches = [athlete for athlete in athletes if athlete["fullName"].lower() == player["name"].lower()]
            n_matches = len(matches)
            if player["name"] in ad.ESPN_PLAYER_HEADSHOT_FIXES:
                headshot = ad.ESPN_PLAYER_HEADSHOT_FIXES[player["name"]]
            elif n_matches != 1:
                print(f"[ESPN] {n_matches} matches for player \"{player['name']}\"")
                for match in matches:
                    print(f"    {match['fullName']} | {match['headshot']}")
                print("[ESPN] provide the headshot link manually:")
                headshot = input()
            else:
                if "headshot" in matches[0]:
                    headshot = matches[0]["headshot"]["href"]
                else:
                    headshot = "https://a.espncdn.com/i/headshots/nophoto.png"
            player["headshot_embed_link"] = headshot


########################
# LEVEL 1 : per pillar #
########################

def get_all_team_data():
    """
    """
    data = []
    for snc_team_id in ad.SNC_TEAM_IDS.values():
        data.append(snc_get_all_data_for_team(snc_team_id))
        bdl_get_id_for_team(data[-1])
        bdl_get_abbreviation_for_team(data[-1])
        espn_get_logo_embed_link_for_team(data[-1])
        sleep(f"SNC team ID {snc_team_id}")
    return {"data": data}

def get_all_player_data():
    """
    """
    #first we need a list of all player IDs
    snc_player_ids = None
    if os.path.exists(ad.DATA_SAVE_PATH + "snc_player_ids.json"):
        print(f"Found list of SNC player IDs at {ad.DATA_SAVE_PATH}snc_player_ids.json.")
        with open(ad.DATA_SAVE_PATH + "snc_player_ids.json", "r") as fh:
            snc_player_ids = json.load(fh)
    else:
        print(f"No list of SNC player IDs at {ad.DATA_SAVE_PATH}snc_player_ids.json. Scraping them now.")
        snc_player_ids = snc_get_id_for_all_players()
        with open(ad.DATA_SAVE_PATH + "snc_player_ids.json", "w") as fh:
            json.dump(snc_player_ids, fh, indent = 4)

    data = []
    for snc_player_id in snc_player_ids:
        data.append(get_all_data_for_player(snc_player_id))
        sleep(f"SNC player id {snc_player_id}")

    espn_get_headshot_embed_link_for_all_players(data)

    return {"data": data}

def get_all_game_data():
    """
    """
    data = []
    params = {
        "page": 1,
        "per_page": 100,
        "seasons[]": 2020
    }
    while True:
        res = requests.get(ad.ep.BDL_GAMES, params=params)
        if res.status_code != 200:
            raise RuntimeError(f"request failed for get_all_game_data() on page {params['page']}")
        jsn = res.json()
        meta = jsn["meta"]
        games = jsn["data"]
        if meta["total_pages"] <= meta["current_page"]:
            break

        for game in games:
            game_data = {
                "_bdl_id":        game["id"],
                "date":           re.split("T", game["date"])[0],
                "season":         str(game["season"]),
                "_h_team_bdl_id": game["home_team"]["id"],
                "h_score":        game["home_team_score"],
                "_v_team_bdl_id": game["visitor_team"]["id"],
                "v_score":        game["visitor_team_score"],
            }

            bdl_get_stats_for_game(game_data)
            time.sleep(1)
            data.append(game_data)

        sleep(f"games page {params['page']}")
        params["page"] += 1
    return {"data": data}

######################
# LEVEL 0 : all data #
######################

def get_and_write_all_data(do_players=True, do_teams=True, do_games=True):
    """
    The main scrape function. A single call
    to this function should reproducibly get
    and write all data needed for the backend
    through the functions defined above.
    This function will write 3 files:
     - players.json
     - teams.json
     - games.json
    whose basic structure is tabular, with
    row IDs == our API's internal IDs == index into list.
    """
    if do_teams:
        with open(ad.DATA_SAVE_PATH + "teams.json", "w") as fh:
            json.dump(get_all_team_data(), fh, indent = 4)
    if do_players:
        with open(ad.DATA_SAVE_PATH + "players.json", "w") as fh:
            json.dump(get_all_player_data(), fh, indent = 4)
    if do_games:
        with open(ad.DATA_SAVE_PATH + "games.json", "w") as fh:
            json.dump(get_all_game_data(), fh, indent = 4)

if __name__ == "__main__":
    do_players = False
    do_teams   = False
    do_games   = False

    for arg in sys.argv[1:]:
        if arg == "players":
            do_players = True
        elif arg == "teams":
            do_teams = True
        elif arg == "games":
            do_games = True
        else:
            print(f"Target '{arg}' invalid. Ignored. Valid targets are 'players', 'teams', and 'games'.")

    if not (do_players or do_teams or do_games):
        do_players = do_teams = do_games = True

    targets = []
    if do_players:
        targets.append("players")
    if do_teams:
        targets.append("teams")
    if do_games:
        targets.append("games")

    print(f"Scraping {targets}. This will take a while.")
    get_and_write_all_data(do_players=do_players, do_teams=do_teams, do_games=do_games)
